package be.hcpl.android.energica.services.mqtt

open class MqttPushData(
    val topic: String = "energica",
    open val payload: Any?,
    val qos: Int? = 1,
    val retain: Int? = 1,
)

data class MqttPushSoc(
    override val payload: Int?
) : MqttPushData(topic = "energica/soc", payload = payload)

data class MqttPushRange(
    override val payload: Int?
) : MqttPushData(topic = "energica/range", payload = payload)

data class MqttPushOdometer(
    override val payload: Int?
) : MqttPushData(topic = "energica/odo", payload = payload)

data class MqttPushReserveWh(
    override val payload: Int?
) : MqttPushData(topic = "energica/reserve", payload = payload)

data class MqttPushAmbientTemp(
    override val payload: Int?
) : MqttPushData(topic = "energica/temp/ambient", payload = payload)

data class MqttPushBatteryTemp(
    override val payload: Int?
) : MqttPushData(topic = "energica/temp/battery", payload = payload)

data class MqttPushChargeCurrent(
    override val payload: Float?
) : MqttPushData(topic = "energica/current", payload = payload)

data class MqttPushChargePower(
    override val payload: Float?
) : MqttPushData(topic = "energica/power", payload = payload)

data class MqttPushBatteryVoltage(
    override val payload: Float?
) : MqttPushData(topic = "energica/voltage", payload = payload)

data class MqttPushMinCellVoltage(
    override val payload: Int?
) : MqttPushData(topic = "energica/minCellVoltage", payload = payload)

data class MqttPushMaxCellVoltage(
    override val payload: Int?
) : MqttPushData(topic = "energica/maxCellVoltage", payload = payload)