package be.hcpl.android.energica.model.data

data class LastLocation(val latitude: Double, val longitude: Double)
