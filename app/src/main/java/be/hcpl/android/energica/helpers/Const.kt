package be.hcpl.android.energica.helpers

import be.hcpl.android.energica.BuildConfig

object Const {

    const val DEFAULT_ENDPOINT = "http://10.0.2.2/"
    const val DEFAULT_TOKEN = "NO_TOKEN"
    const val DATA_PUSH_DELAY = 30*1000L // time in between data pushes
    const val DEFAULT_BLE_AUTO_CONNECT = true // auto reconnect is disabled by default
    const val DEFAULT_BLE_RAW_DEBUG = false // raw logs disabled by default
    const val DEFAULT_OBD2_RAW_DEBUG = false // raw logs disabled by default

    const val NOT_CHARGING = -1f
    const val CONSUMPTION_UNIT_KM_KWH = 1
    const val CONSUMPTION_UNIT_KWH_100KM = 0
    const val CONSUMPTION_UNIT_KWH_100MI = 3
    const val CONSUMPTION_UNIT_MI_KWH = 4
    const val CONSUMPTION_UNIT_MPG = 6
    const val CONSUMPTION_UNIT_WH_KM = 2
    const val CONSUMPTION_UNIT_WH_MI = 5
    const val DISTANCE_UNIT_MI = 1
    const val DISTANCE_UNIT_KM = 0
    const val ENERGICA_SERVICE_UUID = "14839AC5-7D7F-415D-9A43-167340CF233A"
    const val SETTINGS_DEVICE = "SETTINGS_DEVICE"

    // BLE state values
    const val STATE_SEARCHING = 0
    const val STATE_NOT_CONNECTED = -1
    const val STATE_IDLE = 60
    const val STATE_CHARGE = 100
    const val STATE_RUN = 40
    const val STATE_ERROR = 80

    // OBD2 state values
    const val STATE_OBD_RUNNING = 31
    const val STATE_OBD_STOPPED = 30

    // GPS state values
    const val STATE_GPS_RUNNING = 21
    const val STATE_GPS_STOPPED = 20

    const val MPS_TO_KPH = 3.60f
    const val KM_TO_MI = 0.621371192

    const val BROADCAST_FILTER = BuildConfig.APPLICATION_ID + ".BROADCAST"
    const val BROADCAST_EVENT = BuildConfig.APPLICATION_ID + ".EVENT"
    const val BROADCAST_MESSAGE = BuildConfig.APPLICATION_ID + ".MESSAGE"
    const val MESSAGE = "message"

    const val BROADCAST_LOCATION = BuildConfig.APPLICATION_ID + ".LOCATION"
    const val LOCATION_LAT = "lat"
    const val LOCATION_LON = "lon"
    const val LOCATION_PRECISION = "precision"
    const val LOCATION_SPEED = "speed"

    const val BROADCAST_GPS_STATE = BuildConfig.APPLICATION_ID + ".GPS.STATE"
    const val GPS_STATE = "gpsState"

    const val BROADCAST_BLE_STATE = BuildConfig.APPLICATION_ID + ".BLE.STATE"
    const val BLE_STATE = "bleState"
    const val BROADCAST_BLE_EVENT = BuildConfig.APPLICATION_ID + ".BLE.EVENT"
    const val BLE_EVENT = "bleEvent"
    const val BROADCAST_BLE_GENERIC_EVENT = BuildConfig.APPLICATION_ID + ".BLE.GENERIC.EVENT"
    const val BLE_GENERIC_EVENT = "bleGenericEvent"

    const val BROADCAST_OBD2_STATE = BuildConfig.APPLICATION_ID + ".OBD2.STATE"
    const val BROADCAST_OBD2_RAW = BuildConfig.APPLICATION_ID + ".OBD2.RAW"
    const val BROADCAST_OBD2_DATA = BuildConfig.APPLICATION_ID + ".OBD2.DATA"
    const val BROADCAST_OBD2_DATA_CHARGE = BuildConfig.APPLICATION_ID + ".OBD2.DATA.CHARGE"
    const val BROADCAST_OBD2_GENERIC_DATA = BuildConfig.APPLICATION_ID + ".OBD2.GENERIC.DATA"

    const val OBD2_JOB_STATE = "jobState"
    const val OBD2_RAW_DATA = "rawData"
    const val OBD2_NAME = "genericDataName"
    const val OBD2_RESULT = "genericDataResult"
    const val OBD2_SOC = "soc"
    const val OBD2_VOLT = "voltage"
    const val OBD2_CURRENT = "current"
    const val OBD2_POWER = "power"
    const val OBD2_CURRENT_AC = "currentAC"
    const val OBD2_CURRENT_DC = "currentDC"
    const val OBD2_TEMP1 = "temp1"
    const val OBD2_TEMP2 = "temp2"
    const val OBD2_CHARGE_STATE = "state"
    const val OBD2_CELL_MIN = "cellMin"
    const val OBD2_CELL_MAX = "cellMax"
    const val OBD2_ODOMETER = "odoMeter"

    const val APP_LOGS_FILE = "applogs.txt"
}
