package be.hcpl.android.energica.helpers;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import be.hcpl.android.energica.interfaces.CommParserCallback;
import be.hcpl.android.energica.model.ble.GpsData;
import be.hcpl.android.energica.model.ble.VehicleStatus;

public class CommParser {

    public static final int ADDRESSMATCHATTEMPTS = 5;
    public static final int NO_CHARACTERISTICS = -1;
    public static final int OK = 1;
    public static final int OUT_OF_BOUNDS = -2;

    public static final int SEED_RECEIVED     = 0;
    public static final int MATCH_ATTEMPT     = 1;
    public static final int VEHICLE_STATUS    = 2;
    public static final int OUTPUT            = 3;
    public static final int ODOMETER          = 4;
    public static final int DIAGNOSTICS       = 25;
    public static final int CHARGE_LIMIT_20   = 20;
    public static final int CHARGE_LIMIT_21   = 21;
    public static final int GPS_DATA          = 26;
    public static final int CHARGER_LOCATIONS = 28;

    // Helpers to handle unsigned conversions a bit easier

    static public int   u8(byte b) { return (b & 0xff); }
    static public int  u16(byte b1, byte b2) { return ((b1 & 0xff) << 8) | (b2 & 0xff); }
    static public int  s16(byte b1, byte b2) { return (b1 << 8) | (b2 & 0xff); }
    static public int  u32(byte b1, byte b2, byte b3, byte b4) { return ((b1 & 0xff) << 24) | ((b2 & 0xff) << 16) | ((b3 & 0xff) << 8) | (b4 & 0xff); }
    static public int  s32(byte b1, byte b2, byte b3, byte b4) { return (b1 << 24) | ((b2 & 0xff) << 16) | ((b3 & 0xff) << 8) | (b4 & 0xff); }
    static public long s64(byte b1, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8) { return ((long)b1 << 56) | ((long)(b2 & 0xff) << 48) | ((long)(b3 & 0xff) << 40) | ((long)(b4 & 0xff) << 32) | ((long)(b5 & 0xff) << 24) | ((long)(b6 & 0xff) << 16) | ((long)(b7 & 0xff) << 8) | (long)(b8 & 0xff); }

    static public int  u16(byte[] b, int i1, int i2) { return u16(b[i1], b[i2]); }
    static public int  s16(byte[] b, int i1, int i2) { return s16(b[i1], b[i2]); }
    static public int  u32(byte[] b, int i1, int i2, int i3, int i4) { return u32(b[i1], b[i2], b[i3], b[i4]); }
    static public int  s32(byte[] b, int i1, int i2, int i3, int i4) { return s32(b[i1], b[i2], b[i3], b[i4]); }
    static public long s64(byte[] b, int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8) { return s64(b[i1], b[i2], b[i3], b[i4], b[i5], b[i6], b[i7], b[i8]); }

    private final GpsData gpsData;
    private static int matchAttempt = ADDRESSMATCHATTEMPTS;
    private static boolean deviceRecognized = false;
    private static long lastCPOIUpdate;
    private static BluetoothGattCharacteristic writeBleGatt;
    private static boolean usrKeySts = false;
    private final BleWrapper bleWrapper;
    private final CommParserCallback parserCallback;
    private final VehicleStatus vehicleStatus;

    public CommParser(BleWrapper CommWrapper, BluetoothGattCharacteristic characteristic, CommParserCallback callback) {
        parserCallback = (callback == null ? new CommParserCallback.Null() : callback);
        bleWrapper = CommWrapper;
        writeBleGatt = characteristic;
        vehicleStatus = new VehicleStatus();
        gpsData = new GpsData();
    }

    public GpsData getGpsData() {
        return gpsData;
    }

    public void setWriteCharacteristic(BluetoothGattCharacteristic writeChar) {
        writeBleGatt = writeChar;
    }

    /**
     * send BLE request for vehicle info
     */
    public void vehicleInfoRequest() {
        byte[] txBuffer = {4, 17, 2, -1, 0, 0, 0, 0, 0, 0};
        if (bleWrapper != null && bleWrapper.isConnected() && writeBleGatt != null) {
            bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        }
    }

    /**
     * BLE request for odometer info
     */
    public void odometerInfoRequest() {
        byte[] txBuffer = {4, 17, 4, -1, 0, 0, 0, 0, 0, 0};
        if (bleWrapper != null && bleWrapper.isConnected() && writeBleGatt != null) {
            bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        }
    }

    public int setChargePowerLimit(byte power) {
        if (power < 0 || power > 100) {
            return OUT_OF_BOUNDS;
        }
        byte[] txBuffer = {4, 17, 20, -1, power, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public int getChargePowerLimit() {
        byte[] txBuffer = {4, 17, 21, -1, 0, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public int setChgTermination() {
        byte[] txBuffer = {4, 17, 22, -1, 0, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public int setSpeedLimit(int spd) {
        if (spd < 20 || spd > 255) {
            return OUT_OF_BOUNDS;
        }
        byte[] txBuffer = {4, 17, 23, -1, (byte) spd, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public int setHornPulse() {
        byte[] txBuffer = {4, 17, 24, -1, 1, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    /**
     * BLE request to retrieve diagnostics codes
     */
    public int getDTCList() {
        byte[] txBuffer = {4, 17, 25, -1, 0, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public int sendFoundStationsNumber(int stationsNumber) {
        byte[] txBuffer = {4, 17, 27, -1, (byte) stationsNumber, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public int setResetTrip() {
        byte[] txBuffer = {4, 17, 29, -1, 1, 0, 0, 0, 0, 0};
        if (bleWrapper == null || !bleWrapper.isConnected() || writeBleGatt == null) {
            return NO_CHARACTERISTICS;
        }
        bleWrapper.writeDataToCharacteristic(writeBleGatt, txBuffer);
        return OK;
    }

    public void parseBuffer(byte[] rx_buffer) {
        switch (rx_buffer[0]) {
            case SEED_RECEIVED:
                if (rx_buffer[1] == -1) {
                    int num = u32(rx_buffer, 5, 4, 3, 2);
                    parserCallback.onSeedReceived(num);
                    return;
                }
                return;

            case MATCH_ATTEMPT:
                if (rx_buffer[2] != 0) {
                    deviceRecognized = true;
                    matchAttempt = ADDRESSMATCHATTEMPTS;
                    parserCallback.onConnectionConfirmed();
                } else {
                    deviceRecognized = false;
                    matchAttempt--;
                    if (matchAttempt <= 0) {
                        parserCallback.onConnectionRejected();
                    }
                }
                usrKeySts = (rx_buffer[6] != 0);
                vehicleStatus.model = rx_buffer[4];
                // add support for other models also...
                switch (vehicleStatus.model) {
                    case VehicleStatus.MODEL_EGO:
                        Log.d("CommParser","model EGO");
                        break;
                    case VehicleStatus.MODEL_EVA:
                        // Also used for Eva Ribelle
                        Log.d("CommParser","model EVA");
                        break;
                    case VehicleStatus.MODEL_SS9_PLUS:
                        Log.d("CommParser","model EVA SS9");
                        break;
                }
                return;

            case VEHICLE_STATUS:
                switch (rx_buffer[1]) {
                    case -2:
                        vehicleStatus.cMAINSV          = (short)u16(rx_buffer, 3, 2);
                        vehicleStatus.resBatteryEnergy = (short)u16(rx_buffer, 5, 4);
                        parserCallback.onVehicleStatusReceived(vehicleStatus);
                        return;

                    case 0:
                        vehicleStatus.soc      = u8(rx_buffer[2]);
                        vehicleStatus.state    = u8(rx_buffer[3]);
                        vehicleStatus.subState = u8(rx_buffer[4]);
                        vehicleStatus.range    = u16(rx_buffer, 6, 5);
                        vehicleStatus.temp     = u8(rx_buffer[7]);
                        // added a partial update here
                        parserCallback.onVehicleStatusReceived(vehicleStatus);
                        return;

                    case 1:
                        vehicleStatus.avgConsumption = s32(rx_buffer, 5, 4, 3, 2) / 10.0f;
                        vehicleStatus.instKmKwh      = s16(rx_buffer, 7 ,6) / 100.0f;
                        // added a partial update here
                        parserCallback.onVehicleStatusReceived(vehicleStatus);
                        return;

                    case 2:
                        vehicleStatus.instKwh100Km = u16(rx_buffer, 3, 2) / 100.0f;
                        vehicleStatus.bPackV       = u16(rx_buffer, 5, 4) / 10.0f;
                        vehicleStatus.bPackI       = s16(rx_buffer, 7, 6) / 10.0f;
                        // added a partial update here
                        parserCallback.onVehicleStatusReceived(vehicleStatus);
                        return;

                    case 3:
                        vehicleStatus.cDCC    = s16(rx_buffer, 3, 2) / 100.0f;
                        vehicleStatus.cDCV    = s16(rx_buffer, 5, 4) / 10.0f;
                        vehicleStatus.cMAINSC = s16(rx_buffer, 7, 6) / 10.0f;
                        // added a partial update here
                        parserCallback.onVehicleStatusReceived(vehicleStatus);
                        return;
                }
                return;

            case OUTPUT:
                if (rx_buffer[1] == -1) {
                    vehicleStatus.speed  = s16(rx_buffer, 3, 2);
                    vehicleStatus.rpm    = s16(rx_buffer, 5, 4);
                    vehicleStatus.torque = s16(rx_buffer, 7, 6);
                    vehicleStatus.power  = Math.round(vehicleStatus.torque * 2 * 3.1415927f * vehicleStatus.rpm / 60000.0f);
                    parserCallback.onSpeedRPMTorqueReceived(vehicleStatus.speed, vehicleStatus.rpm, vehicleStatus.torque, (float) vehicleStatus.power);
                    return;
                }
                return;

            case ODOMETER:
                switch (rx_buffer[1]) {
                    case -2:
                        vehicleStatus.tripMeter = u32(rx_buffer, 5, 4, 3, 2) / 10.0f;
                        // added a partial update here
                        parserCallback.onVehicleStatusReceived(vehicleStatus);
                        return;

                    case 0:
                        vehicleStatus.totalOdometer      = u32(rx_buffer, 5, 4, 3, 2);
                        vehicleStatus.avgConsumptionUnit = u8(rx_buffer[6]);
                        vehicleStatus.distanceUnit       = u8(rx_buffer[7]);
                        // added a partial update here
                        parserCallback.onVehicleStatusReceived(vehicleStatus);
                        return;
                }
                return;

            case CHARGE_LIMIT_20:
            case CHARGE_LIMIT_21:
                // case 20 and 21 were duplicated here, check if we can combine or have error in logic
                if (rx_buffer[1] == -1) {
                    vehicleStatus.chgPowerLimit = u8(rx_buffer[2]);
                    parserCallback.onChgPwrLimitReceived(vehicleStatus.chgPowerLimit);
                    return;
                }
                return;

            case DIAGNOSTICS:
                // TODO test and complete diagnostics data received, no trigger in app ATM
                if (rx_buffer[1] == 0) {
                    int dtc1 = s32((byte)0, (byte)(rx_buffer[4] & 0x0f), rx_buffer[3], rx_buffer[2]);
                    int dtc2 = s32((byte)0, (byte)(rx_buffer[7] & 0x0f), rx_buffer[6], rx_buffer[5]);
                    return;
                } else if (rx_buffer[1] == -1) {
                    int dtc1 = s32((byte)0, (byte)(rx_buffer[4] & 0x0f), rx_buffer[3], rx_buffer[2]);
                    int dtc2 = s32((byte)0, (byte)(rx_buffer[7] & 0x0f), rx_buffer[6], rx_buffer[5]);
                    return;
                } else if (rx_buffer[1] == -2) {
                    int dtc1 = s32((byte)0, (byte)(rx_buffer[4] & 0x0f), rx_buffer[3], rx_buffer[2]);
                    int dtc2 = s32((byte)0, (byte)(rx_buffer[7] & 0x0f), rx_buffer[6], rx_buffer[5]);
                    return;
                } else {
                    parserCallback.onParseError(rx_buffer);
                    return;
                }

            case GPS_DATA:
                // GPS data received from bike
                if (rx_buffer[1] == 0) {
                    gpsData.course          = (rx_buffer[2] & 255) | ((rx_buffer[3] & 1) << 8);
                    gpsData.speed           = ((rx_buffer[3] >> 1) & 0x7f) | ((rx_buffer[4] & 3) << 8);
                    gpsData.latitudeDeciMilliminutes = (((rx_buffer[4] & 255) >> 2) & 63) | ((rx_buffer[5] & 255) << 6);
                    gpsData.latitudeMinutes = rx_buffer[6] & 63;
                    gpsData.latSign         = ((rx_buffer[7] >> 6) & 1) != 0 ? -1 : 1;
                    gpsData.latitudeDegrees = ((rx_buffer[6] >> 6) & 3) | ((rx_buffer[7] & 63) << 2);
                } else if (rx_buffer[1] == 1) {
                    gpsData.fix = rx_buffer[2] & 3;
                    int altitude = ((rx_buffer[2] >> 2) & 63) | (rx_buffer[3] << 6) | ((rx_buffer[4] & 1) << 14);
                    int altitudeSign = (rx_buffer[4] >> 1) & 1;
                    if (altitudeSign != 0) {
                        altitude = -altitude;
                    }
                    gpsData.altitude = altitude;
                    gpsData.longitudeDeciMilliminutes = (((rx_buffer[4] & 255) >> 2) & 63) | ((rx_buffer[5] & 255) << 6);
                    gpsData.longitudeMinutes = rx_buffer[6] & 63;
                    gpsData.longitudeDegrees = ((rx_buffer[6] >> 6) & 3) | ((rx_buffer[7] & 63) << 2);
                    gpsData.lonSign = ((rx_buffer[7] >> 6) & 1) != 0 ? -1 : 1;
                } else if (rx_buffer[1] == -2) { // 0xfe
                    gpsData.dateMilliseconds    = (rx_buffer[2] & 255) | ((rx_buffer[3] & 3) << 8);
                    gpsData.dateSeconds         = (rx_buffer[3] >> 2) & 63;
                    gpsData.dateMinutes         = rx_buffer[4] & 63;
                    gpsData.dateHour            = ((rx_buffer[4] >> 6) & 3) | ((rx_buffer[5] & 7) << 2);
                    gpsData.dateDay             = (rx_buffer[5] >> 3) & 31;
                    gpsData.dateMonth           = rx_buffer[6] & 15;
                    gpsData.dateYear            = ((rx_buffer[6] >> 4) & 15) | ((rx_buffer[7] & 7) << 4);
                    gpsData.connectedSatellites = (rx_buffer[7] >> 3) & 31;
                    gpsData.latitude  = gpsData.convertLat();
                    gpsData.longitude = gpsData.convertLon();
                    if (!(gpsData.latitude == 0.0d && gpsData.longitude == 0.0d)) {
                        parserCallback.onGPSDataReceived(gpsData);
                    }
                }
                long t = System.currentTimeMillis();
                if (t > lastCPOIUpdate + 15000) { // Request charge points every 15s
                    lastCPOIUpdate = t;
                    if (gpsData.convertLat() != 0 || gpsData.convertLon() != 0) {
                        parserCallback.onNearbyChargePointsRequested();
                        return;
                    }
                    return;
                }
                return;

            case CHARGER_LOCATIONS:
                if (rx_buffer[1] == -1) {
                    parserCallback.onChargePointsRequested();
                    return;
                }
                return;

            default:
                return;
        }
    }
}
