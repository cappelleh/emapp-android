package be.hcpl.android.energica.services.evmonitor

import android.content.Context
import be.hcpl.android.energica.R
import be.hcpl.android.energica.helpers.Help
import be.hcpl.android.energica.model.evmonitor.LoginOutput
import be.hcpl.android.energica.model.evmonitor.PushDataOutput
import be.hcpl.android.energica.model.evmonitor.VehiclesOutput
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.POST
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.http.Field
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded


interface EvMonitorApiService {

    // see https://ev-monitor.com/web/devs/

    @FormUrlEncoded
    @POST("api/login")
    fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<LoginOutput>

    @FormUrlEncoded
    @POST("api/get_vehicles")
    fun vehicles(@Field("token") token: String): Call<VehiclesOutput>

    @FormUrlEncoded
    @POST("api/push_data")
    fun pushData(
        @Field("token") token: String,
        @FieldMap data: Map<String, @JvmSuppressWildcards Any>
    ): Call<PushDataOutput>

}

class EvMonitorApiServiceImpl {

    fun getService(context: Context): EvMonitorApiService {
        return getRetrofit(context).create(EvMonitorApiService::class.java)
    }

    private fun getRetrofit(context: Context): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.apply { interceptor.level = HttpLoggingInterceptor.Level.BODY }
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        // when service run in separate process with xml process config these don't get updated anymore, instead use onStartCommand(intent...)
        val prefs = Help.getSharedPrefs(context)
        val useHttps = prefs.getBoolean(context.getString(R.string.key_enable_evmonitor_https), true)
        val baseUrl = if (useHttps) "https://ev-monitor.com/" else "http://ev-monitor.com/"

        val gson = GsonBuilder().setLenient().create()
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

}
