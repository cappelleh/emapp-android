package be.hcpl.android.energica;


import static be.hcpl.android.energica.helpers.Help.launchView;
import static be.hcpl.android.energica.helpers.Help.setStateColorFor;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import be.hcpl.android.energica.services.ble.BleService;
import be.hcpl.android.energica.services.gps.GpsService;
import be.hcpl.android.energica.services.obd2.Obd2Service;
import be.hcpl.android.energica.ui.range.RangeFragment;

public class RangeActivity extends AppCompatActivity {

    private ImageView gpsStateView;
    private ImageView bleStateView;
    private ImageView obdStateView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // no need to keep screen on for this view
        setContentView(R.layout.activity_container);

        final Toolbar toolbar = findViewById(R.id.home_topbar);
        setSupportActionBar(toolbar);
        showInitialContent();

        // state indications
        gpsStateView = findViewById(R.id.gps_state);
        bleStateView = findViewById(R.id.ble_state);
        obdStateView = findViewById(R.id.obd_state);

        // handle navigation here
        findViewById(R.id.nav_dashboard).setOnClickListener(v -> launchView(this, MainActivity.class));
        findViewById(R.id.nav_range).setOnClickListener(v -> { /* ignore*/ });
        findViewById(R.id.nav_ble).setOnClickListener(v -> launchView(this, BleEnergicaActivity.class));
        findViewById(R.id.nav_obd).setOnClickListener(v -> launchView(this, Obd2Activity.class));
    }

    private void showInitialContent() {
        // load range calculation
        switchFragment(rangeFragment);
    }

    private final Fragment rangeFragment = new RangeFragment();

    private void switchFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

    private void updateServiceStates() {
        setStateColorFor(gpsStateView, GpsService.class);
        setStateColorFor(bleStateView, BleService.class);
        setStateColorFor(obdStateView, Obd2Service.class);
    }

    public void onResume() {
        super.onResume();
        // update state of services
        updateServiceStates();
    }

    // region menu

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.close_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle item selection
        if (item.getItemId() == R.id.close) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    // endregion

}