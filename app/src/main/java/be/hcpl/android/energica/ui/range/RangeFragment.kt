package be.hcpl.android.energica.ui.range

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatSeekBar
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import be.hcpl.android.energica.R

class RangeFragment : Fragment() {

    private lateinit var viewModel: RangeViewModel

    private lateinit var selectedCapacity: TextView
    private lateinit var calculatedRange: TextView
    private lateinit var sliderSoc: AppCompatSeekBar
    private lateinit var selectedSoc: TextView
    private lateinit var chargeTimeView: TextView

    // dynamically added views from parent.children
    private lateinit var capacityViews: List<TextView>
    private lateinit var roadViews: List<TextView>
    private lateinit var ridingViews: List<TextView>
    private lateinit var landscapeViews: List<TextView>
    private lateinit var weatherViews: List<TextView>
    private lateinit var chargerViews: List<TextView>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(this)[RangeViewModel::class.java]
        val root = inflater.inflate(R.layout.fragment_range, container, false)

        findAllViews(root)

        // receive updates
        viewModel.selectedBattery.observe(viewLifecycleOwner) {
            selectedCapacity.text =
                getString(R.string.selected_battery_is, it, viewModel.nominalForCapacity())
            updateChargeTime()
        }
        viewModel.calculatedRange.observe(viewLifecycleOwner) {
            calculatedRange.text = getString(R.string.range_value, it, it * 0.621371192)
        }
        viewModel.calculatedRange.observe(viewLifecycleOwner) {
            calculatedRange.text = getString(R.string.range_value, it, it * 0.621371192)
        }
        viewModel.selectedSoc.observe(viewLifecycleOwner) {
            selectedSoc.text = getString(R.string.label_percentage, it)
            updateChargeTime()
        }
        viewModel.selectedCharger.observe(viewLifecycleOwner) {
            updateChargeTime()
        }

        // perform updates
        sliderSoc.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                // ignore
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // ignore
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                viewModel.selectedSoc.value = progress.toDouble()
                viewModel.calculateRange()
                //updateChargeTime()
            }
        })
        // add listener to all found views
        capacityViews.forEach { textView ->
            textView.setOnClickListener {
                viewModel.selectedBattery.value = textView.text.toString()
                viewModel.calculateRange()
                // update color of all views back to default
                capacityViews.forEach { it.textColor(R.color.white) }
                // mark selection
                textView.textColor(R.color.colorAccent)
            }
        }
        roadViews.forEach { textView ->
            textView.setOnClickListener {
                viewModel.selectedRoad.value = textView.text.toString()
                viewModel.calculateRange()
                roadViews.forEach { it.textColor(R.color.white) }
                textView.textColor(R.color.colorAccent)
            }
        }
        ridingViews.forEach { textView ->
            textView.setOnClickListener {
                viewModel.selectedRiding.value = textView.text.toString()
                viewModel.calculateRange()
                ridingViews.forEach { it.textColor(R.color.white) }
                textView.textColor(R.color.colorAccent)
            }
        }
        landscapeViews.forEach { textView ->
            textView.setOnClickListener {
                viewModel.selectedLandscape.value = textView.text.toString()
                viewModel.calculateRange()
                landscapeViews.forEach { it.textColor(R.color.white) }
                textView.textColor(R.color.colorAccent)
            }
        }
        weatherViews.forEach { textView ->
            textView.setOnClickListener {
                viewModel.selectedWeather.value = textView.text.toString()
                viewModel.calculateRange()
                weatherViews.forEach { it.textColor(R.color.white) }
                textView.textColor(R.color.colorAccent)
            }
        }
        chargerViews.forEach { textView ->
            textView.setOnClickListener {
                viewModel.selectedCharger.value = Integer.parseInt(textView.text.toString())
                chargerViews.forEach { it.textColor(R.color.white) }
                textView.textColor(R.color.colorAccent)
            }
        }

        // init form
        viewModel.calculateRange()
        viewModel.selectedCharger.postValue(3) // defaults to 3 kW charger

        return root
    }

    private fun updateChargeTime() {
        // update values on screen
        chargeTimeView.text = getString(
            R.string.estimated_charge_time,
            viewModel.chargeTimeInHours(),
            viewModel.chargeTimeInMinutes()
        )
    }

    fun TextView.textColor(colorValue: Int) {
        this.setTextColor(getColor(context, colorValue))
    }

    private fun findAllViews(root: View) {
        selectedCapacity = root.findViewById(R.id.selected_battery)
        calculatedRange = root.findViewById(R.id.estimated_range_value)
        sliderSoc = root.findViewById(R.id.soc_slider)
        selectedSoc = root.findViewById(R.id.selected_soc)
        chargeTimeView = root.findViewById(R.id.estimated_charge_time)
        // dynamically added views
        capacityViews =
            (root.findViewById(R.id.capacities) as? ViewGroup)?.children?.filter { it is TextView }?.map { it as TextView }?.toList().orEmpty()
        roadViews = (root.findViewById(R.id.roads) as? ViewGroup)?.children?.filter { it is TextView }?.map { it as TextView }?.toList().orEmpty()
        ridingViews =
            (root.findViewById(R.id.ridingstyles) as? ViewGroup)?.children?.filter { it is TextView }?.map { it as TextView }?.toList().orEmpty()
        landscapeViews =
            (root.findViewById(R.id.landscapes) as? ViewGroup)?.children?.filter { it is TextView }?.map { it as TextView }?.toList().orEmpty()
        weatherViews =
            (root.findViewById(R.id.weather) as? ViewGroup)?.children?.filter { it is TextView }?.map { it as TextView }?.toList().orEmpty()
        chargerViews =
            (root.findViewById(R.id.chargers) as? ViewGroup)?.children?.filter { it is TextView }?.map { it as TextView }?.toList().orEmpty()
    }

}
