package be.hcpl.android.energica

import android.os.Build
import android.os.Bundle
import android.os.FileObserver
import android.text.method.ScrollingMovementMethod
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import be.hcpl.android.energica.helpers.ExportData
import be.hcpl.android.energica.helpers.Help.setStateColorFor
import be.hcpl.android.energica.services.ble.BleService
import be.hcpl.android.energica.services.gps.GpsService
import be.hcpl.android.energica.services.obd2.Obd2Service

class AppLogsActivity : AppCompatActivity(R.layout.activity_data) {

    // toolbar state icons
    private lateinit var gpsStateView: ImageView
    private lateinit var bleStateView: ImageView
    private lateinit var obdStateView: ImageView

    private lateinit var logsView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // find views
        val toolbar: Toolbar = findViewById(R.id.home_topbar)
        val titleView: TextView = findViewById(R.id.toolbar_title)
        gpsStateView = findViewById(R.id.gps_state)
        bleStateView = findViewById(R.id.ble_state)
        obdStateView = findViewById(R.id.obd_state)
        logsView = findViewById(R.id.data_logs)
        val logsLabelView: TextView = findViewById(R.id.logs_label)
        val refreshView: View = findViewById(R.id.refresh)

        // handle toolbar
        setSupportActionBar(toolbar)
        titleView.text = getString(R.string.app_logs)

        // stats and app logs
        logsView.movementMethod = ScrollingMovementMethod()

        // show app version
        logsLabelView.text = String.format(getString(R.string.app_logs_for_app_version_s_build_d),
            BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE)

        // monitor app logs here
        val logFile = ExportData.getCurrentAppLogFile(applicationContext)
        if (logFile?.exists() == false) {
            logFile.createNewFile()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && logFile != null) {
            val observer = object : FileObserver(logFile) {
                override fun onEvent(state: Int, text: String?) {
                    //logsView.append(text)
                    if (state == CLOSE_WRITE || state == CLOSE_NOWRITE) {
                        runOnUiThread { logsView.text = logFile.readText() }
                    }
                }
            }
            observer.startWatching() // TODO need to stop observing on pause and start again on resume?
        } else {
            logsView.text = getString(R.string.no_file_observer_support)
        }

        // manual logs refresh option here
        refreshView.setOnClickListener { updateLogs() }
    }

    // region lifecycle

    override fun onResume() {
        super.onResume()
        // update state of services
        updateServiceStates()
        updateLogs()
    }

    private fun updateLogs() {
        // load logs
        val logFile = ExportData.getCurrentAppLogFile(applicationContext)
        logsView.text = logFile?.readText().orEmpty()
    }

    // endregion

    // region stats

    private fun updateServiceStates() {
        setStateColorFor(gpsStateView, GpsService::class.java)
        setStateColorFor(bleStateView, BleService::class.java)
        setStateColorFor(obdStateView, Obd2Service::class.java)
    }

    // endregion

    // region menu

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.close_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.close -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    // endregion

}