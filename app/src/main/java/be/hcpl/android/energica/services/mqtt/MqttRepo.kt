package be.hcpl.android.energica.services.mqtt

import android.content.Context
import be.hcpl.android.energica.R
import be.hcpl.android.energica.helpers.ExportData.Companion.log
import be.hcpl.android.energica.helpers.Help
import be.hcpl.android.energica.model.evmonitor.PushDataInput
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MqttRepo(private val context: Context) {

    // DONE cache data on local storage if push fails? since we piggy back on EvMonitorRepo for that
    private var service: MqttApiService = MqttApiServiceImpl().getService()
    private val prefs = Help.getSharedPrefs(context)
    private var enabled = DEFAULT_ENABLED
    private var url = DEFAULT_HA_URL
    private var token = DEFAULT_HA_TOKEN

    companion object {

        private const val DEFAULT_ENABLED = false
        private const val DEFAULT_HA_URL = "http://homeassistant.local:8123/api/services/mqtt/publish"
        private const val DEFAULT_HA_TOKEN = "MISSING_TOKEN"

    }

    init {
        // get initial values from settings here once
        updateConfig()
    }

    private fun updateConfig() {
        enabled = prefs.getBoolean(context.getString(R.string.key_enable_mqtt), DEFAULT_ENABLED)
        url = prefs.getString(context.getString(R.string.key_mqtt_url), null) ?: DEFAULT_HA_URL
        token = prefs.getString(context.getString(R.string.key_mqtt_token), null) ?: DEFAULT_HA_TOKEN
    }

    /**
     * force push all data to remote, use this on finishing services for example
     * All push___ functions redirect to this single flush
     */
    fun flushData(dataSet: List<PushDataInput>) {
        // always work on latest config
        updateConfig()
        // don't push if not enabled
        if (!enabled) {
            return
        }
        // don't push anything if we have nothing at this point
        if (dataSet.isEmpty()) {
            log(context, "MQTT publish data failed, no data recorded")
            return
        }
        // push all data we've collected at this point
        dataSet.forEach { input ->
            checkValue(input.battSoc) { pushData(url, "Bearer $token", MqttPushSoc(payload = input.battSoc)) }
            checkValue(input.range) { pushData(url, "Bearer $token", MqttPushRange(payload = input.range)) }
            checkValue(input.odometer) { pushData(url, "Bearer $token", MqttPushOdometer(payload = input.odometer)) }
            checkValue(input.reserve) { pushData(url, "Bearer $token", MqttPushReserveWh(payload = input.reserve)) }
            checkValue(input.battTemp) { pushData(url, "Bearer $token", MqttPushBatteryTemp(payload = input.battTemp)) }
            checkValue(input.chargeCurrent) { pushData(url, "Bearer $token", MqttPushChargeCurrent(payload = input.chargeCurrent)) }
            checkValue(input.chargePower) { pushData(url, "Bearer $token", MqttPushChargePower(payload = input.chargePower)) }
            checkValue(input.battVoltage) { pushData(url, "Bearer $token", MqttPushBatteryVoltage(payload = input.battVoltage)) }
            checkValue(input.cellMin) { pushData(url, "Bearer $token", MqttPushMinCellVoltage(payload = input.cellMin)) }
            checkValue(input.cellMax) { pushData(url, "Bearer $token", MqttPushMaxCellVoltage(payload = input.cellMax)) }
        }

    }

    private fun checkValue(value: Number?, next: () -> Unit){
        if (value != null && value != 0) next()
    }

    private fun pushData(url: String, token: String, data: MqttPushData) {
        service.pushData(url, token, data).enqueue(object : Callback<Unit> {

            override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                if (!response.isSuccessful) {
                    log(context, "MQTT publish data failed, check config")
                    return
                }
            }

            override fun onFailure(call: Call<Unit>, t: Throwable) {
                log(context, "MQTT publish data failed, check config")
            }
        })
    }

}