package be.hcpl.android.energica;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import be.hcpl.android.energica.ui.map.MapFragment;
import be.hcpl.android.energica.helpers.Const;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.helpers.Help;
import be.hcpl.android.energica.services.ServiceExtensionsKt;
import be.hcpl.android.energica.services.ble.BleService;
import be.hcpl.android.energica.services.evmonitor.EvMonitorRepo;
import be.hcpl.android.energica.services.gps.GpsService;
import be.hcpl.android.energica.services.obd2.Obd2Service;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.BLUETOOTH_CONNECT;
import static android.Manifest.permission.BLUETOOTH_SCAN;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

import static be.hcpl.android.energica.helpers.Help.doAfterConfirmation;
import static be.hcpl.android.energica.helpers.Help.launchView;
import static be.hcpl.android.energica.helpers.Help.setStateColorFor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private ImageView gpsStateView;
    private ImageView bleStateView;
    private ImageView obdStateView;

    private void log(String message) {
        ExportData.Companion.log(getApplicationContext(), message);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // allow keeping app awake
        // TODO doesn't work with setShowWhenLocked, check alternative
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
        //    setShowWhenLocked(true);
        //} else {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        //}
        setContentView(R.layout.activity_container);

        SharedPreferences prefs = Help.getSharedPrefs(this);

        final Toolbar toolbar = findViewById(R.id.home_topbar);
        setSupportActionBar(toolbar);

        // Only show the first time the app has started
        // https://stackoverflow.com/questions/4636141/determine-if-android-app-is-being-used-for-the-first-time
        if (prefs.getString("version", "").isEmpty()) {
            // this initial app view is responsible for checking all permissions
            // have a message about tracking BEFORE location permission request
            final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
            builder.setMessage("This app collects location data to enable route tracking and gpx route exports even when the app is closed or not in use.");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(dialog -> checkBluetoothAndPermissions());
            builder.show();
            // Don't bother later
            prefs.edit()
                    .putString("version", BuildConfig.VERSION_NAME + "-" + BuildConfig.BUILD_TYPE)
                    .apply();
        } else {
            checkBluetoothAndPermissions();
        }

        // state indications
        gpsStateView = findViewById(R.id.gps_state);
        gpsStateView.setOnClickListener(view -> toggleService(GpsService.class));
        bleStateView = findViewById(R.id.ble_state);
        bleStateView.setOnClickListener(view -> toggleService(BleService.class));
        obdStateView = findViewById(R.id.obd_state);
        obdStateView.setOnClickListener(view -> toggleService(Obd2Service.class));

        // moved to onResume
        showInitialContent();

        // handle navigation here
        findViewById(R.id.nav_dashboard).setOnClickListener(v -> {/* ignore */});
        findViewById(R.id.nav_range).setOnClickListener(v -> launchView(this, RangeActivity.class));
        findViewById(R.id.nav_ble).setOnClickListener(v -> launchView(this, BleEnergicaActivity.class));
        findViewById(R.id.nav_obd).setOnClickListener(v -> launchView(this, Obd2Activity.class));
    }

    private void showInitialContent() {
        // load map vew for gps functionality
        switchFragment(dashboardFragment);
    }

    private final MapFragment dashboardFragment = new MapFragment();

    private void switchFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

    // region broadcast messages from service for state handling

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            updateServiceStates();
        }
    };

    private boolean gpsServiceRunning;
    private boolean bleServiceRunning;
    private boolean obd2ServiceRunning;
    private Messenger bleServiceMessenger;

    private final ServiceConnection gpsServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            log("GPS - onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            log("GPS - onServiceDisconnected");
        }
    };

    private final ServiceConnection bleServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            bleServiceMessenger = new Messenger(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bleServiceMessenger = null;
        }
    };

    private final ServiceConnection obd2ServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        }
    };

    private void updateServiceStates() {
        gpsServiceRunning = ServiceExtensionsKt.isServiceRunning(GpsService.class, this);
        bleServiceRunning = ServiceExtensionsKt.isServiceRunning(BleService.class, this);
        obd2ServiceRunning = ServiceExtensionsKt.isServiceRunning(Obd2Service.class, this);
        setStateColorFor(gpsStateView, GpsService.class);
        setStateColorFor(bleStateView, BleService.class);
        setStateColorFor(obdStateView, Obd2Service.class);
    }

    private void toggleService(Class<?> serviceClass) {
        try {
            if (serviceClass == GpsService.class) {
                if (gpsServiceRunning) {
                    disableGpsLocationUpdates();
                } else {
                    enableGpsLocationUpdates();
                }
            } else if (serviceClass == BleService.class) {
                if (bleServiceRunning) {
                    if (bleServiceMessenger != null)
                        bleServiceMessenger.send(Message.obtain(null, BleService.MANUALLY_STOPPED, 0, 0));
                    ServiceExtensionsKt.stopService(BleService.class, this, bleServiceConnection);
                    BleService.Companion.setRunning(false);
                } else {
                    if (bleServiceMessenger != null)
                        bleServiceMessenger.send(Message.obtain(null, BleService.MANUALLY_STARTED, 0, 0));
                    ServiceExtensionsKt.startService(BleService.class, this, bleServiceConnection);
                }
            } else if (serviceClass == Obd2Service.class) {
                if (obd2ServiceRunning) {
                    ServiceExtensionsKt.stopService(Obd2Service.class, this, obd2ServiceConnection);
                    Obd2Service.running = false;
                } else {
                    ServiceExtensionsKt.startService(Obd2Service.class, this, obd2ServiceConnection);
                }
            }
        } catch (Exception e) {
            log(String.format("error starting or stopping background service %s", e.getMessage()));
        }
    }

    // endregion

    // region menu

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == R.id.start_tracking) {
            enableGpsLocationUpdates();
            log("started tracking from menu");
            return true;
        } else if (item.getItemId() == R.id.stop_tracking) {
            disableGpsLocationUpdates();
            log("tracking stopped from menu");
            return true;
        } else if (item.getItemId() == R.id.settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (item.getItemId() == R.id.logs) {
            startActivity(new Intent(this, AppLogsActivity.class));
            return true;
        } else if (item.getItemId() == R.id.flush) {
            // moved to thread to prevent blocked UI
            new Thread(() -> EvMonitorRepo.Companion.instance(getApplicationContext()).flushData()).start();
            return true;
        } else if (item.getItemId() == R.id.reload) {
            reloadRouteAfterConfirmation();
            return true;
        } else if (item.getItemId() == R.id.drop) {
            dropAllDataAfterConfirmation();
            return true;
        } else if (item.getItemId() == R.id.about) {
            startActivity(new Intent(this, AboutActivity.class));
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void dropAllDataAfterConfirmation() {
        doAfterConfirmation(MainActivity.this,
                R.string.confirm_dropping_data,
                (dialog, id) -> EvMonitorRepo.Companion.instance(this).dropData()
        );
    }

    private void reloadRouteAfterConfirmation() {
        doAfterConfirmation(MainActivity.this,
                R.string.confirm_load_route_from_cache,
                (dialog, id) -> dashboardFragment.loadDataFromCache()
        );
    }

    // endregion

    // region lifecycle handling

    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    public void onResume() {
        super.onResume();
        // connect to service for broadcast messages
        IntentFilter filter = new IntentFilter(Const.BROADCAST_FILTER);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(broadcastReceiver, filter, RECEIVER_EXPORTED);
        } else {
            registerReceiver(broadcastReceiver, filter);
        }
        // check if gps setting was disabled to stop gps tracking
        //if(prefs.getBoolean(getString(R.string.key_use_device_gps), false)){
        //    disableGpsLocationUpdates();
        //}
        // update state of services
        updateServiceStates();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    // endregion

    // region permission handling

    static class PermissionRequest {

        private final String permission;
        private final String rationale;
        private final String failedMessage;

        public PermissionRequest(
                final String permission,
                final String rationale,
                final String failedMessage) {

            this.permission = permission;
            this.rationale = rationale;
            this.failedMessage = failedMessage;

        }
    }

    private final Map<String, PermissionRequest> permissionRequests = new HashMap<>();

    private void checkBluetoothAndPermissions() {
        // check all permissions first
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            permissionRequests.put(BLUETOOTH_SCAN, new PermissionRequest(BLUETOOTH_SCAN, "Bluetooth scanning permission is used to discover BLE devices", "Without bluetooth scanning permission we can't look for bluetooth devices"));
            permissionRequests.put(BLUETOOTH_CONNECT, new PermissionRequest(BLUETOOTH_CONNECT, "Bluetooth connect permission is used to connect to BLE device", "Without bluetooth connect permission we can't connect to bluetooth devices"));
        }
        permissionRequests.put(ACCESS_FINE_LOCATION, new PermissionRequest(ACCESS_FINE_LOCATION, "Location permission is used to track GPS location and routes", "Without location permission we can 't record routes"));
        checkPermissions(permissionRequests.values().toArray(new PermissionRequest[]{}));
    }

    @SuppressLint("MissingPermission")
    private void checkPermissions(PermissionRequest[] permissions) {
        final List<String> missingPermissions = new ArrayList<>();
        for (PermissionRequest permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission.permission) == PackageManager.PERMISSION_GRANTED) {
                // You can use the API that requires the permission.
                log("Permission " + permission.permission + " already granted");
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                    && shouldShowRequestPermissionRationale(permission.permission)) {
                // In an educational UI, explain to the user why your app requires this
                // permission for a specific feature to behave as expected.
                log("Permission " + permission.permission + " rationale triggered");
                final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                builder.setMessage(permission.rationale);
                builder.setPositiveButton(android.R.string.ok, null).show();
            } else {
                // You can directly ask for the permission.
                log("Permission " + permission.permission + " requested");
                // The registered ActivityResultCallback gets the result of this request.
                missingPermissions.add(permission.permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            requestMultiplePermissionLauncher.launch(missingPermissions.toArray(new String[]{}));
        }
        if (missingPermissions.isEmpty() || !containsBluetoothPermissions(missingPermissions.toArray(new String[]{}))) {
            // only when we have all permissions granted can we continue with bluetooth connection
            // BLUETOOTH_CONNECT is required before we can handle this
            final BluetoothManager btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            final BluetoothAdapter btAdapter = btManager.getAdapter();
            if (btAdapter != null && !btAdapter.isEnabled()) {
                startActivity(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE));
                log("bluetooth disabled on device, check settings");
            }
        }
    }

    private boolean containsBluetoothPermissions(String[] permissions) {
        for (final String permission : permissions) {
            if (permission.equals(BLUETOOTH_CONNECT) || permission.equals(BLUETOOTH_SCAN) || permission.equals(ACCESS_FINE_LOCATION)) {
                return true;
            }
        }
        return false;
    }

    // Register the permissions callback, which handles the user's response to the
    // system permissions dialog. Save the return value, an instance of
    // ActivityResultLauncher, as an instance variable.
    private final ActivityResultLauncher<String[]> requestMultiplePermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), isGranted -> {
                for (String key : isGranted.keySet()) {
                    Boolean granted = isGranted.get(key);
                    if (granted != null && granted) {
                        log("Permission " + key + " is granted. Continue the action or workflow in your app.");
                    } else {
                        // Explain to the user that the feature is unavailable because ...
                        PermissionRequest permReq = permissionRequests.get(key);
                        if (permReq == null) {
                            permReq = new PermissionRequest(key, "", "(unknown permission)");
                        }
                        log("Permission " + permReq.permission + " denied");
                        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
                        builder.setMessage(permReq.failedMessage);
                        builder.setPositiveButton(android.R.string.ok, null).show();
                    }
                }
            });

    // endregion

    // region GPS service logic

    private boolean locationEnabled;

    private void enableGpsLocationUpdates() {
        // no need to enable if already enabled
        if (locationEnabled) {
            return;
        }
        // check for permissions
        if (ActivityCompat.checkSelfPermission(
                MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {

            Toast.makeText(MainActivity.this, "App doesn't have location permissions", Toast.LENGTH_SHORT).show();
            checkBluetoothAndPermissions();
            return;
        }
        locationEnabled = true;
        gpsStateView.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.fluo_green, null));
        ServiceExtensionsKt.startService(GpsService.class, this, gpsServiceConnection);
    }

    private void disableGpsLocationUpdates() {
        locationEnabled = false;
        gpsStateView.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));
        ServiceExtensionsKt.stopService(GpsService.class, this, gpsServiceConnection);
        GpsService.running = false;
    }

    // endregion

}