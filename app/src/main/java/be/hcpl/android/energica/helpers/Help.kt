package be.hcpl.android.energica.helpers

import android.app.Service
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.preference.PreferenceManager
import be.hcpl.android.energica.R
import be.hcpl.android.energica.helpers.Const.DISTANCE_UNIT_MI
import be.hcpl.android.energica.services.isServiceRunning
import kotlin.math.abs
import kotlin.math.atan2
import kotlin.math.ln
import kotlin.math.roundToInt
import kotlin.math.tan

object Help {

    @JvmStatic
    fun withColor(context: Context, colorResource: Int) = ResourcesCompat.getColor(context.resources, colorResource, null)

    @JvmStatic
    fun withStateColor(context: Context, state: Boolean) = withColor(context, if (state) R.color.fluo_green else R.color.colorAccent)

    @JvmStatic
    fun setStateColorFor(view: ImageView, serviceClass: Class<out Service?>) {
        view.setColorFilter(withStateColor(view.context, serviceClass.isServiceRunning(view.context)))
    }

    private fun toDegrees(radians: Double): Double {
        return 57.29577951308232 * radians
    }

    private fun toRadians(degrees: Double): Double {
        return 0.017453292519943295 * degrees
    }

    @JvmStatic
    fun getBearing(lat1: Double, long1: Double, lat2: Double, long2: Double): Int {
        val startLat = toRadians(lat1)
        val startLong = toRadians(long1)
        val endLat = toRadians(lat2)
        var dLong = toRadians(long2) - startLong
        val dPhi = ln(tan(endLat / 2.0 + 0.7853981633974483) / tan(startLat / 2.0 + 0.7853981633974483))
        if (abs(dLong) > 3.141592653589793) {
            if (dLong > 0.0) {
                dLong = -(6.283185307179586 - dLong)
            } else {
                dLong += 6.283185307179586
            }
        }
        return ((360.0 + toDegrees(atan2(dLong, dPhi))) % 360.0).roundToInt()
    }

    @JvmStatic
    fun getConsumptionUnit(unit: Int): String {
        return when (unit) {
            Const.CONSUMPTION_UNIT_KM_KWH -> "km/kWh"
            Const.CONSUMPTION_UNIT_WH_KM -> "Wh/km"
            Const.CONSUMPTION_UNIT_KWH_100MI -> "kWh/100 mi"
            Const.CONSUMPTION_UNIT_MI_KWH -> "mi/kWh"
            Const.CONSUMPTION_UNIT_WH_MI -> "Wh/mi"
            Const.CONSUMPTION_UNIT_MPG -> "MPGe"
            else -> "kWh/100 km"
        }
    }

    @JvmStatic
    fun getConsumptionUnitUrlSafe(unit: Int): String {
        return when (unit) {
            Const.CONSUMPTION_UNIT_KM_KWH -> "km-kWh"
            Const.CONSUMPTION_UNIT_WH_KM -> "Wh-km"
            Const.CONSUMPTION_UNIT_KWH_100MI -> "kWh-100mi"
            Const.CONSUMPTION_UNIT_MI_KWH -> "mi-kWh"
            Const.CONSUMPTION_UNIT_WH_MI -> "Wh-mi"
            Const.CONSUMPTION_UNIT_MPG -> "MPGe"
            else -> "kWh-100km"
        }
    }

    @JvmStatic
    fun getDistanceUnit(unit: Int) = if (unit == DISTANCE_UNIT_MI) "mi" else "km"

    @JvmStatic
    fun getSharedPrefs(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @JvmStatic
    fun launchView(context: Context, activityClass: Class<out AppCompatActivity?>) {
        val intent = Intent(context, activityClass)
        //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP) // keeps the same view on top so graphs and maps are kept
        context.startActivity(intent)
    }

    @JvmStatic
    fun doAfterConfirmation(
        context: Context,
        message: Int,
        onConfirmation: DialogInterface.OnClickListener
    ): AlertDialog? {
        return AlertDialog.Builder(context, R.style.AlertDialogStyle)
            .setMessage(message)
            .setPositiveButton(android.R.string.yes, onConfirmation)
            .setNegativeButton(android.R.string.no, null).show()
    }

}
