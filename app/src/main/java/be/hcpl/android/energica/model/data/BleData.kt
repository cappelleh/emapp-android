package be.hcpl.android.energica.model.data

import be.hcpl.android.energica.model.ble.VehicleStatus
import java.io.Serializable

data class BleData(val selectedUnit: Int, val status: VehicleStatus) : Serializable
