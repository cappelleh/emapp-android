package be.hcpl.android.energica.config

import com.github.pires.obd.commands.protocol.ObdRawCommand

fun ObdRawCommand.int(start: Int, end: Int): Int = if (result != null) Integer.decode("0x" + result.subSequence(start, end)) else 0

fun ObdRawCommand.signedDouble(start: Int, end: Int): Double = if (result != null) result.subSequence(start,end).toString().toInt(16).toDouble() else 0.0
