package be.hcpl.android.energica.ui.range

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class RangeViewModel : ViewModel() {

    val selectedBattery: MutableLiveData<String> by lazy { MutableLiveData<String>("14.4") }
    val selectedSoc: MutableLiveData<Double> by lazy { MutableLiveData<Double>(85.0) }
    val selectedRoad: MutableLiveData<String> by lazy { MutableLiveData<String>("Road") }
    val selectedRiding: MutableLiveData<String> by lazy { MutableLiveData<String>("Conservative") }
    val selectedLandscape: MutableLiveData<String> by lazy { MutableLiveData<String>("Flat") }
    val selectedWeather: MutableLiveData<String> by lazy { MutableLiveData<String>("Mild") }

    val calculatedRange: MutableLiveData<Double> by lazy { MutableLiveData<Double>() }
    val selectedCharger: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }

    fun calculateRange() {
        val soc: Double = selectedSoc.value!! / 100 // soc percentage
        // given range for road minus penalties
        val result =
            capacities[selectedBattery.value]!! * rangeValues[selectedRoad.value]!! * soc * ridingStylePenalties[selectedRiding.value]!! * landscapePenalties[selectedLandscape.value]!! * weatherPenalties[selectedWeather.value]!! * energicaCorrection()
        calculatedRange.postValue(result)
    }

    private fun energicaCorrection() = if (selectedBattery.value == "13.4" || selectedBattery.value == "21.5") 0.9 else 1.0

    fun nominalForCapacity() = nominal[selectedBattery.value]

    fun chargeTimeInHours() = secondsToHoursMinutesSeconds((calculateChargeTime() * 60).toInt()).first

    fun chargeTimeInMinutes() = secondsToHoursMinutesSeconds((calculateChargeTime() * 60).toInt()).second

    private fun calculateChargeTime(): Double {
        val soc: Double = selectedSoc.value!! / 100
        val nominalCapacity = nominal[selectedBattery.value]!!
        val amountToChargeInkWh = nominalCapacity - soc * nominalCapacity
        // either from full capacity * 0.85 or from nominal capacity
        val charger = selectedCharger.value?.toDouble() ?: 3.0
        return (amountToChargeInkWh / charger) * 60 // in minutes
    }

    private fun secondsToHoursMinutesSeconds(seconds: Int) = Pair(seconds / 3600, (seconds % 3600) / 60)//, (seconds % 3600) % 60)

    private val capacities: Map<String, Double> = mapOf(
        "7.2" to 7.2,
        "13.4" to 13.4,
        "14.4" to 14.4,
        "15.5" to 15.5,
        "18.0" to 18.0,
        "21.5" to 21.5
    ) // battery capacities in doubles

    private val nominal: Map<String, Double> = mapOf(
        "7.2" to 6.3,
        "13.4" to 11.7,
        "14.4" to 12.6,
        "15.5" to 13.6,
        "18.0" to 15.8,
        "21.5" to 18.9
    ) // nominal battery capacities

    private val rangeValues: Map<String, Double> = mapOf(
        "City" to 19.93,
        "City/Road" to 15.0,
        "City/Highway" to 13.4,
        "Road" to 12.08,
        "Highway" to 9.93
    ) // full battery range per kWh

    private val ridingStylePenalties: Map<String, Double> = mapOf("Conservative" to 1.0, "Aggressive" to 0.9)
    private val landscapePenalties: Map<String, Double> = mapOf("Flat" to 1.0, "Hills" to 0.9)
    private val weatherPenalties: Map<String, Double> = mapOf("Cold" to 0.9, "Mild" to 0.95, "Warm" to 1.0)

}
