package be.hcpl.android.energica.model.ble;

import java.util.UUID;

public class BleDefinedUUIDs {

    public static class Characteristic {
        public static final UUID ENERGICA_NOTIFY = UUID.fromString("0734594B-A8E8-4B1B-A6B2-CD5243059A58");
        public static final UUID ENERGICA_WRITE = UUID.fromString("8B00ACE8-EB0C-49B1-BBEA-9AEE0A26E1A4");
    }

    public static class Descriptor {
        public static final UUID CHAR_CLIENT_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    }

}
