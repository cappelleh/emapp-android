package be.hcpl.android.energica.ui.ble;

import static android.content.Context.RECEIVER_EXPORTED;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.Serializable;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import be.hcpl.android.energica.BleEnergicaActivity;
import be.hcpl.android.energica.R;
import be.hcpl.android.energica.model.data.BleData;
import be.hcpl.android.energica.model.data.ChargeLimit;
import be.hcpl.android.energica.helpers.Const;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.model.ble.VehicleStatus;
import be.hcpl.android.energica.services.ble.ConnectionManager;

import static be.hcpl.android.energica.helpers.Const.BLE_EVENT;
import static be.hcpl.android.energica.helpers.Const.BLE_GENERIC_EVENT;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_EVENT;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_GENERIC_EVENT;
import static be.hcpl.android.energica.helpers.Const.STATE_CHARGE;

public class ChargeFragment extends Fragment {

    public static final DataPoint[] EMPTY_SET = new DataPoint[]{new DataPoint(0, 0)};

    private BleEnergicaActivity context;

    private TextView currentLimitView;
    private TextView rangeView;
    private TextView consumptionView;
    private TextView chargeLevel;
    private TextView battTemp;
    private TextView chargePower;
    private TextView packI;
    private TextView packV;
    private TextView resBatteryEnergy;
    private GraphView graphView1;
    private GraphView graphView2;

    private final LineGraphSeries<DataPoint> powerSeries = new LineGraphSeries<>(EMPTY_SET);
    private final LineGraphSeries<DataPoint> tempSeries = new LineGraphSeries<>(EMPTY_SET);
    private final LineGraphSeries<DataPoint> powerSeries2 = new LineGraphSeries<>(EMPTY_SET);
    private final LineGraphSeries<DataPoint> tempSeries2 = new LineGraphSeries<>(EMPTY_SET);
    private boolean scaledForAC = false;

    // region broadcastreceiver for getting data from service running in background

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String event = intent.getStringExtra(Const.BROADCAST_EVENT);
            if (BROADCAST_BLE_EVENT.equals(event)) {
                // receive BLE updates
                final BleData data = (BleData) intent.getSerializableExtra(BLE_EVENT);
                if( data != null ) onBleDataEvent(data);
            } else if (BROADCAST_BLE_GENERIC_EVENT.equals(event)) {
                // temp solution posting generic events like resetConnection and more
                final Serializable data = intent.getSerializableExtra(BLE_GENERIC_EVENT);
                if( data instanceof ChargeLimit){
                    onChargeLimit((ChargeLimit) data);
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        context.unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    private void onBleDataEvent(final BleData event){
        updateValues(event.getStatus());
    }

    // endregion

    @SuppressLint({"StringFormatInvalid", "UnspecifiedRegisterReceiverFlag"})
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {

        context = (BleEnergicaActivity) getActivity();
        final View rootView = inflater.inflate(R.layout.fragment_ble_charge, container, false);

        // listen for broadcast updates
        IntentFilter filter = new IntentFilter(Const.BROADCAST_FILTER);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            context.registerReceiver(broadcastReceiver, filter, RECEIVER_EXPORTED);
        } else {
            context.registerReceiver(broadcastReceiver, filter);
        }

        battTemp = rootView.findViewById(R.id.batt_temp);
        chargePower = rootView.findViewById(R.id.power_limit_value);
        chargeLevel = rootView.findViewById(R.id.charge_level_view);
        currentLimitView = rootView.findViewById(R.id.set_charge_limit);
        rangeView = rootView.findViewById(R.id.range);
        consumptionView = rootView.findViewById(R.id.avg_consumption);
        packI = rootView.findViewById(R.id.pack_i_value);
        packV = rootView.findViewById(R.id.pack_v_value);
        resBatteryEnergy = rootView.findViewById(R.id.res_battery_energy_value);

        final Button stopChargeView = rootView.findViewById(R.id.shutdown_btn);
        final Button resetGraph = rootView.findViewById(R.id.reset_graph);
        final Button scaleGraph = rootView.findViewById(R.id.scale_graph);

        // have live updating graph for charge power and soc over time
        graphView1 = rootView.findViewById(R.id.graph1);
        initStyleGraph(getContext(), graphView1);
        graphView1.addSeries(powerSeries);
        graphView1.addSeries(tempSeries);
        // set colors on series same to color of labels
        powerSeries.setColor(getColor(R.color.fluo_green));
        tempSeries.setColor(getColor(R.color.colorPrimary));

        // a second graph to plot based on SOC level
        graphView2 = rootView.findViewById(R.id.graph2);
        initStyleGraph(getContext(), graphView2);
        graphView2.getGridLabelRenderer().setHorizontalAxisTitleColor(getColor(R.color.colorAccent));
        graphView2.getGridLabelRenderer().setHorizontalLabelsColor(getColor(R.color.colorAccent));
        graphView2.getGridLabelRenderer().setHorizontalLabelsVisible(true); // show SOC
        graphView2.addSeries(tempSeries2);
        graphView2.addSeries(powerSeries2);
        // set colors on series same to color of labels
        powerSeries2.setColor(getColor(R.color.fluo_green));
        tempSeries2.setColor(getColor(R.color.colorPrimary));

        if (scaleGraph != null)
            scaleGraph.setOnClickListener(v -> {
            if (scaledForAC) {
                scaledForAC = false;
                if( graphView1 != null && graphView1.getViewport() != null ) graphView1.getViewport().setMaxY(25);
                if( graphView2 != null && graphView2.getViewport() != null ) graphView2.getViewport().setMaxY(25);
            } else {
                scaledForAC = true;
                if( graphView1 != null && graphView1.getViewport() != null ) graphView1.getViewport().setMaxY(4);
                if( graphView2 != null && graphView2.getViewport() != null ) graphView2.getViewport().setMaxY(4);
            }
        });
        if (resetGraph != null)
            resetGraph.setOnClickListener(v -> new AlertDialog.Builder(getContext(), R.style.AlertDialogStyle)
                    .setMessage("CLEAR Graphs?")
                    .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                        // just resets both graphs to empty
                        powerSeries.resetData(EMPTY_SET);
                        tempSeries.resetData(EMPTY_SET);
                        powerSeries2.resetData(EMPTY_SET);
                        tempSeries2.resetData(EMPTY_SET);
                        lastSocPlotted = 0;
                        lastTimePlotted = 0;
                        log("charge graphs cleared");
                    })
                    .setNegativeButton(android.R.string.no, null).show());
        final View exportView = rootView.findViewById(R.id.export_data);
        if (exportView != null)
            exportView.setOnClickListener(view -> {
                ExportData.Companion.exportData(context, powerSeries, "charge-power-time", 0, lastTimePlotted);
                ExportData.Companion.exportData(context, powerSeries2, "charge-power-soc", 0, lastTimePlotted);
                ExportData.Companion.exportData(context, tempSeries, "temp-time", 0, lastSocPlotted);
                ExportData.Companion.exportData(context, tempSeries2, "temp-soc", 0, lastSocPlotted);
                Toast.makeText(context, "data exported", Toast.LENGTH_SHORT).show();
            });
        // removed input for charge limit config
        if (stopChargeView != null)
            stopChargeView.setOnClickListener(v -> {
            if (ConnectionManager.getInstance(context).getCurrentStatus().state == STATE_CHARGE) {
                new AlertDialog.Builder(getContext(), R.style.AlertDialogStyle)
                        .setMessage(R.string.ChgTermConfirm).setCancelable(true)
                        .setPositiveButton(R.string.optionYes, (dialog, id) -> {
                            // sends stop charging command
                            if( context != null ) context.setChgTermination();
                            log("stop charge confirmed and requested");
                            // informs user
                            Toast.makeText(context, getString(R.string.message_disconnect_charger),
                                           Toast.LENGTH_SHORT).show();
                        }).setNegativeButton(R.string.optionNo, (dialog, id) -> dialog.cancel())
                .show();
            } else {
                Toast.makeText(getContext(), "not charging", Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }

    private void log(final String message) {
        if (context != null ) ExportData.Companion.log(context, message);
    }

    private int getColor(int colorResId){
        return ResourcesCompat.getColor(getResources(), colorResId, null);
    }

    private int lastTimePlotted = 0;
    private int lastSocPlotted = 0;

    public static void initStyleGraph(final Context context, final GraphView graphView) {
        graphView.setVisibility(View.VISIBLE);
        graphView.getGridLabelRenderer().setGridColor(ContextCompat.getColor(context, R.color.dark_white));
        graphView.getGridLabelRenderer().setVerticalAxisTitleColor(ContextCompat.getColor(context, R.color.dark_white));
        graphView.getGridLabelRenderer().setVerticalLabelsColor(ContextCompat.getColor(context, R.color.dark_white));
        graphView.getGridLabelRenderer().setHorizontalAxisTitleColor(ContextCompat.getColor(context, R.color.dark_white));
        graphView.getGridLabelRenderer().setHorizontalLabelsColor(ContextCompat.getColor(context, R.color.dark_white));
        graphView.getGridLabelRenderer().setHorizontalLabelsVisible(false); // hide time counter by default
        // don't autoscale viewport, charge power is max 24 kW
        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getViewport().setYAxisBoundsManual(true);
        graphView.getViewport().setMinX(0);
        graphView.getViewport().setMaxX(100); // display up to 100% SOC
        graphView.getViewport().setMinY(0);
        graphView.getViewport().setMaxY(25);// max charge power 24 kW on CCS DC
    }

    public void onChargeLimit(ChargeLimit event) {
        if( !isAdded() ) return;
        // only set charge limit on view when we receive this value as a confirmation
        currentLimitView.setText(String.format(getString(R.string.charge_power_limit_value), event.getLimit()));
    }

    private void updateGraph(final int soc, final float power, final int temp) {
        if( !isAdded() ) return;
        if (soc > lastSocPlotted) {
            powerSeries2.appendData(new DataPoint(soc, power), false, 100); // no auto scrolling
            tempSeries2.appendData(new DataPoint(soc, temp), false, 100); // no auto scrolling
            lastSocPlotted = soc;
        }
        powerSeries.appendData(new DataPoint(lastTimePlotted, power), false, 100);
        tempSeries.appendData(new DataPoint(lastTimePlotted++, temp), true,100);
    }

    private void updateChargeValues(
            final float chargePower,
            final VehicleStatus vehicleStatus) {

        if( !isAdded() ) return;
        if (vehicleStatus.state == STATE_CHARGE) {
            this.chargePower.setText(getString(R.string.charge_power_value, chargePower));
            packI.setText(getString(R.string.bpacki_value, vehicleStatus.bPackI));
            packV.setText(getString(R.string.bpackv_value, vehicleStatus.bPackV));
            resBatteryEnergy.setText(getString(R.string.resbattenergy_value, vehicleStatus.resBatteryEnergy));
        } else {
            this.chargePower.setText(R.string.no_charge_power);
        }
        battTemp.setText(getString(R.string.celsius_value, vehicleStatus.temp));
        chargeLevel.setText(getString(R.string.soc_value, vehicleStatus.soc));
        consumptionView.setText(getString(R.string.avg_cons_value, vehicleStatus.avgConsumption));
        rangeView.setText(getString(R.string.range_value_short, vehicleStatus.range));
    }

    private void updateValues(final VehicleStatus currentStatus) {
        if( !isAdded() ) return;
        // removed fixed interval of DTC code requests here
        if (currentStatus.state == STATE_CHARGE) {
            // charge data handling
            int chargePower = 0;
            if (currentStatus.subState == 101) {
                chargePower =
                        (int) (((double) currentStatus.cMAINSV) * 1.0d * ((double) currentStatus.cMAINSC));
            } else if (currentStatus.subState == 104) {
                chargePower = (int) (((double) currentStatus.bPackV) * 0.94d * ((double) currentStatus.bPackI));
            }
            // changed to push all new values and now longer check on previous charge value
            float chargePowerInKwh = ((float) chargePower) / 1000.0f;
            // update charge view here
            updateChargeValues(chargePowerInKwh, currentStatus);
            updateGraph(currentStatus.soc, chargePowerInKwh, currentStatus.temp);
        } else {
            // when user is not charging show some defaults on that view
            updateChargeValues(Const.NOT_CHARGING, currentStatus);
        }
    }

}
