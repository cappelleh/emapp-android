package be.hcpl.android.energica.services.obd2;

import static com.google.android.gms.common.util.ArrayUtils.removeAll;

import android.os.Looper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;
import com.github.pires.obd.commands.ObdCommand;
import com.github.pires.obd.exceptions.ResponseException;

import be.hcpl.android.energica.helpers.ExportData;

public class ObdZeroCommand extends ObdCommand {


    /**
     * <p>Constructor for ObdRawCommand.</p>
     *
     * @param command a {@link java.lang.String} object.
     */
    public ObdZeroCommand(String command) {
        super(command);
        responseDelayInMs = (long) 500;
    }

    public String getFormattedResult() {
        return rawData;
    }

    protected void performCalculations() {

    }

    public String getCalculatedResult() {
        return "";
    }

    public String getName() {
        return "Zero OBD2 Custom command";
    }
    protected void fillBuffer() {

    }



    public static int readInputStreamWithTimeout(InputStream is, byte[] b, int timeoutMillis)
            throws IOException  {
        int bufferOffset = 0;
        long maxTimeMillis = System.currentTimeMillis() + timeoutMillis;
        while (System.currentTimeMillis() < maxTimeMillis && bufferOffset < b.length) {
            int readLength = java.lang.Math.min(is.available(),b.length-bufferOffset);
            // can alternatively use bufferedReader, guarded by isReady():
            int readResult = is.read(b, bufferOffset, readLength);
            if (readResult == -1) break;
            bufferOffset += readResult;
        }
        return bufferOffset;
    }

    protected void readRawData(InputStream in) throws IOException {
        byte[] inputData = new byte[8192];
        int readCount = readInputStreamWithTimeout(in, inputData, 3000);
        byte[] dst = new byte[readCount];
        System.arraycopy(inputData, 0, dst, 0,readCount);
        rawData = new String(dst);
    }

    protected void sendCommand(OutputStream out) throws IOException,
            InterruptedException {
        // write to OutputStream (i.e.: a BluetoothSocket) with an added
        // Carriage return
        out.write((cmd + "\r\n").getBytes());
        out.flush();
        if (responseDelayInMs != null && responseDelayInMs > 0) {
            Thread.sleep(responseDelayInMs);
        }
    }

    /**
     * Resends this command.
     *
     * @param out a {@link java.io.OutputStream} object.
     * @throws java.io.IOException            if any.
     * @throws java.lang.InterruptedException if any.
     */

    protected void resendCommand(OutputStream out) throws IOException,
            InterruptedException {
        out.write("\r".getBytes());
        out.flush();
        if (responseDelayInMs != null && responseDelayInMs > 0) {
            Thread.sleep(responseDelayInMs);
        }
    }
    protected void readResult(InputStream in) throws IOException {
        readRawData(in);
        cleanTextContent();
        checkForErrors();
    }
    void checkForErrors() throws IOException {
        // response should include ZERO MBB>
        if (!rawData.contains("ZERO MBB>")) {
            throw new IOException("No MBB detected: " + rawData);
        }
    }
    public boolean isValid() {
        // will be overwritten by subclasses.
        return true;
    }
    public String getResult() {
        return (rawData);
    }
    public void run(InputStream in, OutputStream out) throws IOException,
            InterruptedException {
                synchronized (ObdZeroCommand.class) {//Only one command can write and read a data in one time.
                sendCommand(out);
                readResult(in);
            }
    }


    private  void cleanTextContent()
    {
        // strips off all non-ASCII characters
        rawData = rawData.replaceAll("[^\\x00-\\x7F]", "");
        // erases all the ASCII control characters
        rawData = rawData.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");
    }
}
