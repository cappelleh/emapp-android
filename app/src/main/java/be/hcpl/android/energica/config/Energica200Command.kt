package be.hcpl.android.energica.config

import okhttp3.internal.toHexString

class Energica200Command : EnergicaCommand() {

    override fun getName() = "Energica 200 001"

    override fun getFormattedResult(): String {
        // 0C  // see next
        // 01  // 0C01 is 3084 /10 = 308.4 pack V
        return try{
            // 200 16 47 64 17 0C 01 FF FE
            "Temp=${int(3, 5)}/${int(9, 11)}°C SOC=${int(5, 7)}% SOH=${int(7, 9)}% Volt=${getPackVoltage()} Amps=${getPackCurrent()} ACC=${getAcCurrent()}A DCC=${getDcCurrent()}A"
        } catch(e: Exception) {
            "ERROR ${e.localizedMessage} RAW=$result"
        }
    }

    fun getRawResult() =
        try {
            "DEC ${int(3, 5)} ${int(5, 7)} ${int(7, 9)} ${int(9, 11)} ${int(11, 13)} ${int(13, 15)} ${int(15, 17)} ${int(17, 19)}"
        } catch(e: Exception) {
            "ERROR ${e.localizedMessage} RAW=$result"
        }

    fun getTemp1() = int(3, 5)

    fun getTemp2() = int(9, 11)

    fun getSOC() = int(5, 7)

    fun getSOH() = int(7, 9)

    fun getPackVoltage() = int(11,15)/10

    fun getPackCurrent() = signedDouble(15,19)/10

    fun getPower() = getPackVoltage() * 0.94 * getPackCurrent() / 1000

    fun getAcCurrent(): Int {
        val current = int(17, 19) / 6
        return if (current == DEFAULT_CURRENT_AC || current > MAX_CURRENT_AC) 0 else current
    }

    fun getDcCurrent(): Int {
        val current = int(15, 17) * 25 + int(17, 19) / 10
        return if (current == DEFAULT_CURRENT_DC || current > MAX_CURRENT_DC) 0 else current
    }

    override fun generateDummyData() {
        // example DEC 17 89 100 18 12 159 255 254
        // raw 200164764170C01FFFE
        this.rawData = "200${debugTemp1()}${debugSoc()}64${debugTemp2()}0C88003D" //200 16 55 64 1 8  0  C  8  8  0  0  3  D
    }

    private fun debugSoc(): String {
        if (++debugSoc >= 100) {
            debugSoc = 0
        }
        return debugSoc.toHexString().fixFormat()
    }

    private fun debugTemp1(): String {
        if( debugSoc % 10 == 0 ){
            ++debugTemp1
        }
        return debugTemp1.toHexString().fixFormat()
    }

    private fun debugTemp2(): String {
        if( debugSoc % 10 == 0 ){
            ++debugTemp2
        }
        return debugTemp2.toHexString().fixFormat()
    }

    private fun String.fixFormat() = if (this.length < 2) "0${this.uppercase()}" else this.uppercase()

    companion object {

        private const val DEFAULT_CURRENT_AC = 42
        private const val MAX_CURRENT_AC = 20
        private const val DEFAULT_CURRENT_DC = 6400
        private const val MAX_CURRENT_DC = 100

        private var debugSoc = 0
        private var debugTemp1 = 20
        private var debugTemp2 = 21


    }

}
