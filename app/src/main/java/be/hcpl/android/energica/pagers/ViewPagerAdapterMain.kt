package be.hcpl.android.energica.pagers

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import be.hcpl.android.energica.ui.ble.ChargeFragment
import be.hcpl.android.energica.ui.ble.OdometerFragment

class ViewPagerAdapterMain(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        private val odometerFragment: OdometerFragment =
                OdometerFragment()
        private val chargeFragment: ChargeFragment =
            ChargeFragment()

        override fun getItem(position: Int): Fragment {
                return when (position) {
                        1 -> chargeFragment
                        else -> odometerFragment
                }
        }

        override fun getPageTitle(position: Int): CharSequence {
                return when (position) {
                        1 -> "Charge Settings"
                        else -> "Energica BLE"
                }
        }

        override fun getCount() = 2

}
