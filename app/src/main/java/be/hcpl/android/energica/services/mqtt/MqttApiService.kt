package be.hcpl.android.energica.services.mqtt

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface MqttApiService {

    @Headers("Content-Type: application/json")
    @POST
    fun pushData(
        @Url url: String,
        @Header("Authorization") token: String,
        @Body content: MqttPushData
    ): Call<Unit>

}

class MqttApiServiceImpl {

    // example push data url
    // https://homeassistant.local:8321/api/services/mqtt/publish
    fun getService(): MqttApiService = getRetrofit().create(MqttApiService::class.java)

    private fun getRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.apply { interceptor.level = HttpLoggingInterceptor.Level.BODY }
        val client = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()
        val gson = GsonBuilder().setLenient().create()
        val baseUrl = "http://base/" // ignored
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

}