package be.hcpl.android.energica.services.chargemap

import be.hcpl.android.energica.model.ocm.ChargeLocation
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ChargeMapApiService {

    // see doc https://openchargemap.io/site/develop/api

    // reference info
    // https://api.openchargemap.io/v3/referencedata/?key=53b37716-5a32-4063-a5c6-f58447e19dce

    // example request (Kortrijk, BE) for DC (type2) chargers only within 20 km, to be checked every 10 km. Max 10 results
    // https://api.openchargemap.io/v3/poi?key=53b37716-5a32-4063-a5c6-f58447e19dce
    // &latitude=50.802784&longitude=3.2095745&output=json&maxresults=10&distance=20&distanceUnit=KM&compact=true&connectiontypeid=33

    @GET("/v3/poi")
    fun chargeLocations(
        @Query("key") apiKey: String = "53b37716-5a32-4063-a5c6-f58447e19dce",
        @Query("latitude") lat: Double?,
        @Query("longitude") lon: Double?,
        @Query("output") output: String = "json",
        @Query("compact") compact: Boolean = true,
        @Query("maxresults") maxResults: Int = 10, // get 10 results
        @Query("distance") distance: Int = 20, // in a 20 km radius (fetched every 10 km for overlap)
        @Query("distanceunit") distanceUnit: String = "KM", // "KM" or "Miles" depending on config
        @Query("connectiontypeid") connectionTypeId: String = "33,32" // 33 is EU DC type2 chargers, 32 is US DC
    ): Call<List<ChargeLocation>>

}

class ChargeMapApiServiceImpl {

    fun getService(): ChargeMapApiService = getRetrofit().create(ChargeMapApiService::class.java)

    private fun getRetrofit(): Retrofit {
        val gson = GsonBuilder().setLenient().create()
        return Retrofit.Builder()
            .baseUrl("https://api.openchargemap.io") // this is used
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

}
