package be.hcpl.android.energica.services.ble

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.app.Service
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import be.hcpl.android.energica.BleEnergicaActivity
import be.hcpl.android.energica.R
import be.hcpl.android.energica.helpers.Const
import be.hcpl.android.energica.helpers.Const.BLE_EVENT
import be.hcpl.android.energica.helpers.Const.BLE_GENERIC_EVENT
import be.hcpl.android.energica.helpers.Const.BLE_STATE
import be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_EVENT
import be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_GENERIC_EVENT
import be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_STATE
import be.hcpl.android.energica.helpers.Const.BROADCAST_EVENT
import be.hcpl.android.energica.helpers.Const.BROADCAST_FILTER
import be.hcpl.android.energica.helpers.Const.CONSUMPTION_UNIT_WH_KM
import be.hcpl.android.energica.helpers.Const.DISTANCE_UNIT_KM
import be.hcpl.android.energica.helpers.Const.STATE_CHARGE
import be.hcpl.android.energica.helpers.Const.STATE_IDLE
import be.hcpl.android.energica.helpers.Const.STATE_NOT_CONNECTED
import be.hcpl.android.energica.helpers.Const.STATE_RUN
import be.hcpl.android.energica.helpers.Const.STATE_SEARCHING
import be.hcpl.android.energica.helpers.ExportData
import be.hcpl.android.energica.helpers.ExportData.Companion.appendToFile
import be.hcpl.android.energica.helpers.ExportData.Companion.timeFormatFile
import be.hcpl.android.energica.helpers.ExportData.Companion.timeFormatLogStatement
import be.hcpl.android.energica.helpers.Help
import be.hcpl.android.energica.model.ble.VehicleStatus
import be.hcpl.android.energica.model.data.BleData
import be.hcpl.android.energica.services.evmonitor.EvMonitorRepo
import java.io.Serializable


class BleService : Service() {

    private lateinit var prefs: SharedPreferences
    private var evMonitorRepo: EvMonitorRepo? = null
    private var serviceThread: Thread? = null

    private val handler = Handler()
    private lateinit var connection: ConnectionManager
    private var taskCounter = 0

    private var log2file = false
    private val logFileName = "ble-data-${timeFormatFile.format(System.currentTimeMillis())}.txt"

    companion object {

        const val MANUALLY_STOPPED = 100
        const val MANUALLY_STARTED = 200
        private const val NOTIFICATION_CHANNEL_ID = "ble-channel"
        private const val NOTIFICATION_CHANNEL_NAME = "BLE Connection"
        private const val NOTIFICATION_ID = 456
        private const val NOTIFICATION_TEXT = "BLE Service running"

        private const val TASK_TIME_INTERVAL = 500 // update loop interval, TODO make this a config option
        private const val SCANNING_TIMEOUT: Long = 30000

        // alternative way of killing service thread
        var running = false

    }

    // region receive config updates

    internal class IncomingHandler(private val connection: ConnectionManager) : Handler() {

        override fun handleMessage(msg: Message) {
            when (msg.what) {
                MANUALLY_STOPPED -> connection.isManuallyStopped = true
                MANUALLY_STARTED -> connection.isManuallyStopped = false
                else -> super.handleMessage(msg)
            }
        }
    }

    private lateinit var messageHandler: Handler
    private lateinit var messenger: Messenger

    // endregion

    // region data broadcasting

    private fun sendStateUpdate(bleState: Int) {
        val bundle = Bundle()
        bundle.putInt(BLE_STATE, bleState)
        sendBroadcast(BROADCAST_BLE_STATE, bundle)
    }

    private fun sendBleData(data: BleData){
        val bundle = Bundle()
        bundle.putSerializable(BLE_EVENT, data)
        sendBroadcast(BROADCAST_BLE_EVENT, bundle)
    }

    private fun sendGenericEvent(data: Serializable){
        val bundle = Bundle()
        bundle.putSerializable(BLE_GENERIC_EVENT, data)
        sendBroadcast(BROADCAST_BLE_GENERIC_EVENT, bundle)
    }

    private fun sendBroadcast(event: String, bundle: Bundle?) {
        val intent = Intent(BROADCAST_FILTER)
        intent.putExtra(BROADCAST_EVENT, event)
        if (bundle != null) {
            intent.putExtras(bundle)
        }
        sendBroadcast(intent)
    }

    // endregion

    // region logging

    private fun log(message: String) {
        ExportData.log(applicationContext, message)
    }

    private fun logBleData(bleData: BleData) {
        // optional file logging
        if (log2file) {
            val timestamp = timeFormatLogStatement.format(System.currentTimeMillis())
            appendToFile(logFileName, "$timestamp ${bleData.status}\n", applicationContext)
        }
    }

    // endregion

    // region keep service running in background mode with notification to inform user

    override fun onBind(intent: Intent?): IBinder? = messenger.binder

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        log("BLE - onStartCommand")
        if (!running) {
            // workaround to start/stop service work from other context
            running = true
            startService()
        }
        super.onStartCommand(intent, flags, startId)
        updateSettingsFromIntent(intent)
        return START_STICKY
    }

    private fun updateSettingsFromIntent(intent: Intent?) {
        // TODO consume new settings here from intent instead of SharedPreferences if we want to have separate process on services
    }

    private var builder: Notification.Builder? = null
    private var builderCompat: NotificationCompat.Builder? = null
    private var notificationManager: NotificationManager? = null

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun getNotification(): Notification? {
        val contentIntent = PendingIntent.getActivity(applicationContext, 0, Intent(applicationContext, BleEnergicaActivity::class.java), FLAG_IMMUTABLE)
        val channel = NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)
        if (notificationManager == null) notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager?.createNotificationChannel(channel)
        if (builder == null) builder = Notification.Builder(applicationContext, NOTIFICATION_CHANNEL_ID)
        builder?.apply {
            setSmallIcon(R.drawable.ic_action_bluetooth_connected)
            setContentTitle(getString(R.string.app_name))
            setContentText(NOTIFICATION_TEXT)
            setContentIntent(contentIntent)
        }
        return builder?.build()
    }

    private fun getCompatNotification(): Notification? {
        val contentIntent = PendingIntent.getActivity(applicationContext, 0, Intent(applicationContext, BleEnergicaActivity::class.java), FLAG_IMMUTABLE)
        if (notificationManager == null) notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (builderCompat == null) builderCompat = NotificationCompat.Builder(applicationContext)
        builderCompat?.apply{
            setSmallIcon(R.drawable.ic_action_bluetooth_connected)
            setContentTitle(getString(R.string.app_name))
            setContentText(NOTIFICATION_TEXT)
            setContentIntent(contentIntent)
            setAutoCancel(false)
            setOngoing(true)
        }
        return builderCompat?.build()
    }

    private fun cancelNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            stopForeground(true)
        } else {
            notificationManager?.cancel(NOTIFICATION_ID)
        }
    }

    // endregion

    // region lifecycle

    override fun onCreate() {
        super.onCreate()
        log("BLE - Creating service..")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(NOTIFICATION_ID, getNotification())
        } else {
            notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager?.notify(NOTIFICATION_ID, getCompatNotification())
        }
        // when service run in separate process with xml process config these don't get updated anymore, instead use onStartCommand(intent...)
        prefs = Help.getSharedPrefs(applicationContext)
        evMonitorRepo = EvMonitorRepo.instance(applicationContext)

        // update if logging is enabled here
        log2file = prefs.getBoolean(getString(R.string.key_log_to_file), false)

        connection = ConnectionManager.getInstance(applicationContext)
        // our communication channel with the view
        messageHandler = IncomingHandler(connection)
        messenger = Messenger(messageHandler)
        connection.setCallback { event -> sendGenericEvent(event) }
        // update auto connect feature from preferences
        connection.shouldReconnect = prefs.getBoolean(getString(R.string.key_ble_autoconnect), Const.DEFAULT_BLE_AUTO_CONNECT)

        startService()
        log("BLE - Service created.")
    }

    override fun onDestroy() {
        super.onDestroy()
        log("BLE - Destroying service...")
        evMonitorRepo?.flushData()
        stopService()
        log("BLE - Service destroyed.")
    }

    @SuppressLint("MissingPermission")
    private fun startService() {
        log("BLE - Starting service..")
        running = true
        // in debug mode just continue with generated test data
        val inDebugMode = prefs.getBoolean(applicationContext.getString(R.string.key_general_debug_data), false)
        if( inDebugMode ){
            generateDebugData()
            return
        }
        // connect bluetooth if not already connected
        if (!connection.isConnected) {
            // enabled reconnect while app is live and conn drops
            connection.shouldReconnect = prefs.getBoolean(getString(R.string.key_ble_autoconnect), Const.DEFAULT_BLE_AUTO_CONNECT)
            // not connected so create a connection logic executed here
            connect()
        } else {
            // already connected so disconnect logic goes here
            connection.shouldReconnect = false
            stopScanning()
            disconnect()
        }
        // main worker thread
        serviceThread = Thread {
            while (running) {
                handler.post{
                    val connected: Boolean = connection.isConnected
                    val currentStatus: VehicleStatus = connection.currentStatus
                    val selectedUnit = currentStatus.distanceUnit

                    // if connected or in demo mode
                    if (connected) {
                        if (taskCounter % 2 == 0) {
                            // every even run we request new values
                            connection.vehicleInfoRequest()
                        } else {
                            // on uneven runs update odometer (probably should run these together)
                            connection.odometerInfoRequest() // moved from %10 interval to this same %20
                        }
                        // check if we need to use IDLE state or another value
                        if( currentStatus.state == STATE_SEARCHING )
                            sendStateUpdate(STATE_IDLE) // FIXED prevent overruling the ones received from BLE (RUN and CHARGE)
                        else
                            sendStateUpdate(currentStatus.state)
                        // every uneven run we handle display of values
                        handleBleData(BleData(selectedUnit, currentStatus))
                    } else {
                        /*if(!connection.shouldReconnect) */ sendStateUpdate(STATE_NOT_CONNECTED) // runnning but not connected, separate state needed?
                        // not yet connected and not yet in demo mode, some view initialisation? No data yet
                        if (taskCounter != 0 && taskCounter % 100 == 0) {
                            if (connection.shouldReconnect) {
                                log("BLE - triggered auto reconnect")
                                connection.setMAC(null)// we need to clear this first or it will prevent reconnect
                                connect()
                            }
                        }
                    }
                    taskCounter = if (taskCounter >= 199) 0 else taskCounter + 1 // just counts to prevent sending too much data at once
                }
                try {
                    Thread.sleep(TASK_TIME_INTERVAL.toLong()) // a delay between executions within loop
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }
        serviceThread?.start()
    }

    private fun generateDebugData() {
        serviceThread = Thread {
            var counter = 0.1
            while (running) {
                handler.post {
                    // dummy BLE data generated
                    val status = VehicleStatus()
                    // for ChargeSettingsFragment
                    status.bPackI = (75 * counter).toFloat()
                    status.bPackV = (333.3f * counter).toFloat()
                    status.resBatteryEnergy = (16400 * counter).toInt()
                    status.soc = (100 * counter).toInt()
                    status.temp = (40 * counter).toInt()
                    status.range = (320 * counter).toInt()
                    status.avgConsumption = 60f
                    status.avgConsumptionUnit = CONSUMPTION_UNIT_WH_KM
                    status.state = if (counter < 0.3) STATE_CHARGE else if (counter > 0.3 && counter < 0.6) STATE_RUN else STATE_IDLE
                    status.chgPowerLimit = 75
                    status.cDCC = 75 - (75 * counter).toFloat()
                    status.cDCV = (300 * counter).toFloat()
                    status.cMAINSC = 15 - (15 * counter).toFloat()
                    status.cMAINSV = (220 * counter).toFloat()
                    status.subState = 104
                    // for OdometerFragment
                    status.speed = (140 * counter).toInt()
                    status.rpm = (6000 * counter).toInt()
                    status.power = (80 * counter).toInt()
                    status.torque = (200 * counter).toInt()
                    status.tripMeter = (300 * counter).toFloat()
                    status.totalOdometer = 21350f
                    // combine
                    val bleData = BleData(
                        selectedUnit = DISTANCE_UNIT_KM,
                        status = status
                    )
                    handleBleData(bleData)
                    // push mocked data to remotes
                    evMonitorRepo?.pushEnergicaBleData(status)
                    // have some counter running for somewhat realistic values
                    counter = if( counter > 1.0 ) {
                        0.0
                    } else {
                        if (counter % 0.1 == 0.0) evMonitorRepo?.flushData() // push mocked data in between
                        counter + 0.01
                    }
                }
                try {
                    Thread.sleep((TASK_TIME_INTERVAL*2).toLong()) // a delay between executions within loop
                } catch (e: InterruptedException) {
                    // ignore, this will trigger on killing service anyway
                    // e.printStackTrace()
                }
            }
        }
        serviceThread?.start()
    }

    private fun handleBleData(bleData: BleData) {
        // optional file logging
        logBleData(bleData)
        // push data using broadcasts
        sendBleData(bleData)
    }

    private fun stopService() {
        log("BLE - Stopping service..")
        if( connection.isConnected ) {
            stopScanning()
            disconnect()
        }
        sendStateUpdate(STATE_NOT_CONNECTED)
        cancelNotification()
        running = false
        serviceThread?.interrupt()
        stopSelf() // kill service
    }

    // endregion

    private fun startScanning() {
        if (connection.isScanning) {
            stopScanning()
        }
        addScanningTimeout()
        connection.startScanning()
    }

    private fun stopScanning() {
        if (connection.isScanning) {
            // remove running stopScanning task from addScanningTimeout()
            handler.removeCallbacks(scanningTimedOut)
            // stop scanning logic
            connection.stopScanning()
        }
    }

    private fun connect() {
        if (!connection.isBluetoothEnabled) {
            log("BLE - bluetooth disabled or no permissions (check settings)")
            return
        }
        sendStateUpdate(STATE_SEARCHING)
        // check if we have a device to connect to
        val linkedDevice = prefs.getString(Const.SETTINGS_DEVICE, null)
        if (linkedDevice == null || linkedDevice.isEmpty() || connection.shouldScan()) {
            log("BLE - no confirmed linked device found, start scanning")
            startScanning()
        } else {
            log("BLE - connect directly to confirmed linked device $linkedDevice")
            stopScanning()
            connection.handleFoundDevice(linkedDevice)
        }
    }

    private fun disconnect() {
        if (connection.isConnected) {
            connection.disconnect()
            sendStateUpdate(STATE_NOT_CONNECTED)
        }
    }

    private val scanningTimedOut = Runnable {
        log("BLE - bluetooth scanning timed out")
        stopScanning()
    }

    private fun addScanningTimeout() {
        handler.postDelayed(scanningTimedOut, SCANNING_TIMEOUT)
    }

}