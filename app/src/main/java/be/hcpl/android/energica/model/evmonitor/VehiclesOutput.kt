package be.hcpl.android.energica.model.evmonitor

import androidx.annotation.Keep

@Keep
data class VehiclesOutput(
    val SUCCESS: String?,
    val vehicles: List<Vehicle>?,
    val error: String?
    )
