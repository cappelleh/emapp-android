package be.hcpl.android.energica.services.chargemap

import android.content.Context
import be.hcpl.android.energica.helpers.ExportData
import be.hcpl.android.energica.model.ocm.ChargeLocation
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChargeMapRepo(private val context: Context) {

    // remote connection
    private var chargeMapApiService = ChargeMapApiServiceImpl().getService();

    // limit requests to one at a time
    private var fetchingChargers: Boolean = false

    companion object {
        private const val TAG = "ChargeMapRepo"

        // TODO move settings for enabled and such to here instead (static)
    }

    class ChargeConfig(
        val searchRadiusInKm: Int = 20, // list chargers within this reach
        val numberOfResults: Int = 10, // how many chargers to show at max
        val typeOfChargers: String = "33,32", // EU and US DC chargers
        val distanceUnit: String = "KM" // "KM" or "Miles"
    )

    private fun log(message: String) {
        ExportData.log(context, message)
    }

    fun chargeLocations(
        latitude: Double, // location
        longitude: Double, // location
        getChargeConfig: () -> ChargeConfig,
        onSuccess: (List<ChargeLocation>) -> Unit, // on retrieval of chargers
        onFailure: () -> Unit, // on failure
        onBusy: () -> Unit // request ignored because already fetching
    ) {

        if (fetchingChargers) {
            onBusy()
            return
        }
        fetchingChargers = true
        val chargeConfig = getChargeConfig()
        chargeMapApiService.chargeLocations(
            lat = latitude,
            lon = longitude,
            distance = chargeConfig.searchRadiusInKm,
            distanceUnit = chargeConfig.distanceUnit,
            maxResults = chargeConfig.numberOfResults,
            connectionTypeId = chargeConfig.typeOfChargers
        ).enqueue(object : Callback<List<ChargeLocation>> {

                override fun onResponse(call: Call<List<ChargeLocation>>, response: Response<List<ChargeLocation>>) {

                    fetchingChargers = false
                    if (!response.isSuccessful) {
                        log("failed response for fetching chargers with message ${response.message()}")
                        onFailure()
                    } else if (response.body() != null) {
                        onSuccess(response.body() ?: emptyList())
                    }
                }

                override fun onFailure(call: Call<List<ChargeLocation>>, t: Throwable) {
                    fetchingChargers = false
                    log("failed response for fetching chargers with message ${t.message}")
                    onFailure()
                }
            })

    }

}
