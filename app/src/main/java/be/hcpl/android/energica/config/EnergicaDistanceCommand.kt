package be.hcpl.android.energica.config

import com.github.pires.obd.commands.protocol.ObdRawCommand
import kotlin.math.pow

class EnergicaDistanceCommand : ObdRawCommand("0131") {

    // ObdRawCommand("0131"), // "distance since codes cleared"), // formula is (256*A + B) max is 65.535 km

    override fun getName() = "Distance since codes cleared"

    override fun getFormattedResult(): String {
        return try {
            // ex. 7EF037F01337EF044131390B w/ >390B< to int = 14.603 km
            "${calculateValue()}"
        } catch(e: Exception) {
            "ERROR ${e.localizedMessage} RAW=$result"
        }
    }

    fun calculatedValue() : Int = try { calculateValue() } catch(e: Exception) { 0 }

    // 7EF037F01337EF044131390B >390B< to int = 14.603 km
    // received as "7EF037F01337EF044131390B"
    private fun calculateValue() = int(20,24)

}