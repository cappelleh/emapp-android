package be.hcpl.android.energica.services.obd2;

import java.io.ByteArrayOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

import be.hcpl.android.energica.helpers.DataLogger;

public class LoggableOBDInputStream extends FilterInputStream {

    private final DataLogger logger;
    private final ByteArrayOutputStream bos;

    public LoggableOBDInputStream(InputStream out, DataLogger logger) {
        super(out);
        this.logger = logger;
        if (logger != null) {
            this.bos = new ByteArrayOutputStream(16);
        } else {
            this.bos = null;
        }
    }

    static public boolean isEnd(byte b) { return (char)b == '>'; }

    private int hasEnd(byte[] a, int off, int len) {
        for (int i = off; i < len; i++) {
            if (isEnd(a[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int read(byte[] a, int off, int len) throws IOException {
        int n = super.read(a, off, len);
        if (bos != null && n > 0) {
            int i = hasEnd(a, off, n);
            if (i >= 0) { // End character found in
                bos.write(a, off, i - off + 1); // Keep the end character
                logger.log("<", bos.toByteArray());
                bos.reset();
                bos.write(a, i, n - (i - off)); // Write the remainder
            }
        }
        return n;
    }

    @Override
    public int read(byte[] a) throws IOException {
        return read(a, 0, a.length);
    }

    @Override
    public int read() throws IOException {
        int b = super.read();
        if (b != -1 && bos != null) {
            bos.write(b);
            if (isEnd((byte)b)) {
                logger.log("<", bos.toByteArray());
                bos.reset();
            }
        }
        return b;
    }

}
