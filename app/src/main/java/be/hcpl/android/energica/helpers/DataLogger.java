package be.hcpl.android.energica.helpers;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.charset.StandardCharsets;
import java.security.InvalidParameterException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class DataLogger {

//    static public DateFormat dfLog = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    /** The executor writing to the log file, so that calls to log() won't block. */
    static private ExecutorService exeWrite = Executors.newSingleThreadExecutor();

    protected boolean active = true; // TODO: log level
    protected final File logFile; // TODO: java.nio.file.Path requires API level 26 (current min is 21) but would be better for throughput
    protected final ByteBuffer logBuf;

    public DataLogger(File logFile, int szBuf) throws NullPointerException {
        if (logFile == null) {
            throw new NullPointerException("'logFile' is null");
        }
        this.logFile = logFile;//logFile.toPath();
        this.logBuf = ByteBuffer.wrap(new byte[szBuf]); // ensures logBuf.array() returns a buffer. TODO: allocateDirect() would be more efficient with use of AsynchronousFileChannel but requires API level 26
    }

    public DataLogger(File logFile) throws NullPointerException, InvalidParameterException {
        this(logFile, 1024*1024); // 1MB buffer size
    }

    public DataLogger activateLogging(boolean active) {
        this.active = active;
        return this;
    }

    @NonNull
    static protected StringBuilder prepareLogging(String source, int extraLen) {
        StringBuilder sb = new StringBuilder(24 + (source != null ? source.length() : 0) + extraLen); // 24 for date encoding
//      sb.append(dfLog.format(new Date()));
        sb.append(System.currentTimeMillis()); // Epoch instead of formatted date
        if (source != null && !source.isEmpty()) {
            sb.append(' ');
            sb.append(source);
        }
        sb.append(":");
        return sb;
    }

    protected void postLogging(StringBuilder sb) {
        sb.append('\n');

        byte[] enc = sb.toString().getBytes(StandardCharsets.UTF_8);
        if (logBuf != null) {
            synchronized (this) {
                if (logBuf.remaining() < enc.length) {
                    flush();
                }
                logBuf.put(enc);
            }
        }
    }

    public void log(String source, String message) {
        if (!active) {
            return;
        }

        StringBuilder sb = prepareLogging(source, message != null ? message.length() : 0);
        if (message == null) {
            sb.append(" (null)");
        } else {
            sb.append(message);
        }
        postLogging(sb);
    }

    public void log(String message) {
        log(null, message);
    }

    public void log(String source, byte[] buffer) {
        if (!active) {
            return;
        }

        StringBuilder sb = prepareLogging(source, buffer != null ? 3 * buffer.length : 0);
        if (buffer == null) {
            sb.append(" (null)");
        } else {
            for (byte b : buffer) {
                sb.append(String.format(" %02x", b & 0xff));// unsigned byte
            }
        }

        postLogging(sb);
    }

    public void log(byte[] buffer) {
        log(null, buffer);
    }

    public void flush() {
        if (logBuf == null || logBuf.position() == 0) { // Nothing to write
            return;
        }

        byte[] bw;
        synchronized (this) {
            bw = Arrays.copyOf(logBuf.array(), logBuf.position());
            logBuf.clear();
        }

        // TODO: AsynchronousFileChannel could get rid of Executor but needs API26
//        try (AsynchronousFileChannel fc = AsynchronousFileChannel.open(logFile, StandardOpenOption.APPEND)) {
//            logBuf.flip();
//            fc.write(logBuf);
//        }
        exeWrite.submit(() -> {
            try (FileOutputStream fos = new FileOutputStream(logFile, true)) {
                fos.write(bw);
            } catch (IOException e) {
                // TODO:
            }
        });
    }

}
