package be.hcpl.android.energica.services.ble;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Handler;
import android.util.Log;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import be.hcpl.android.energica.model.data.ChargeLimit;
import be.hcpl.android.energica.model.data.ConnectionRejected;
import be.hcpl.android.energica.model.data.LocationData;
import be.hcpl.android.energica.model.data.NewState;
import be.hcpl.android.energica.helpers.BleWrapper;
import be.hcpl.android.energica.helpers.CommParser;
import be.hcpl.android.energica.helpers.Const;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.helpers.Help;
import be.hcpl.android.energica.interfaces.BleWrapperUiCallback;
import be.hcpl.android.energica.interfaces.CommParserCallback;
import be.hcpl.android.energica.model.ble.BleDefinedUUIDs;
import be.hcpl.android.energica.model.ble.GpsData;
import be.hcpl.android.energica.model.ble.SecurityAccess;
import be.hcpl.android.energica.model.ble.VehicleStatus;
import be.hcpl.android.energica.services.evmonitor.EvMonitorRepo;

import static be.hcpl.android.energica.helpers.Const.MPS_TO_KPH;
import static be.hcpl.android.energica.helpers.Const.SETTINGS_DEVICE;
import static be.hcpl.android.energica.helpers.Const.STATE_NOT_CONNECTED;
import static be.hcpl.android.energica.helpers.Const.STATE_SEARCHING;
import static be.hcpl.android.energica.services.evmonitor.EvMonitorRepo.*;

import androidx.annotation.NonNull;

public class ConnectionManager implements BleWrapperUiCallback, CommParserCallback {

    private final WeakReference<Context> context; // prevent memory leaks with weakReference
    private BleWrapper bleWrapper;
    private final CommParser commParser;
    private String MAC;
    private final Runnable authRunnable = () -> {
        // informs bike about this device, this is some session that is created and maintained
        if (bleWrapper == null || bleWrapper.getDevice() == null) {
            return;
        }
        String address = bleWrapper.getDevice().getAddress();
        byte[] data = new byte[6];
        for (int i = 0; i < 17; i += 3) {
            data[i / 3] = (byte) ((Character.digit(address.charAt(i), 16) << 4) + Character.digit(address.charAt(i + 1), 16));
        }
        bleWrapper.writeDataToCharacteristic(
                ConnectionManager.this.writeBleGatt,
                new byte[]{4, 17, 0, -2, data[0], data[1], data[2], data[3], data[4], data[5]});
    };
    private VehicleStatus currentStatus = new VehicleStatus();
    private boolean deviceConnected = false;
    private boolean isScanning = false; // marks BLE scanning in progress
    private boolean scanned = false; // scan at least once?
    private boolean isConnecting = false; // in some conditions with auto reconnect we were stuck
    private final Handler handler = new Handler();
    private boolean shouldReconnect = false; // only enable auto connects if user connected manually once
    private BluetoothGattCharacteristic notifyBleGatt = null;
    private BluetoothGattCharacteristic writeBleGatt = null;

    private final SharedPreferences prefs;
    private final EvMonitorRepo evMonitorRepo;

    // region need to track manually stopped services so we can avoid auto reconnects in that scenario
    // because UI (stop button pressed) and service are running in different threads this is a workaround
    private boolean manuallyStopped = false; // marks intentionally stopped services

    public void setManuallyStopped(boolean manually) {
        manuallyStopped = manually;
        Log.d("ConnectionManager", String.format("update manuallyStopped set %b on instance %s", manuallyStopped, instance != null ? instance.toString() : "null"));
    }

    public boolean isManuallyStopped() {
        Log.d("ConnectionManager", String.format("update manuallyStopped retrieved %b on instance %s", manuallyStopped, instance != null ? instance.toString() : "null"));
        return manuallyStopped;
    }

    // endregion

    // region singleton

    private static ConnectionManager instance;
    private EventCallback eventCallback; // temp solution to send info back to service

    private ConnectionManager(final Context context) {
        this.context = new WeakReference<>(context);
        prefs = Help.getSharedPrefs(context);
        evMonitorRepo = Companion.instance(context);
        bleWrapper = new BleWrapper(context, this);
        commParser = new CommParser(bleWrapper, null, this);
        bleWrapper.initialize();
    }

    private static ConnectionManager init(final Context context) {
        if (instance == null) {
            // note because of threads we still will have an instance for frontend and another for backend
            instance = new ConnectionManager(context);
        }
        return instance;
    }

    public void setCallback(final EventCallback eventCallback){
        this.eventCallback = eventCallback;
    }

    public interface EventCallback {

        void post(Serializable event);

    }

    private void post(Serializable event){
        if( eventCallback != null ) eventCallback.post(event);
    }

    public static ConnectionManager getInstance(final Context context) {
        return init(context);
    }

    // endregion

    public VehicleStatus getCurrentStatus() {
        return currentStatus;
    }

    public boolean isBluetoothEnabled() {
        return bleWrapper.isBtEnabled();
    }

    public boolean isConnected() {
        return bleWrapper.isConnected() && deviceConnected && notifyBleGatt != null && writeBleGatt != null;
    }

    public boolean isScanning() {
        return isScanning;
    }

    public void onSeedReceived(Integer seed) {
        SecurityAccess seedAndKey = new SecurityAccess(seed);
        byte[] sendBuffer = {4, 17, 0, 0, seedAndKey.getKeyByte(0), seedAndKey.getKeyByte(1), seedAndKey.getKeyByte(2), seedAndKey.getKeyByte(
                3), 0, 0};
        if (writeBleGatt != null && bleWrapper.getAdapter() != null) {
            bleWrapper.writeDataToCharacteristic(writeBleGatt, sendBuffer);
            handler.postDelayed(authRunnable, 15); // keep confirming session
        }
    }

    public void onConnectionConfirmed() {
        prefs.edit().putString(SETTINGS_DEVICE, MAC).apply(); // only once confirmed allow connect without scanning
        deviceConnected = true;
    }

    public void onVehicleStatusReceived(VehicleStatus status) {
        currentStatus = status;
        if( evMonitorRepo != null ){
            evMonitorRepo.pushEnergicaBleData(status);
        }
    }

    public void onChgPwrLimitReceived(Integer limit) {
        post(new ChargeLimit(limit));
    }

    public void onParseError(byte[] record) {
        // ignored since too noisy EventBus.getDefault().post(new LogEvent("data parsing error"));
    }

    public void onConnectionRejected() {
        post(new ConnectionRejected());
    }

    public void onSpeedRPMTorqueReceived(int speed, int rpm, int torque, float power) {
        // ignored
    }

    public void onGPSDataReceived(@NonNull GpsData gpsData) {
        double latitude = gpsData.convertLat();
        double longitude = gpsData.convertLon();
        Location location = new Location("bike"); // use provider to select gps or bike speeds only
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        location.setSpeed(gpsData.speed/MPS_TO_KPH); // this speed is received in kph (or mph) so convert it to m/s here
        location.setTime(System.currentTimeMillis());
        post(new LocationData(location));
    }

    public void onNearbyChargePointsRequested() {
        // ignore
    }

    public void onChargePointsRequested() {
        // ignore
    }

    private void startLogging() {
        if (!prefs.getBoolean(context.get().getString(be.hcpl.android.energica.R.string.key_ble_raw_debug), Const.DEFAULT_BLE_RAW_DEBUG)) {
            return;
        }
        bleWrapper.startLogging();
    }

    private void stopLogging() {
        bleWrapper.stopLogging();
    }

    public void uiDeviceConnected(BluetoothGatt gatt, BluetoothDevice device) {
        if (isScanning) {
            isScanning = false;
            bleWrapper.stopScanning();
        }
        startLogging();
        deviceConnected = true;
        isConnecting = false;
        MAC = device.getAddress(); // recovers connected device
        triggerUpdateStateEvent();
        bleWrapper.startServicesDiscovery();
    }

    public void triggerUpdateStateEvent() {
        if (isScanning()) {
            post(new NewState(STATE_SEARCHING));
        } else if (isConnected()) {
            // no fixed state indication here cause we'll receive one from the bike
            post(new NewState(currentStatus.state));
        } else {
            post(new NewState(STATE_NOT_CONNECTED));
        }
    }

    public void uiDeviceDisconnected(BluetoothGatt gatt, BluetoothDevice device) {
        stopLogging();
        isConnecting = false; // added this to improve connection stability
        if (shouldReconnect && !manuallyStopped) { // FIXED was also triggered when service is manually stopped!!
            Log.d("ConnectionManager", String.format("device disconnected with shouldReconnect %b, manuallyStopped %b from instance %s", shouldReconnect, manuallyStopped, instance != null ? instance.toString() : "null"));
            // auto reconnect while services are running, just to prevent unexpected disconnects
            // but allow this only once
            handleFoundDevice(device.getAddress());
            return;
        }
        MAC = null; // remove this so we can reconnect to it
        isScanning = false;
        deviceConnected = false;
        notifyBleGatt = null;
        writeBleGatt = null;
        bleWrapper.close();
        triggerUpdateStateEvent();
    }

    public void uiAvailableServices(
            BluetoothGatt gatt,
            BluetoothDevice device,
            List<BluetoothGattService> services) {

        if (!device.getAddress().contains(MAC)) {
            log("Error, wrong device returned from ServiceDiscovery");
            return;
        }
        boolean serviceFound = false;
        Iterator<BluetoothGattService> it = services.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            BluetoothGattService service = it.next();
            if (UUID.fromString(Const.ENERGICA_SERVICE_UUID).equals(service.getUuid())) {
                log("EnergicaServiceUUID found");
                bleWrapper.getCharacteristicsForService(service);
                serviceFound = true;
                MAC = device.getAddress(); // not really needed but can work as a confirmation
                break;
            }
        }
        if (!serviceFound) {
            log("Error, Device doesn't support Energica Services");
        }
    }

    public void uiCharacteristicForService(
            BluetoothGatt gatt,
            BluetoothDevice device,
            BluetoothGattService service,
            List<BluetoothGattCharacteristic> chars) {

        if (!device.getAddress().contains(MAC)) {
            log("Error, wrong device returned from ServiceDiscovery");
            return;
        }
        for (BluetoothGattCharacteristic characteristic : chars) {
            if (characteristic.getUuid().compareTo(BleDefinedUUIDs.Characteristic.ENERGICA_WRITE) == 0) {
                log("ENERGICA_WRITE found");
                writeBleGatt = characteristic;
                commParser.setWriteCharacteristic(writeBleGatt);
            }
            if (characteristic.getUuid().compareTo(BleDefinedUUIDs.Characteristic.ENERGICA_NOTIFY) == 0) {
                log("ENERGICA_NOTIFY found");
                notifyBleGatt = characteristic;
                bleWrapper.setNotificationForCharacteristic(notifyBleGatt, true);
            }
        }
        if (notifyBleGatt != null && writeBleGatt != null) {
            deviceConnected = true;
        }
    }

    private void log(final String message) {
        ExportData.Companion.log(context.get(), message);
    }

    public void uiNewValueForCharacteristic(BluetoothGattCharacteristic characteristic, byte[] rawValue) {
        if (characteristic != null && notifyBleGatt != null && notifyBleGatt.getUuid() != null && notifyBleGatt.getUuid().equals(characteristic.getUuid())) {
            commParser.parseBuffer(rawValue);
        }
    }

    public void uiNewRssiAvailable(
            BluetoothGatt gatt,
            BluetoothDevice device,
            int rssi) {

        // ignore not posting new RSSI values since too noisy
    }

    public void uiDeviceFound(
            BluetoothDevice device,
            int rssi,
            byte[] record) {

        handleFoundDevice(device.getAddress());
    }

    public void uiSuccessfulWrite(
            BluetoothGatt gatt,
            BluetoothDevice device,
            BluetoothGattService service,
            BluetoothGattCharacteristic ch) {

        // ignored
    }

    public void uiFailedWrite(
            BluetoothGatt gatt,
            BluetoothDevice device,
            BluetoothGattService service,
            BluetoothGattCharacteristic ch) {

        // ignored
    }

    public void handleFoundDevice(final String deviceAddress) {
        String pairedDevice = prefs.getString(SETTINGS_DEVICE, null);
        if( MAC != null && MAC.equals(deviceAddress) && isConnecting && !isConnected()){
            log(String.format("skipping found device, MAC: %s, deviceAddress: %s, isConnecting: %b, isConnected: %b", MAC, deviceAddress, isConnecting, isConnected()));
            Log.d("ConnectionManager", String.format("skipping found device since already connecting, MAC: %s, deviceAddress: %s, isConnecting: %b, isConnected: %b", MAC, deviceAddress, isConnecting, isConnected()));
            // already connecting or connected to that MAC so skip here
            return; // FIXED check isConnecting sometimes blocking direct connection w/o scanning
            // also it does work right away if you disconnect and reconnect manually again, so definitely something state related
        }
        if (pairedDevice == null || pairedDevice.equals(deviceAddress)) {
            log(String.format("reconnect attempt with found device, MAC: %s, deviceAddress: %s, isConnecting: %b, isConnected: %b", MAC, deviceAddress, isConnecting, isConnected()));
            Log.d("ConnectionManager", String.format("reconnect attempt with found device, MAC: %s, deviceAddress: %s, isConnecting: %b, isConnected: %b", MAC, deviceAddress, isConnecting, isConnected()));
            isConnecting = true;
            MAC = deviceAddress; // mark the MAC address already connecting to prevent multiple connects to same
            bleWrapper.connect(deviceAddress);
        }
    }

    public void setMAC(final String macValue) {
        MAC = macValue;
    }

    public void setShouldReconnect(final boolean shouldReconnect) {
        this.shouldReconnect = shouldReconnect;
    }

    public boolean getShouldReconnect() {
        return this.shouldReconnect;
    }

    public void stopScanning() {
        bleWrapper.stopScanning();
        isScanning = false;
    }

    public void disconnect() {
        bleWrapper.disconnect();
        bleWrapper.close();
    }

    public void startScanning() {
        scanned = true;
        isScanning = true;
        bleWrapper.startScanning();
    }

    public boolean shouldScan() { return !scanned; }

    public void vehicleInfoRequest() {
        commParser.vehicleInfoRequest();
    }

    public void odometerInfoRequest() {
        commParser.odometerInfoRequest();
    }

    public void setChargeTermination() {
        commParser.setChgTermination();
    }

    public void setResetTrip() {
        commParser.setResetTrip();
    }

}
