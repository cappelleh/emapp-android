package be.hcpl.android.energica.config

class Energica203Command : EnergicaCommand() {

    override fun getName() = "Energica 203 001"

    override fun getFormattedResult(): String {
        return try{
            // 203 0F 40 43 2E 0F 36 0F 4D
            "Cell Balance (min/max)=${getMinCellVoltage()}/${getMaxCellVoltage()} mV"
        } catch(e: Exception) {
            "ERROR ${e.localizedMessage} RAW=$result"
        }
    }

    fun getRawResult(): String = result

    fun getMinCellVoltage() = int(11, 15)

    fun getMaxCellVoltage() = int(15, 19)

    override fun generateDummyData() {
        this.rawData = "2030F40432E0F360F4D"
    }

}
