package be.hcpl.android.energica.services.obd2;

import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

import be.hcpl.android.energica.helpers.DataLogger;

public class LoggableOBDOutputStream extends FilterOutputStream {

    private final DataLogger logger;
    private final ByteArrayOutputStream bos;

    public LoggableOBDOutputStream(OutputStream out, DataLogger logger) {
        super(out);
        this.logger = logger;
        if (logger != null) {
            this.bos = new ByteArrayOutputStream(16);
        } else {
            this.bos = null;
        }
    }

    // Some implementations of BluetoothSocket.outputStream might not loop over write(int) so we force it here for consistency

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        if (bos != null) {
            int n = off+len;
            for (int i = off; i < n; i++) {
                write(b[i]);
            }
        } else { // No logging => call super
            super.write(b, off, len);
        }
    }

    @Override
    public void write(byte[] b) throws IOException {
        write(b, 0, b.length);
    }

    @Override
    public void write(int b) throws IOException { // Other write() methods ultimately call this one
        if (bos != null) {
            bos.write(b);
        }
        super.write(b);
    }

    @Override
    public void flush() throws IOException {
        if (bos != null) {
            byte[] a;
            synchronized (bos) {
                a = bos.toByteArray();
                bos.reset();
            }
            logger.log(">", a);
        }
        super.flush();
    }

}
