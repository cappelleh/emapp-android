package be.hcpl.android.energica.services.generic

import android.content.Context
import android.content.SharedPreferences
import be.hcpl.android.energica.R
import be.hcpl.android.energica.helpers.Const
import be.hcpl.android.energica.helpers.ExportData.Companion.log
import be.hcpl.android.energica.helpers.Help
import be.hcpl.android.energica.model.evmonitor.PushDataInput
import be.hcpl.android.energica.model.evmonitor.PushDataOutput
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenericCloudRepo(private val context: Context) {

    // DONE cache data on local storage if push fails? since we piggy back on EvMonitorRepo for that
    private var service = GenericCloudApiServiceImpl().getService()
    private val prefs: SharedPreferences = Help.getSharedPrefs(context)
    private var enabled = DEFAULT_ENABLED
    private var pushDataUrl = Const.DEFAULT_ENDPOINT
    private var token = Const.DEFAULT_TOKEN

    companion object {
        private const val DEFAULT_ENABLED = false
    }

    init {
        // get initial values from settings here once
        updateConfig()
    }

    private fun updateConfig() {
        enabled = prefs.getBoolean(context.getString(R.string.key_enable_generic_cloud), DEFAULT_ENABLED)
        pushDataUrl = prefs.getString(context.getString(R.string.key_generic_cloud_url), null) ?: Const.DEFAULT_ENDPOINT
        token = prefs.getString(context.getString(R.string.key_generic_cloud_token), null) ?: Const.DEFAULT_TOKEN
    }

    /**
     * force push all data to remote, use this on finishing services for example
     * All push___ functions redirect to this single flush
     */
    fun flushData(dataSet: List<PushDataInput>) {
        // always work with latest config
        updateConfig()

        // don't push if not enabled
        if (!enabled) return
        // don't push anything if we have nothing at this point
        if (dataSet.isEmpty()) {
            log(context, "generic cloud push data failed, no data recorded")
            return
        }
        dataSet.forEach { input -> input.token = token }
        // push all data we've collected at this point
        service.pushData(pushDataUrl, dataSet).enqueue(object : Callback<PushDataOutput> {

            override fun onResponse(
                call: Call<PushDataOutput>,
                response: Response<PushDataOutput>
            ) {
                if (!response.isSuccessful) {
                    log(context, "generic cloud push data response was not in 200 range")
                    return
                }
                response.body()?.let {
                    if (it.error.isNullOrEmpty())
                        log(context, "generic cloud push data done")
                }
            }

            override fun onFailure(call: Call<PushDataOutput>, t: Throwable) {
                log(context, "generic cloud push data failed failed to parse response")
            }
        })
    }

}