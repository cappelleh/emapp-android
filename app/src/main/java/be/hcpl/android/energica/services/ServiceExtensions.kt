package be.hcpl.android.energica.services

import android.app.ActivityManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Process
import androidx.core.content.ContextCompat

fun Class<out Service>.startService(context: Context, serviceConnection: ServiceConnection) {
    // start background services logic
    val intent = Intent(context, this)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        // to improve background handling on Android 8 and higher run foreground service instead
        context.startForegroundService(intent)
        context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    } else {
        // as a backup also support original services
        //context.startService(intent) // TODO test better service retention changes
        ContextCompat.startForegroundService(context, intent)
    }
}

fun Class<out Service>.stopService(context: Context, serviceConnection: ServiceConnection) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        // stops bound foreground services (notification)
        try {
            context.unbindService(serviceConnection)
        } catch (e: Exception) {
            //e.printStackTrace() /* ignore */
            // this will kill complete app if service not running in separate process!
            // this.kill(context) // if normal stopService failed try killing
        }
    }
    // stop background service that are not bound
    val intent = Intent(context, this)
    context.stopService(intent)
}

fun Class<out Service>.isServiceRunning(context: Context): Boolean {
    val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
    for (service in manager!!.getRunningServices(Int.MAX_VALUE)) {
        if (this.name == service.service.className) {
            return true
        }
    }
    return false
}

fun Class<out Service>.kill(context: Context){
    val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
    for (service in manager!!.getRunningServices(Int.MAX_VALUE)) {
        if (this.name == service.service.className) {
            Process.killProcess(service.pid)
            return
        }
    }
}

/*
fun Class<out Service>.toggleService(context: Context) {
        if (isServiceRunning(context)) {
            stopService(context, bleServiceConnection)
        } else {
            startService(context, bleServiceConnection)
        }
}*/