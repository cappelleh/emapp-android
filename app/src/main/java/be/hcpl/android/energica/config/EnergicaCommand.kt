package be.hcpl.android.energica.config

import com.github.pires.obd.commands.protocol.ObdRawCommand

open class EnergicaCommand : ObdRawCommand("001") {

    override fun getName() = "Energica specific command (no details)"

    open fun generateDummyData() { /* provide specific dummy data */ }

}