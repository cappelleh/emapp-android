package be.hcpl.android.energica.services.generic

import be.hcpl.android.energica.model.evmonitor.PushDataInput
import be.hcpl.android.energica.model.evmonitor.PushDataOutput
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface GenericCloudApiService {

    @POST
    fun pushData(@Url url: String, @Body content: List<PushDataInput>): Call<PushDataOutput>
        // @Field("token") token: String, @FieldMap data: Map<String, @JvmSuppressWildcards Any>): Call<PushDataOutput>
}

class GenericCloudApiServiceImpl() {

    // example push data url
    // https://c2f2-2a02-1808-200-7eeb-508c-9f0f-dc73-5d68.eu.ngrok.io/api/VSQu6LvYgeargUMZ/items/create
    fun getService(): GenericCloudApiService = getRetrofit().create(GenericCloudApiService::class.java)

    private fun getRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.apply { interceptor.level = HttpLoggingInterceptor.Level.BODY }
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        val gson = GsonBuilder().setLenient().create()
        val baseUrl = "http://base/" // ignored
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

}