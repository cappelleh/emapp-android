package be.hcpl.android.energica.ui.settings

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import be.hcpl.android.energica.R
import be.hcpl.android.energica.helpers.Const
import be.hcpl.android.energica.helpers.Const.DEFAULT_BLE_AUTO_CONNECT
import be.hcpl.android.energica.helpers.Help
import be.hcpl.android.energica.services.evmonitor.EvMonitorRepo

class SettingsFragment : PreferenceFragmentCompat() {

    private lateinit var prefs: SharedPreferences
    private lateinit var evMonitorService: EvMonitorRepo

    private var vehiclePreference: Preference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = Help.getSharedPrefs(requireContext())
        evMonitorService = EvMonitorRepo.instance(requireContext())

        // display current selected vehicle
        displayCurrentVehicle()

    }

    private fun displayCurrentVehicle() {
        vehiclePreference?.summary = getString(R.string.evmonitor_vehicle_info) + " current value [" + prefs.getString(
            getString(R.string.key_evmonitor_vehicle_name),
            "NOT_SET"
        ) + "]"
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        // load settings from xml resource
        setPreferencesFromResource(R.xml.app_settings, rootKey)

        // reset BLE connection preference
        val resetConnectionPreference = findPreference<Preference>(getString(R.string.key_reset_connection))
        resetConnectionPreference?.setOnPreferenceClickListener {
            resetConnection()
            true
        }

        // select ev-monitor vehicle preference
        vehiclePreference = findPreference(getString(R.string.key_evmonitor_vehicle))
        // allow picking another vehicle here
        vehiclePreference?.setOnPreferenceClickListener {
            // show a loading indication
            val builder = android.app.AlertDialog.Builder(requireContext(), R.style.AlertDialogStyle)
            builder.setMessage("Retrieving vehicles to pick from, please wait...")
            val dialog = builder.show()
            // get vehicles
            evMonitorService.vehicles { availableVehicles ->
                dialog.dismiss()
                val vehicles = availableVehicles.map { vehicle -> vehicle.nickname }.toTypedArray()
                // allow user to select a vehicle from service response here
                val bldr = AlertDialog.Builder(requireContext())
                bldr.setTitle(R.string.pick_evmonitor_vehicle)
                    .setItems(vehicles) { dialog, which ->
                        // The 'which' argument contains the index position of the selected item
                        val selectedVehicleName = vehicles[which]
                        val selectedVehicleId = availableVehicles.find { it.nickname == selectedVehicleName }?.id
                        prefs.edit().putString(getString(R.string.key_evmonitor_vehicle_name), selectedVehicleName)
                            .putString(getString(R.string.key_evmonitor_vehicle), selectedVehicleId).apply()
                        displayCurrentVehicle()
                        dialog.dismiss()
                    }
                bldr.create().show()

            }
            true
        }

        // allow use of device GPS instead of bike GPS
        findPreference<SwitchPreferenceCompat>(getString(R.string.key_use_device_gps))
        // ?.setOnPreferenceChangeListener { _, newValue ->
        // eventbus removed, added check on MainActivity.onResume instead
        //     true
        // }
    }

    private fun resetConnection() {
        AlertDialog.Builder(requireContext(), R.style.AlertDialogStyle)
            .setMessage("Only reset connection on problems, you'll have to restart the bike before connect. Continue with RESET?")
            .setPositiveButton(android.R.string.ok) { _, _ ->
                resetConnectionConfirmed()
            }
            .setNegativeButton(android.R.string.cancel, null).show()
    }

    private fun resetConnectionConfirmed() {
        // eventbus removed, used to trigger the service stop if that screen was still active
        // remove some settings
        prefs.edit()?.remove(Const.SETTINGS_DEVICE)?.putBoolean(getString(R.string.key_ble_autoconnect), DEFAULT_BLE_AUTO_CONNECT)?.apply()
        Toast.makeText(requireContext(), "Bluetooth pairing reset", Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        // close keyboard first here so if user goes back some other way it's not in front of other views
        hideSoftKeyboard(view)
        super.onPause()
    }

    private fun hideSoftKeyboard(view: View?) {
        val manager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        manager?.hideSoftInputFromWindow(view?.windowToken, 0)
    }

}