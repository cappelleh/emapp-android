package be.hcpl.android.energica.services.obd2;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class ZeroBmsCommand extends ObdZeroCommand {
    private int pack_capacity_ah=0, low_battery_temp=0, high_battery_temp = 0, cellMin = 0;
    private float battVoltage = 0;

    /**
     * <p>Constructor for ObdRawCommand.</p>
     *
     * @param command a {@link String} object.
     */
    public ZeroBmsCommand() {
        super("bms interface");
    }
    public String getName() {
        return "BMS interface";
    }
    protected void readResult(InputStream in) throws IOException {
        super.readResult(in);
        parseData();
    }
    public void parseData() {
        Scanner scanner = new Scanner(rawData);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine().trim();
            String[] data = line.split(" ");
            if (data.length <= 2) continue;
            String param = data[1].trim();
            if (param.equals("pack_capacity_ah")) {
                pack_capacity_ah=Integer.parseInt(data[2].trim());
            } else if (param.equals("min_pack_temp_c")) {
                // Log.w("min_pack_temp_c SOC", data[2].trim());
                low_battery_temp=Integer.parseInt(data[2].trim());
            } else if (param.equals("max_pack_temp_c")) {
                high_battery_temp=Integer.parseInt(data[2].trim());
            } else if (param.equals("pack_voltage_mv")) {
                battVoltage=Integer.parseInt(data[2].trim())/1000;
            } else if (param.equals("lowest_cell_voltage_mv")) {
                cellMin=Integer.parseInt(data[2].trim());
            }
        }
        scanner.close();
    }

    public int getPack_capacity_ah() {
        return pack_capacity_ah;
    }
    public int getLow_battery_temp() {
        return low_battery_temp;
    }
    public int getHigh_battery_temp() {
        return high_battery_temp;
    }
    public int getCellMin() {
        return cellMin;
    }
    public float getBattVoltage() {
        return battVoltage;
    }

    public String getFormattedResult() {
        return String.format("CapacityAh = %d, battVoltage = %.0f, cellMin = %d, LowBattTemp = %d, HighBattTemp = %d", pack_capacity_ah, battVoltage, cellMin, low_battery_temp, high_battery_temp);
    }
    public boolean isValid() {
        // Test if data is valid
        return pack_capacity_ah != 0 && cellMin != 0;
    }
}
