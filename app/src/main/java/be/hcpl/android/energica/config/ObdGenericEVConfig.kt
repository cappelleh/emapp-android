package be.hcpl.android.energica.config

import com.github.pires.obd.commands.ObdCommand
import com.github.pires.obd.commands.SpeedCommand
import com.github.pires.obd.commands.control.ModuleVoltageCommand
import com.github.pires.obd.commands.control.VinCommand
import com.github.pires.obd.commands.engine.RPMCommand
import com.github.pires.obd.commands.fuel.ConsumptionRateCommand
import com.github.pires.obd.commands.fuel.FindFuelTypeCommand
import com.github.pires.obd.commands.fuel.FuelLevelCommand
import com.github.pires.obd.commands.protocol.EchoOffCommand
import com.github.pires.obd.commands.protocol.LineFeedOffCommand
import com.github.pires.obd.commands.protocol.ObdWarmstartCommand
import com.github.pires.obd.commands.protocol.SelectProtocolCommand
import com.github.pires.obd.commands.protocol.TimeoutCommand
import com.github.pires.obd.commands.temperature.AmbientAirTemperatureCommand
import com.github.pires.obd.commands.temperature.EngineCoolantTemperatureCommand
import com.github.pires.obd.enums.ObdProtocols

object ObdGenericEVConfig : ObdConfig {

    override val repeatableCommands: List<ObdCommand>
        get() {
            return listOf(
                // these are looped
                ModuleVoltageCommand(), // ObdRawCommand("0142"), // "12V voltage"), // (256*A+B)/1000
                AmbientAirTemperatureCommand(), //ObdRawCommand("0146"), // "Amb air temp"), // A-40
                EngineCoolantTemperatureCommand(),
                ConsumptionRateCommand(),
                FuelLevelCommand(),
                RPMCommand(),
                SpeedCommand(), //ObdRawCommand("010D"), // "vehicle speed"), // (formula A in decimal max 255)
                HybridBatteryPackCommand(), //ObdRawCommand("01 5B"), // "Hybrid battery pack remaining life"), // 100/255*A use this for Zero SOC
                EVehicleSystemDataCommand(), //ObdRawCommand("01 9A"), // "Hybrid/EV Vehicle System Data"),
                OdoMeterCommand(), //ObdRawCommand("01 A6"), // "odometer"), // formula is (A2^24 + B2^16 + C*2^8 + D) / 10
            )
        }

    override val singleCommands: List<ObdCommand>
        get() {
            return listOf(
                // overview of all AT commands for this chipset to be found here https://www.sparkfun.com/datasheets/Widgets/ELM327_AT_Commands.pdf
                ObdWarmstartCommand(), //ObdRawCommand("AT WS"), // "warm start", true), // this gives info on the used OBD dongle
                EchoOffCommand(), //ObdRawCommand("AT E0"), // "echo OFF"),
                LineFeedOffCommand(), //ObdRawCommand("AT L0"), // "linefeed OFF"),
                TimeoutCommand(62), //ObdRawCommand("AT S0"), // "set timeout"),
                SelectProtocolCommand(ObdProtocols.AUTO), //ObdRawCommand("AT SP6"), // "set protocol"),
                //AdaptiveTimingCommand(1), //ObdRawCommand("AT AT1"), // "adaptive timing"),
                // note that most of the default obd commands from lib won't work with headers ON
                //ObdRawCommand("ATH1"), // "headers ON", false),

                // generic EV OBD data
                VinCommand(),
                FindFuelTypeCommand(),
            )

        }
}
