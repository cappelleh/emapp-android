package be.hcpl.android.energica.services.gps;

import static android.app.PendingIntent.FLAG_IMMUTABLE;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_EVENT;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_FILTER;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_GPS_STATE;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_LOCATION;
import static be.hcpl.android.energica.helpers.Const.GPS_STATE;
import static be.hcpl.android.energica.helpers.Const.LOCATION_LAT;
import static be.hcpl.android.energica.helpers.Const.LOCATION_LON;
import static be.hcpl.android.energica.helpers.Const.LOCATION_PRECISION;
import static be.hcpl.android.energica.helpers.Const.LOCATION_SPEED;
import static be.hcpl.android.energica.helpers.Const.STATE_GPS_STOPPED;
import static be.hcpl.android.energica.services.evmonitor.EvMonitorRepo.Companion;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import java.util.Date;
import java.util.List;

import be.hcpl.android.energica.MainActivity;
import be.hcpl.android.energica.R;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.helpers.Help;
import be.hcpl.android.energica.model.evmonitor.PushDataInput;
import be.hcpl.android.energica.services.evmonitor.EvMonitorRepo;

@SuppressLint("MissingPermission") // permission are in manifest
public class GpsService extends Service {

    // note that caller of this service is responsible for setting up the permission requests

    public static final int NOTIFICATION_ID = 987;
    public static final String NOTIFICATION_CHANNEL_NAME = "GPS Tracking";
    public static final String NOTIFICATION_CHANNEL_ID = "gps-channel";
    public static final String NOTIFICATION_TEXT = "GPS Tracking";
    public static final String GPX_CLOSE_TAGS_TRKSEG_TRK_GPX = "\n</trkseg></trk></gpx>";

    private LocationManager locationManager = null;
    private LocationListener locationListener;
    private static final int LOCATION_INTERVAL = 1_000; // ms in between updates
    private static final float LOCATION_DISTANCE = 5; // meters in between updates

    private SharedPreferences prefs;
    private boolean loggingToFile = false;
    private String log2fileName;
    private EvMonitorRepo evMonitorRepo;

    // alternative way of stopping service
    public static Boolean running = false;

    // region logging

    private void log(final String message) {
        ExportData.Companion.log(getApplicationContext(), message); // logs to system file for in app use
    }

    private void sendStateUpdate(final int state) {
        final Bundle bundle = new Bundle();
        bundle.putInt(GPS_STATE, state);
        sendBroadcast(BROADCAST_GPS_STATE, bundle);
    }

    private void createNewGpxLogFile() {
        log2fileName = "route-" + ExportData.Companion.getTimeFormatFile().format(System.currentTimeMillis()) + ".gpx";
        // GPX file header
        String content = ExportData.GPX_HEADER + "<trk><name>" + log2fileName + " tracked on device " + Build.MANUFACTURER + " " + Build.MODEL + "</name><trkseg>";
        // Proper closing of GPX content (this part will be replaced when adding points
        content += GPX_CLOSE_TAGS_TRKSEG_TRK_GPX;
        ExportData.Companion.writeToNewFile(log2fileName, content, getApplicationContext());
        log("GPS - created new file for logging from background service: " + log2fileName);
    }

    private void appendToGpxLogFile(final Location location) {
        // clear closing xml tags first
        ExportData.Companion.clearLastLineOfFile(log2fileName, getApplicationContext());
        // then append new content
        String segments = "\n<trkpt lat=\"" +
                location.getLatitude() + "\" lon=\"" + location.getLongitude() +
                "\"><time>" + ExportData.Companion.getDateFormatGpx().format(
                new Date(location.getTime())) +
                "</time></trkpt>";
        // also closing it
        segments += GPX_CLOSE_TAGS_TRKSEG_TRK_GPX;
        ExportData.Companion.appendToFile(log2fileName, segments, getApplicationContext());
    }

    // endregion

    // region handle location updates

    private void pushLocation(Location location) {
        // collection and then update visually when the app is resumed
        sendLocation(location);
        // optional cloud data sharing here
        evMonitorRepo.pushLocation(location.getLatitude(), location.getLongitude(), location.getAccuracy(), location.getSpeed());
        // on each iteration also check if the logging condition has changed so we can create a new log file if needed
        boolean shouldLog = prefs.getBoolean(getString(R.string.key_log_to_file), true);
        if (shouldLog && !loggingToFile) {
            // create log file
            createNewGpxLogFile();
            loggingToFile = true;
        } else if (!shouldLog && loggingToFile) {
            // stop logging
            loggingToFile = false;
        }
        // update log GPX route directly to file system if option is enabled
        if (loggingToFile) {
            // append new location to log file here
            appendToGpxLogFile(location);
        }
    }

    private void sendLocation(Location location) {
        if (location == null) return;
        final Bundle bundle = new Bundle();
        bundle.putDouble(LOCATION_LAT, location.getLatitude());
        bundle.putDouble(LOCATION_LON, location.getLongitude());
        bundle.putFloat(LOCATION_SPEED, location.getSpeed());
        bundle.putFloat(LOCATION_PRECISION, location.getAccuracy());
        sendBroadcast(BROADCAST_LOCATION, bundle);
    }

    private void sendBroadcast(String event, Bundle bundle) {
        Intent intent = new Intent(BROADCAST_FILTER);
        intent.putExtra(BROADCAST_EVENT, event);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        sendBroadcast(intent);
    }

    // endregion

    // region keep service running in background mode with notification

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        log("GPS - onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Notification getNotification() {
        final PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), MainActivity.class), FLAG_IMMUTABLE);
        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel); // channel ID needed here
        Notification.Builder builder = new Notification.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_action_gps_fixed);
        builder.setContentText(NOTIFICATION_TEXT);
        builder.setAutoCancel(true); // TODO test better service retention changes
        builder.setContentIntent(contentIntent);
        return builder.build();
    }

    private Notification getCompatNotification() {
        final PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), MainActivity.class), FLAG_IMMUTABLE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
        builder.setSmallIcon(R.drawable.ic_action_gps_fixed)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(NOTIFICATION_TEXT)
                .setAutoCancel(true) // TODO test better service retention changes
                .setPriority(NotificationCompat.PRIORITY_DEFAULT) // TODO test better service retention changes
                //.setOngoing(true) // TODO test better service retention changes
                .setContentIntent(contentIntent);
        return builder.build();
    }

    private void cancelNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            stopForeground(true);
        } else {
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.cancel(NOTIFICATION_ID);
        }
    }

    // endregion

    // region lifecycle

    @Override
    public void onCreate() {
        log("GPS - onCreate");
        running = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(NOTIFICATION_ID, getNotification());
            log("GPS - started as foreground service for Android 8 & up");
        } else {
            //NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            //notificationManager.notify(NOTIFICATION_ID, getCompatNotification());
            startForeground(NOTIFICATION_ID, getCompatNotification()); // TODO test better service retention changes
            log("GPS - showing compat notification for running service");
        }
        // when service run in separate process with xml process config these don't get updated anymore, instead use onStartCommand(intent...)
        prefs = Help.getSharedPrefs(getApplicationContext());
        evMonitorRepo = Companion.instance(getApplicationContext());
        initializeLocationManager();
        try {
            // fetch precision location using GPS provider
            locationListener = new LocationListener(LocationManager.GPS_PROVIDER);
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    locationListener);
            // try getting last known location to begin with
            Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (lastKnownLocation != null) {
                pushLocation(lastKnownLocation);
            }
        } catch (SecurityException ex) {
            log("GPS - fail to request location update, ignore: " + ex.getMessage());
        } catch (IllegalArgumentException ex) {
            log("GPS - gps provider does not exist: " + ex.getMessage());
        }
        // also log how many entries are currently in cache
        final List<PushDataInput> cached = ExportData.Companion.retrieveCachedData(getApplicationContext());
        final int cacheSize = cached.size();
        if (!cached.isEmpty()) cached.clear();
        log("GPS - number of datapoints left in cache on service create: " + cacheSize);
    }

    @Override
    public void onDestroy() {
        log("GPS - onDestroy");
        // flush all collected data to cloud
        evMonitorRepo.flushData();
        // stop gps updates
        if (locationManager != null && locationListener != null) {
            try {
                locationManager.removeUpdates(locationListener);
            } catch (Exception ex) {
                log("GPS - fail to remove location listeners, ignore: " + ex.getMessage());
            }
        }
        // state update for whatever view is present atm
        sendStateUpdate(STATE_GPS_STOPPED);
        // remove notification
        cancelNotification();
        stopSelf();
        running = false;
        super.onDestroy();
    }

    // endregion

    // region receive location updates

    private void initializeLocationManager() {
        log("GPS - initializeLocationManager");
        if (locationManager == null) {
            locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private class LocationListener implements android.location.LocationListener {

        private int previousState = 0;

        private final Location lastLocation;

        public LocationListener(String provider) {
            log("GPS - LocationListener created with provider: " + provider);
            lastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(@NonNull Location location) {
            if (!running) {
                onDestroy(); // clean up
                return;
            }
            lastLocation.set(location);
            pushLocation(location);
            sendStateUpdate(STATE_GPS_STOPPED);
        }

        @Override
        public void onProviderDisabled(@NonNull String provider) {
            log("GPS - onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(@NonNull String provider) {
            log("GPS - onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int newState, Bundle extras) {
            // no need to keep logging this, too much data
            if (previousState != newState) {
                previousState = newState;
                log("GPS - onStatusChanged for [" + provider + "] new state: " + newState);
            }
        }
    }

    // endregion

}
