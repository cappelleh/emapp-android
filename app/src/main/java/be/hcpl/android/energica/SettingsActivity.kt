package be.hcpl.android.energica

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import be.hcpl.android.energica.helpers.Help.setStateColorFor
import be.hcpl.android.energica.services.ble.BleService
import be.hcpl.android.energica.services.gps.GpsService
import be.hcpl.android.energica.services.obd2.Obd2Service
import be.hcpl.android.energica.ui.settings.SettingsFragment

class SettingsActivity : AppCompatActivity(R.layout.activity_settings) {

    // toolbar state icons
    private lateinit var gpsStateView: ImageView
    private lateinit var bleStateView: ImageView
    private lateinit var obdStateView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings_container, SettingsFragment())
            .commit()

        // find all views
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val titleView = findViewById<TextView>(R.id.toolbar_title)
        gpsStateView = findViewById(R.id.gps_state)
        bleStateView = findViewById(R.id.ble_state)
        obdStateView = findViewById(R.id.obd_state)

        // set toolbar and title
        setSupportActionBar(toolbar)
        titleView.text = getString(R.string.menu_settings)
    }

    override fun onResume() {
        super.onResume()
        // update state of services
        updateServiceStates()
    }

    private fun updateServiceStates() {
        setStateColorFor(gpsStateView, GpsService::class.java)
        setStateColorFor(bleStateView, BleService::class.java)
        setStateColorFor(obdStateView, Obd2Service::class.java)
    }

    // region menu

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.close_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.close -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    // endregion

}