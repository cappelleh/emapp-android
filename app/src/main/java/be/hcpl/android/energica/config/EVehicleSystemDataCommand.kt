package be.hcpl.android.energica.config

import com.github.pires.obd.commands.protocol.ObdRawCommand

class EVehicleSystemDataCommand : ObdRawCommand("01 9A") {

    //ObdRawCommand("01 9A"), // "Hybrid/EV Vehicle System Data"),

    override fun getName() = "EV System Data"

    override fun getFormattedResult(): String {
        return try {
            result // example data needed? How should we parse this, not very useful for Energica
        } catch(e: Exception) {
            "ERROR ${e.localizedMessage} RAW=$result"
        }
    }

}