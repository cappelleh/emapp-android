package be.hcpl.android.energica.helpers

import android.content.Context
import android.os.Build
import android.os.Environment
import android.util.Log
import be.hcpl.android.energica.model.evmonitor.PushDataInput
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.io.RandomAccessFile
import java.lang.System.currentTimeMillis
import java.lang.reflect.Type
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale.getDefault


class ExportData {

    companion object {

        private const val TAG = "ExportData"
        val timeFormatFile: DateFormat by lazy { SimpleDateFormat("yyyy-MM-dd-HHmmss", getDefault()) }
        val timeFormatLogStatement: DateFormat by lazy { SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", getDefault()) }
        val dateFormatGpx: DateFormat by lazy { SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", getDefault()) }

        const val GPX_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?><gpx xmlns=\"http://www.topografix.com/GPX/1/1\" " +
                "creator=\"MapSource 6.15.5\" version=\"1.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  " +
                "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">\n"

        private var logFileTimeStamp: String? = null
        private var appLogFile: File? = null
        private val appLogDateFormat: DateFormat by lazy { SimpleDateFormat("yyyy-MM-dd", getDefault()) }

        // used for local fs data caching
        private const val CACHE_FILENAME = "evmonitor-cache.txt"
        private val gson: Gson = GsonBuilder().setLenient().create()
        private var pushDataInputType: Type = object : TypeToken<ArrayList<PushDataInput?>?>() {}.type

        fun getCurrentAppLogFile(context: Context): File? {
            val currentTimestamp = appLogDateFormat.format(Date())
            if (appLogFile == null || logFileTimeStamp != currentTimestamp) {
                // make a new log file every hour (at least)
                logFileTimeStamp = currentTimestamp
                appLogFile = File(
                    context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                    "${logFileTimeStamp}-${Const.APP_LOGS_FILE}"
                )
            }
            return appLogFile
        }

        fun log(context: Context, message: String) {
            Log.d(TAG, message)
            try {
                appLogFile = getCurrentAppLogFile(context)
                if (appLogFile?.exists() == false) {
                    appLogFile?.createNewFile()
                    // create a header with device manufacturer and model
                    appLogFile?.appendText("${timeFormatLogStatement.format(currentTimeMillis())} log file created on device ${Build.MANUFACTURER} ${Build.MODEL}\n")
                    // also add cache size on each start of log file
                    // also log how many entries are currently in cache
                    val cached = retrieveCachedData(context)
                    val cacheSize = cached.size
                    appLogFile?.appendText("${timeFormatLogStatement.format(currentTimeMillis())} cache size ${cacheSize}\n")
                }
                appLogFile?.appendText("${timeFormatLogStatement.format(currentTimeMillis())} ${message}\n")
            } catch (e: Exception) {
                e.message?.let { Log.e(TAG, it) }
            }
        }

        fun exportData(context: Context, series: LineGraphSeries<DataPoint>, name: String, rangeStart: Double, rangeEnd: Double) {
            val speedValues = series.getValues(rangeStart, rangeEnd)
            val data = StringBuilder()
            while (speedValues.hasNext()) {
                val dataPoint = speedValues.next()
                data.append(dataPoint.x).append(";").append(dataPoint.y).append(System.lineSeparator())
            }
            writeToNewFile(
                "export-$name-${timeFormatFile.format(currentTimeMillis())}.txt", data.toString(),
                context
            )
        }

        fun writeToNewFile(fileName: String, data: String, context: Context) {
            try {
                val filePath = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                val fileWriter = FileWriter(File(filePath, fileName))
                fileWriter.write(data)
                fileWriter.flush()
                fileWriter.close()
            } catch (e: Exception) {
                log(context, "File write failed: $e")
                e.printStackTrace()
            }
        }

        fun appendToFile(fileName: String, data: String, context: Context) {
            try {
                val filePath = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                val fileWriter = FileWriter(File(filePath, fileName), true)
                fileWriter.write(data)
                fileWriter.flush()
                fileWriter.close()
            } catch (e: Exception) {
                log(context, "File write failed: $e")
                e.printStackTrace()
            }
        }

        /**
         * helper to remove everything after the last newline in the given file
         * source: https://stackoverflow.com/questions/9149648/deleting-the-last-line-of-a-file-with-java?rq=3
         */
        fun clearLastLineOfFile(fileName: String, context: Context) {
            try {
                val filePath = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                RandomAccessFile(File(filePath, fileName), "rw").use { raf ->
                    val fileLength = raf.length()
                    if (fileLength == 0L) {
                        // File is empty, nothing to delete
                        return
                    }

                    // Start searching for the last newline character from the end of the file
                    var position = fileLength - 1
                    raf.seek(position)

                    var lastByte: Int
                    while ((raf.read().also { lastByte = it }) != -1) {
                        if (lastByte == '\n'.code) {
                            // Found the last newline character
                            break
                        }
                        position--
                        raf.seek(position)
                    }

                    // Truncate the file at the position of the last newline character
                    raf.setLength(position)
                }
            } catch (e: IOException) {
                log(context, "Failed to remove last line from file: $e")
                e.printStackTrace()
            }
        }

        // region cached data handling

        fun retrieveCachedData(context: Context): List<PushDataInput> {
            // get all cached data elements
            val cacheFile = prepareCacheFile(context)
            val dataString = cacheFile.readText()
            return try {
                gson.fromJson(dataString, pushDataInputType)
            } catch (e: Exception) {
                e.printStackTrace()
                emptyList()
            }
        }

        fun addToCachedData(context: Context, data: List<PushDataInput>) {
            if (data.isEmpty()) return
            val cacheFile = prepareCacheFile(context)
            // to not mess up the json format get the current content first
            val cachedData = retrieveCachedData(context)
            val fullSet = cachedData.toMutableList()
            fullSet.addAll(data) // combine with new data
            val dataString = gson.toJson(fullSet)
            cacheFile.writeText(dataString)
            // TODO debug if append is needed here instead
            //cacheFile.appendText(dataString)
        }

        fun clearCachedData(context: Context) {
            val cacheFile = prepareCacheFile(context)
            cacheFile.delete()
        }

        private fun prepareCacheFile(context: Context): File {
            // prepare cache file if needed
            val cacheFile = File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), CACHE_FILENAME)
            if (!cacheFile.exists()) {
                cacheFile.createNewFile()
            }
            return cacheFile
        }

        // endregion

    }
}
