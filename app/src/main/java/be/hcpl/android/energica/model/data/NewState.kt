package be.hcpl.android.energica.model.data

import java.io.Serializable

data class NewState(val state: Int) : Serializable
