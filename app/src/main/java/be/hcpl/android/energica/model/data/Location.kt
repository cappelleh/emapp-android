package be.hcpl.android.energica.model.data

import android.location.Location
import java.io.Serializable

data class LocationData(val location: Location) : Serializable
