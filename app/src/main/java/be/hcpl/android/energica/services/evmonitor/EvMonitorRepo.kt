package be.hcpl.android.energica.services.evmonitor

import android.content.Context
import be.hcpl.android.energica.R
import be.hcpl.android.energica.config.HybridBatteryPackCommand
import be.hcpl.android.energica.config.OdoMeterCommand
import be.hcpl.android.energica.helpers.ExportData
import be.hcpl.android.energica.helpers.Help
import be.hcpl.android.energica.model.ble.VehicleStatus
import be.hcpl.android.energica.model.evmonitor.LoginOutput
import be.hcpl.android.energica.model.evmonitor.PushDataInput
import be.hcpl.android.energica.model.evmonitor.PushDataOutput
import be.hcpl.android.energica.model.evmonitor.Vehicle
import be.hcpl.android.energica.model.evmonitor.VehiclesOutput
import be.hcpl.android.energica.services.generic.GenericCloudRepo
import be.hcpl.android.energica.services.mqtt.MqttRepo
import com.github.pires.obd.commands.ObdCommand
import com.github.pires.obd.commands.SpeedCommand
import com.github.pires.obd.commands.engine.RPMCommand
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference
import kotlin.math.max

class EvMonitorRepo private constructor(private val context: Context) {

    private val prefs = Help.getSharedPrefs(context)
    private var service = EvMonitorApiServiceImpl().getService(context)
    private val data = mutableListOf<PushDataInput>() // in memory local data, this is kept as long as needed (that is while uploads fail)
    private val genericCloudRepo = GenericCloudRepo(context)
    private val mqttRepo = MqttRepo(context)

    companion object {

        private const val DEFAULT_CHUNK_SIZE = 500 // how many entries to push at once
        private const val DEFAULT_DELAY_BETWEEN_PUSHES = 30 // minimal time in between remote data pushes
        private const val MAX_DATA_SIZE_GPS = 60 // how much data to keep local before pushing
        private const val MAX_DATA_SIZE_OBD = 10 // how much data to keep local before pushing
        private const val MAX_DATA_SIZE_BLE = 30 // how much data to keep local before pushing
        private const val RESPONSE_OK = "OK" // what we receive when service response was OK

        private var instance: WeakReference<EvMonitorRepo?> = WeakReference(null)

        fun instance(context: Context): EvMonitorRepo {
            if (instance.get() == null) {
                instance = WeakReference(EvMonitorRepo(context))
            }
            return instance.get()!!
        }

    }

    private fun log(message: String) {
        ExportData.log(context, message)
    }

    fun dropData() {
        data.clear()
        ExportData.clearCachedData(context)
    }

    private fun speedInPreferredUnit(command: SpeedCommand): Float {
        return if (context.getString(R.string.value_imperial) == context.getString(R.string.key_unit_system))
            command.imperialSpeed
        else
            command.metricSpeed.toFloat()
    }

    // region ev monitor login

    /**
     * get new token with configured username and password combination
     */
    private fun updateToken(withToken: (token: String, vehicle: String) -> Unit) {
        // TODO check if we can keep track of time for tokens?
        // less of an issue if we flush all data from time to time
        val username = evMonitorUserName
        val password = evMonitorPassword
        if (username == null || password == null) {
            log("failed to login for token - no login configured")
            return
        }
        service.login(username, password).enqueue(object : Callback<LoginOutput> {

            override fun onResponse(call: Call<LoginOutput>, response: Response<LoginOutput>) {
                if (!response.isSuccessful) {
                    log("failed to login for token")
                    return
                }
                response.body()?.let {
                    val vehicle = evMonitorVehicleId.orEmpty()
                    withToken(it.token.orEmpty(), vehicle)
                }
            }

            override fun onFailure(call: Call<LoginOutput>, t: Throwable) {
                log("failed to login for token - " + t.localizedMessage)
            }
        })
    }

    /**
     * retrieve all vehicles configured for this user
     */
    fun vehicles(withVehicles: (vehicles: List<Vehicle>) -> Unit) {
        updateToken { token, currentVehicleId ->
            service.vehicles(token).enqueue(object : Callback<VehiclesOutput> {

                override fun onResponse(
                    call: Call<VehiclesOutput>,
                    response: Response<VehiclesOutput>
                ) {
                    if (!response.isSuccessful) {
                        log("failed to retrieve vehicle info")
                        return
                    }
                    response.body()?.let {
                        val availableVehicles =
                            it.vehicles?.map { vehicle -> vehicle.copy(selected = vehicle.id == currentVehicleId) }
                                .orEmpty()
                        withVehicles(availableVehicles)
                    }
                }

                override fun onFailure(call: Call<VehiclesOutput>, t: Throwable) {
                    log("failed to retrieve vehicle info - " + t.localizedMessage)
                }
            })
        }
    }

    // endregion

    // region helpers for settings

    private val evMonitorPushEnabled: Boolean
        get() = prefs.getBoolean(context.getString(R.string.key_enable_evmonitor), false)

    private val pushChunkSize: Int
        get() = prefs.getString(context.getString(R.string.key_push_location_chunk_size), null)?.toInt() ?: DEFAULT_CHUNK_SIZE

    private val pushDelay: Long
        get() = 1000L * (prefs.getString(context.getString(R.string.key_push_delay_between), null)?.toInt() ?: DEFAULT_DELAY_BETWEEN_PUSHES)

    private val sizeLocalDataSetLocation: Int
        get() = prefs.getString(context.getString(R.string.key_push_location_trigger_size), null)?.toInt() ?: MAX_DATA_SIZE_GPS

    private val includeDebugEnabled: Boolean
        get() = prefs.getBoolean(context.getString(R.string.key_enable_evmonitor_debug), false)

    private val evMonitorUserName: String?
        get() = prefs.getString(context.getString(R.string.key_evmonitor_username), null)

    private val evMonitorPassword: String?
        get() = prefs.getString(context.getString(R.string.key_evmonitor_password), null)

    private val evMonitorVehicleId: String?
        get() = prefs.getString(context.getString(R.string.key_evmonitor_vehicle), null)

    private val pushMqttEnabled: Boolean
        get() = prefs.getBoolean(context.getString(R.string.key_enable_mqtt), false)

    private val pushGenericCloudEnabled: Boolean
        get() = prefs.getBoolean(context.getString(R.string.key_enable_generic_cloud), false)

    // endregion

    // region data handling

    /**
     * force push all data to remote, use this on finishing services for example
     * All push___ functions redirect to this single flush
     */
    fun flushData() {

        // we have local (in memory only) storage and file (cached) storage
        // create a dataset of what should be pushed, this is both in memory and file storage
        val localDataSet = data.toMutableList()

        // push generic content if enabled, note that for now this will only push data while the app
        // is running and when a connection is live. The fs cache system in place for EVmonitor.com
        // that enables pushing data on a later time when connection is restored is not used here
        genericCloudRepo.flushData(localDataSet)

        // optionally push data to MQTT services also
        mqttRepo.flushData(localDataSet)

        // retrieve data from file storage, that is everything not yet uploaded
        localDataSet.addAll(ExportData.retrieveCachedData(context))
        val dataSet = localDataSet.toList()
        // before clearing local in memory data write to file storage, this way data isn't lost
        ExportData.addToCachedData(context, data)
        data.clear() // clear local memory here, next time will be used from file storage

        // don't push anything if we have nothing at this point
        if (dataSet.isEmpty()) {
            log("no data available to push")
            return
        }

        // push all data we've collected at this point if enabled in preferences
        // already triggered from time to time by services, also to be triggered when a service stops for example
        if (!evMonitorPushEnabled) return

        // start authentication process for ev-monitor.com data pushing
        updateToken { token, vehicle ->
            // push data in chunks of 500 with updates to file and delays between pushes and results checked
            val chunked = dataSet.chunked(pushChunkSize)
            // handle all data chunks recursively, starting at index 0
            recursivelyPushAllData(vehicle, token, chunked, 0)
        }
    }

    private fun recursivelyPushAllData(vehicle: String, token: String, chunked: List<List<PushDataInput>>, index: Int) {
        // when we have valid data to push (could run out of index etc)
        if (index < chunked.size && chunked[index].isNotEmpty()) {
            // go ahead and push that data checking result at this point
            pushDataToRemote(vehicle, token, chunked[index]) { result ->
                if (result) {
                    // if push was OK remove that part and only that part from file storage (that way we keep the non succeeded chunks)
                    val cachedData = ExportData.retrieveCachedData(context)
                    ExportData.clearCachedData(context) // clears current file storage
                    ExportData.addToCachedData(context, cachedData.minus(chunked[index].toSet())) // writes back updated collection
                }
                // add a small delay between data pushes here to prevent overloading services
                GlobalScope.launch {
                    delay(pushDelay)
                    // and once pushed continue with the next set
                    recursivelyPushAllData(vehicle, token, chunked, index + 1)
                }
            }
        }
    }

    private fun pushDataToRemote(vehicle: String, token: String, dataSet: List<PushDataInput>, next: (result: Boolean) -> Unit) {

        // construct a map for all data, this is the expected format
        val dataMap = mutableMapOf<String, Any>()
        dataSet.forEachIndexed { index, element ->
            dataMap.putAll(element.toDataMap(vehicle, index))
        }
        // actual data push goes here
        service.pushData(token, dataMap).enqueue(object : Callback<PushDataOutput> {

            override fun onResponse(
                call: Call<PushDataOutput>,
                response: Response<PushDataOutput>
            ) {
                if (!response.isSuccessful) {
                    log("failed to push data")
                    next(false) // mark as failed
                } else {
                    response.body()?.let {
                        if (it.SUCCESS != RESPONSE_OK) {
                            log("failed to push data - " + it.error)
                            next(false) // mark as failed
                        } else {
                            // mark this set as completely flushed
                            next(true) // mark all is OK
                        }
                        log("pushed data, response was ${it.SUCCESS}")
                    } ?: next(false) // mark failed
                }
            }

            override fun onFailure(call: Call<PushDataOutput>, t: Throwable) {
                log("failed to push data - " + t.localizedMessage)
                next(false) // mark failed
            }
        })
    }

    // endregion

    /**
     * record location information from GPS
     */
    fun pushLocation(latitude: Double, longitude: Double, accuracy: Float, speed: Float) {
        // block if feature isn't enabled
        if (noRemoteServicesEnabled()) return

        // prevent pushing empty values
        if (latitude == 0.0 && longitude == 0.0 && accuracy == 0f && speed == 0f) return
        // add some form of data collection and push set from time to time to reduce amount of requests
        data.add(
            PushDataInput(
                timestamp = System.currentTimeMillis() / 1000,
                gpsAcc = accuracy,
                latitude = latitude,
                longitude = longitude,
                speed = speed
            )
        )
        // if enough data collected flush
        if (data.size > sizeLocalDataSetLocation) flushData()
    }

    /**
     * push energica specific data (batt. temp, soh, soc, charge current)
     */
    fun pushEnergicaObd2Data(
        temp1: Int? = null,
        temp2: Int? = null,
        soh: Int? = null,
        soc: Int? = null,
        voltage: Double? = null,
        current: Double? = null,
        power: Double? = null,
        chargeState: Int? = null,
        cellMin: Int? = null,
        cellMax: Int? = null,
        odoMeter: Int? = null
    ) {

        //  don't when cloud services aren't enabled
        if (noRemoteServicesEnabled()) return

        // debug data separately handled (since it would be 0 anyway), see #pushEnergicaRawData

        // prevent pushing empty values
        if (temp1 == 0 && temp2 == 0 && soh == 0 && soc == 0 && voltage == 0.0 && power == 0.0 && cellMin == 0 && cellMax == 0 && odoMeter == 0) return
        // prepare data to be pushed
        data.add(
            PushDataInput(
                timestamp = System.currentTimeMillis() / 1000,
                battTemp = temp1?.let { temp2?.let { max(temp1, temp2) } }, // always log highest battery temp
                battSoc = soc,
                battSoh = soh,
                battVoltage = voltage?.toFloat(),
                chargeCurrent = current?.toFloat(),
                chargePower = power?.toFloat(),
                chargeState = chargeState,
                cellMin = cellMin,
                cellMax = cellMax,
                odometer = odoMeter // for OBD2 data also push odometer value
            )
        )
        // if enough data collected flush
        if (data.size > MAX_DATA_SIZE_OBD) flushData()
    }

    /**
     * push generic obd2 data, can be used for zero and other vehicles than energica
     */
    fun pushGenericObd2Data(command: ObdCommand) {
        //  don't when cloud services aren't enabled
        if (noRemoteServicesEnabled()) return

        // prepare data to be pushed
        var pushData = PushDataInput(
            timestamp = System.currentTimeMillis() / 1000,
        )
        // enrich with debug data when feature is enabled
        if (includeDebugEnabled) {
            pushData = pushData.copy(debug = command.result)
        }
        when (command) {
            // is ModuleVoltageCommand -> pushData.copy() // maybe log 12V to data[0][general][batt][{battindex}][v]:{v:int}(Battery Voltage in V)
            // is AmbientAirTemperatureCommand -> ...
            // is EngineCoolantTemperatureCommand -> ...
            is RPMCommand -> data.add(pushData.copy(rpm = command.rpm))
            is SpeedCommand -> data.add(pushData.copy(speed = speedInPreferredUnit(command)))
            is HybridBatteryPackCommand -> data.add(pushData.copy(battSoh = command.calculatedValue())) // Zero has it implemented as SOC
            // is EVehicleSystemDataCommand -> ... //ObdRawCommand("01 9A"), // "Hybrid/EV Vehicle System Data"),
            is OdoMeterCommand -> data.add(pushData.copy(odometer = command.calculatedValue()))
        }
        // if enough data collected flush
        if (data.size > MAX_DATA_SIZE_OBD) flushData()
    }

    /**
     * publish energica specific BLE data
     */
    fun pushEnergicaBleData(status: VehicleStatus) {
        //  don't when cloud services aren't enabled
        if (noRemoteServicesEnabled()) return

        // prevent pushing empty values
        if (status.isEmpty) return
        // add some form of data collection and push set from time to time to reduce amount of requests
        data.add(
            PushDataInput(
                timestamp = System.currentTimeMillis() / 1000,
                speed = status.speed.toFloat(),
                range = status.range,
                odometer = status.totalOdometer.toInt(),
                consumption = status.instKwh100Km,
                rpm = status.rpm,
                torque = status.torque,
                power = status.power,
                battSoc = status.soc,
                battTemp = status.temp, // added battery temp
                reserve = status.resBatteryEnergy // battery energy reserve
            )
        )
        // if enough data collected flush
        if (data.size > MAX_DATA_SIZE_BLE) flushData()
    }

    /**
     * push generic raw data when debug option is enabled
     */
    fun pushEnergicaRawData(rawData: String) {
        //  don't when cloud services aren't enabled
        if (noRemoteServicesEnabled()) return
        // debug data separately handled
        if (!includeDebugEnabled) return
        // don't push empty raw responses
        if (rawData.isBlank()) return

        // prepare data to be pushed
        data.add(
            PushDataInput(
                timestamp = System.currentTimeMillis() / 1000,
                debug = rawData
            )
        )
        // if enough data collected flush
        if (data.size > MAX_DATA_SIZE_OBD) flushData()
    }

    private fun noRemoteServicesEnabled() = !anyRemoteServiceEnabled()
    private fun anyRemoteServiceEnabled() = pushMqttEnabled || pushGenericCloudEnabled || evMonitorPushEnabled

}