package be.hcpl.android.energica.model.ble;

import java.io.Serializable;

import static be.hcpl.android.energica.helpers.Const.STATE_CHARGE;

public class VehicleStatus implements Serializable {

    public float avgConsumption;
    public int avgConsumptionUnit;
    public float bPackI;
    public float bPackV;
    public float cDCC;
    public float cDCV;
    public float cMAINSC;
    public float cMAINSV;
    public int chgPowerLimit;
    public int distanceUnit;
    public float instKmKwh;
    public float instKwh100Km;
    public int model;
    public float tripMeter;
    public int power;
    public int range;
    public int temp;
    public int resBatteryEnergy;
    public int rpm;
    public int soc;
    public int speed;
    public int state;
    public int subState;
    public int torque;
    public float totalOdometer;

    @Override
    public String toString() {
        return "VehicleStatus{" +
                "avgConsumption=" + avgConsumption +
                ", avgConsumptionUnit=" + avgConsumptionUnit +
                ", bPackI=" + bPackI +
                ", bPackV=" + bPackV +
                ", cDCC=" + cDCC +
                ", cDCV=" + cDCV +
                ", cMAINSC=" + cMAINSC +
                ", cMAINSV=" + cMAINSV +
                ", chgPowerLimit=" + chgPowerLimit +
                ", distanceUnit=" + distanceUnit +
                ", instKmKwh=" + instKmKwh +
                ", instKwh100Km=" + instKwh100Km +
                ", model=" + model +
                ", partialOdometer=" + tripMeter +
                ", power=" + power +
                ", range=" + range +
                ", resBatteryEnergy=" + resBatteryEnergy +
                ", rpm=" + rpm +
                ", soc=" + soc +
                ", temp=" + temp +
                ", speed=" + speed +
                ", state=" + state +
                ", subState=" + subState +
                ", torque=" + torque +
                ", totalOdometer=" + totalOdometer +
                '}';
    }

    public boolean isEmpty() {
        return soc == 0 && totalOdometer == 0f && power == 0 && range == 0;
    }

    public boolean isCharging() { return state == STATE_CHARGE; }

    public final static int MODEL_EGO = 0;
    public final static int MODEL_EVA = 1;
    public final static int MODEL_SS9_PLUS = 5;

}
