package be.hcpl.android.energica.model.evmonitor

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class PushDataInput(
    @SerializedName("user-agent") val userAgent: String = "Energ1ca App",
    var token: String = "NO_TOKEN",
    val timestamp: Long,

    // keep null as a default so we don't push 0 values
    // collected from GPS service
    val latitude: Double? = null,
    val longitude: Double? = null,
    val gpsAcc: Float? = null,
    val speed: Float? = null,

    // from BLE and OBDII conn. data
    val range: Int? = null,
    val odometer: Int? = null,
    val consumption: Float? = null, //{mean consumption:int}(in watt/100km)

    // battery state
    val battSoc: Int? = null,
    val battSoh: Int? = null,
    val battTemp: Int? = null,
    val chargeCurrent: Float? = null, // in A
    val chargePower: Float? = null, // in kW
    val battVoltage: Float? = null, // :{v:int}(Battery Voltage in V)
    val cellMin: Int? = null, // :{vmin:int}(lowest cellvoltage in mV)
    val cellMax: Int? = null,
    val reserve: Int? = null, // limited to BLE

    // motor output
    val rpm: Int? = null,
    val torque: Int? = null,
    val power: Int? = null,

    // charging state, idle=1, ac=2 or dc=10
    val chargeState: Int? = null,

    val debug: String? = null, // debug data
) {


    fun toDataMap(vehicle: String, index: Int) : Map<String, Any> {
        val data = mutableMapOf<String, Any>(
            "user-agent" to this.userAgent,
            "data[${index}][general][timestamp]" to this.timestamp, // full seconds, not ms
            "data[${index}][general][vehicle]" to vehicle, //?:this.vehicle, // overwrites vehicle when set
        )
        // add all optional fields here
        this.latitude?.let{ data.put("data[${index}][general][gps][lat]", this.latitude) }
        this.longitude?.let{ data.put("data[${index}][general][gps][lng]", this.longitude) }
        this.gpsAcc?.let{ data.put("data[${index}][general][gps][acc]", this.gpsAcc) }
        this.speed?.let{ data.put("data[${index}][general][speed]", this.speed) }

        this.range?.let{ data.put("data[${index}][general][range]", this.range) }
        this.odometer?.let{ data.put("data[${index}][general][odometer]", this.odometer) }
        this.consumption?.let{ data.put("data[${index}][general][watt_100km]", this.consumption) }

        this.battSoc?.let{ data.put("data[${index}][general][batt][0][soc]", this.battSoc) }
        this.battSoh?.let{ data.put("data[${index}][general][batt][0][soh]", this.battSoh) }
        this.battTemp?.let{ data.put("data[${index}][general][batt][0][temp]", this.battTemp) }
        this.chargeCurrent?.let{ data.put("data[${index}][general][batt][0][i_charge]", this.chargeCurrent) }
        this.battVoltage?.let{ data.put("data[${index}][general][batt][0][v]", this.battVoltage) }
        this.reserve?.let{ data.put("data[${index}][general][batt][0][reserve]", this.reserve) }

        this.rpm?.let{ data.put("data[${index}][general][motor][0][rpm]", this.rpm) }
        this.torque?.let{ data.put("data[${index}][general][motor][0][torque]", this.torque) }
        this.power?.let{ data.put("data[${index}][general][motor][0][watt]", this.power) }

        this.debug?.let{ data.put("data[${index}][general][debug]", this.debug) }

        return data
    }

}

