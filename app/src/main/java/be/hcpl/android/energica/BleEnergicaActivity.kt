package be.hcpl.android.energica

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.ServiceConnection
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import be.hcpl.android.energica.helpers.Const
import be.hcpl.android.energica.helpers.Const.BLE_GENERIC_EVENT
import be.hcpl.android.energica.helpers.Const.BLE_STATE
import be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_EVENT
import be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_GENERIC_EVENT
import be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_STATE
import be.hcpl.android.energica.helpers.Const.SETTINGS_DEVICE
import be.hcpl.android.energica.helpers.Const.STATE_NOT_CONNECTED
import be.hcpl.android.energica.helpers.ExportData.Companion.log
import be.hcpl.android.energica.helpers.Help.getSharedPrefs
import be.hcpl.android.energica.helpers.Help.launchView
import be.hcpl.android.energica.helpers.Help.setStateColorFor
import be.hcpl.android.energica.helpers.Help.withColor
import be.hcpl.android.energica.model.data.ChargeLimit
import be.hcpl.android.energica.model.data.ConnectionRejected
import be.hcpl.android.energica.model.data.LocationData
import be.hcpl.android.energica.model.data.NewState
import be.hcpl.android.energica.pagers.ViewPagerAdapterMain
import be.hcpl.android.energica.services.ble.BleService
import be.hcpl.android.energica.services.ble.ConnectionManager
import be.hcpl.android.energica.services.gps.GpsService
import be.hcpl.android.energica.services.isServiceRunning
import be.hcpl.android.energica.services.obd2.Obd2Service

class BleEnergicaActivity : AppCompatActivity(R.layout.activity_ble) {

    private lateinit var prefs: SharedPreferences

    // toolbar state icons
    private lateinit var gpsStateView: ImageView
    private lateinit var bleStateView: ImageView
    private lateinit var obdStateView: ImageView

    private lateinit var connectView: TextView

    // have a single instance here
    private lateinit var connectionManager: ConnectionManager

    // region service connection

    private var serviceMessenger: Messenger? = null

    private val serviceConnection: ServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            serviceMessenger = Messenger(service)
            startService()
            setStateRunning()
        }

        override fun onServiceDisconnected(className: ComponentName) {
            stopService()
            serviceMessenger = null
        }
    }

    private fun startService() {
        connectionManager.isManuallyStopped = false // has no effect
        serviceMessenger?.send(Message.obtain(null, BleService.MANUALLY_STARTED, 0, 0))

        val intent = Intent(this@BleEnergicaActivity, BleService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
            bindService(intent, serviceConnection, BIND_AUTO_CREATE)
        } else {
            //startService(intent)
            ContextCompat.startForegroundService(this, intent)
        }
    }

    private fun stopService() {
        connectionManager.isManuallyStopped = true // has no effect
        serviceMessenger?.send(Message.obtain(null, BleService.MANUALLY_STOPPED, 0, 0))

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                unbindService(serviceConnection)
            } catch (e: Exception) { /* ignore */
            }
        }
        // stop background service for GPS location
        val intent = Intent(this@BleEnergicaActivity, BleService::class.java)
        stopService(intent)
        BleService.running = false
        setStateStopped()
    }

    private fun setStateRunning() {
        // allows for stopping service
        connectView.text = getString(R.string.stop)
        connectView.setOnClickListener { stopService() }
        updateServiceStates(false)
    }

    private fun setStateStopped() {
        // allows for starting service
        connectView.text = getString(R.string.start)
        connectView.setOnClickListener { startService() }
        updateServiceStates(false)
    }

    // endregion

    // region broadcast messages from service

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            when (intent.getStringExtra(Const.BROADCAST_EVENT)) {
                BROADCAST_BLE_STATE -> {
                    // update state on top of view and in toolbar
                    updateState(intent.getIntExtra(BLE_STATE, STATE_NOT_CONNECTED))
                    updateServiceStates()
                }

                BROADCAST_BLE_EVENT -> {
                    // changed to handling this in the ChargeSettings and OdometerFragment directly
                    // here we only need to update state (FIXED bug)
                }

                BROADCAST_BLE_GENERIC_EVENT -> {
                    // temp solution posting generic events like resetConnection and more
                    intent.getSerializableExtra(BLE_GENERIC_EVENT)?.let {
                        when (it) {
                            is ConnectionRejected -> connectionRejected()
                            is ChargeLimit -> {} // handled now in ChargeSettingsFragment directly, can be ignored here
                            is LocationData -> {} // handled on dashboard, ignore here
                            is NewState -> {
                                updateState(it.state)
                            }
                        }
                    }
                }
            }
        }
    }

    // endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // allow keeping app awake
        // TODO doesn't work with setShowWhenLocked, check alternative
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
        //    setShowWhenLocked(true)
        //} else {
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED)
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
        //}
        //setContentView(R.layout.activity_ble)

        prefs = getSharedPrefs(this)
        connectionManager = ConnectionManager.getInstance(applicationContext)

        // find all views
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        val titleView: TextView = findViewById(R.id.toolbar_title)
        val selectedDevice: TextView = findViewById(R.id.selected_device)
        val viewPager: ViewPager = findViewById(R.id.home_viewpager)
        statusView = findViewById(R.id.status_value)
        gpsStateView = findViewById(R.id.gps_state)
        bleStateView = findViewById(R.id.ble_state)
        obdStateView = findViewById(R.id.obd_state)
        connectView = findViewById(R.id.button_connect)

        // set up toolbar
        setSupportActionBar(toolbar)
        titleView.text = getString(R.string.ble_connection)

        // show configured device
        selectedDevice.text = prefs.getString(SETTINGS_DEVICE, "N/A")

        // some state indications
        // services are bound to the MainActivity, no way to toggle them properly here, we can however show current state
        connectView.setOnClickListener { startService(); }

        // fragment pager setup
        val adapter = ViewPagerAdapterMain(supportFragmentManager)
        viewPager.adapter = adapter

        // handle navigation here
        findViewById<View>(R.id.nav_dashboard).setOnClickListener { launchView(this, MainActivity::class.java) }
        findViewById<View>(R.id.nav_range).setOnClickListener { launchView(this, RangeActivity::class.java) }
        findViewById<View>(R.id.nav_ble).setOnClickListener { /* already on that screen */ }
        findViewById<View>(R.id.nav_obd).setOnClickListener { launchView(this, Obd2Activity::class.java) }
    }

    // region menu

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.close_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.close -> {
                finish()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    // endregion

    // region lifecycle handling

    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    override fun onResume() {
        super.onResume()
        // register broadcast receiver
        val filter = IntentFilter(Const.BROADCAST_FILTER)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            applicationContext.registerReceiver(broadcastReceiver, filter, RECEIVER_EXPORTED)
        } else {
            applicationContext.registerReceiver(broadcastReceiver, filter)
        }
        updateServiceStates()
    }

    override fun onPause() {
        applicationContext.unregisterReceiver(broadcastReceiver)
        super.onPause()
    }

    // endregion

    // region more connection handling

    fun connectionRejected() {
        log(applicationContext, "connection rejected by bike")
        val builder = AlertDialog.Builder(this, R.style.AlertDialogStyle)
        builder.setMessage(R.string.ConnRejected)
            .setCancelable(false)
        builder.create().show()
    }

    // endregion

    fun setChgTermination() {
        // only works because it's a singleton
        ConnectionManager.getInstance(applicationContext).setChargeTermination()
    }

    fun setResetTrip() {
        // only works because it's a singleton
        ConnectionManager.getInstance(applicationContext).setResetTrip()
    }

    // region show connection state

    private lateinit var statusView: TextView

    private fun updateServiceStates(updateActionView: Boolean = true) {
        setStateColorFor(gpsStateView, GpsService::class.java)
        setStateColorFor(bleStateView, BleService::class.java)
        setStateColorFor(obdStateView, Obd2Service::class.java)
        val bleServiceRunning = BleService::class.java.isServiceRunning(applicationContext)
        if (!updateActionView) return // optionally skip state update on action view
        if (bleServiceRunning) setStateRunning() else setStateStopped()
    }

    private fun updateState(state: Int) {
        // overrule when service state is not running
        if (!BleService::class.java.isServiceRunning(applicationContext)) {
            statusView.setText(R.string.NotRunningState)
            statusView.setTextColor(withColor(applicationContext, R.color.colorAccent))
        } else if (state == Const.STATE_SEARCHING) {
            statusView.setText(R.string.searchingState)
            statusView.setTextColor(withColor(applicationContext, R.color.colorPrimary))
        } else if (state == STATE_NOT_CONNECTED) {
            statusView.setText(R.string.NotConnectedState)
            statusView.setTextColor(withColor(applicationContext, R.color.colorAccent))
        } else if (state == Const.STATE_IDLE) {
            statusView.setText(R.string.IdleState)
            statusView.setTextColor(withColor(applicationContext, R.color.fluo_green))
        } else if (state == Const.STATE_CHARGE) {
            statusView.setText(R.string.ChargeState)
            statusView.setTextColor(withColor(applicationContext, R.color.fluo_green))
        } else if (state == Const.STATE_RUN) {
            statusView.setText(R.string.RunState)
            statusView.setTextColor(withColor(applicationContext, R.color.fluo_green))
        } else if (state == Const.STATE_ERROR) {
            statusView.setText(R.string.FaultState)
            statusView.setTextColor(withColor(applicationContext, R.color.colorAccent))
        }
    }

    // endregion

}