package be.hcpl.android.energica.ui.map;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import be.hcpl.android.energica.MainActivity;
import be.hcpl.android.energica.R;
import be.hcpl.android.energica.config.HybridBatteryPackCommand;
import be.hcpl.android.energica.model.data.BleData;
import be.hcpl.android.energica.model.data.LastLocation;
import be.hcpl.android.energica.model.data.LocationData;
import be.hcpl.android.energica.helpers.Const;
import be.hcpl.android.energica.helpers.ExportData;
import be.hcpl.android.energica.helpers.Help;
import be.hcpl.android.energica.model.ble.VehicleStatus;
import be.hcpl.android.energica.model.evmonitor.PushDataInput;
import be.hcpl.android.energica.model.ocm.ChargeLocation;
import be.hcpl.android.energica.services.chargemap.ChargeMapRepo;

import static android.content.Context.RECEIVER_EXPORTED;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static be.hcpl.android.energica.helpers.Const.BLE_EVENT;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_BLE_EVENT;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_LOCATION;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_DATA;
import static be.hcpl.android.energica.helpers.Const.BROADCAST_OBD2_GENERIC_DATA;
import static be.hcpl.android.energica.helpers.Const.DISTANCE_UNIT_KM;
import static be.hcpl.android.energica.helpers.Const.DISTANCE_UNIT_MI;
import static be.hcpl.android.energica.helpers.Const.KM_TO_MI;
import static be.hcpl.android.energica.helpers.Const.LOCATION_LAT;
import static be.hcpl.android.energica.helpers.Const.LOCATION_LON;
import static be.hcpl.android.energica.helpers.Const.LOCATION_PRECISION;
import static be.hcpl.android.energica.helpers.Const.LOCATION_SPEED;
import static be.hcpl.android.energica.helpers.Const.MPS_TO_KPH;
import static be.hcpl.android.energica.helpers.Const.OBD2_CELL_MAX;
import static be.hcpl.android.energica.helpers.Const.OBD2_CELL_MIN;
import static be.hcpl.android.energica.helpers.Const.OBD2_CURRENT;
import static be.hcpl.android.energica.helpers.Const.OBD2_NAME;
import static be.hcpl.android.energica.helpers.Const.OBD2_POWER;
import static be.hcpl.android.energica.helpers.Const.OBD2_RESULT;
import static be.hcpl.android.energica.helpers.Const.OBD2_SOC;
import static be.hcpl.android.energica.helpers.Const.OBD2_TEMP1;
import static be.hcpl.android.energica.helpers.Const.OBD2_TEMP2;
import static be.hcpl.android.energica.helpers.Const.OBD2_VOLT;
import static be.hcpl.android.energica.helpers.Help.doAfterConfirmation;

public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    public static final String PLACEHOLDER = "--";
    private final List<Location> mapPoints = new ArrayList<>(); // collects even when in background
    private final List<Polyline> plotted = new ArrayList<>(); // plotted route
    private final List<Marker> markers = new ArrayList<>(); // charge stops
    private final List<Marker> nearbyChargers = new ArrayList<>(); // chargers close to current location
    private final List<Long> chargeStopLengths = new ArrayList<>();
    private MainActivity context;
    private SharedPreferences prefs;
    private TextView consumptionView;
    private TextView socView;
    private TextView tempView;
    private TextView tripMeterView;
    private TextView rangeView;
    private TextView statsView;
    private Button logChargeView;
    private View bleDataView;

    // big numbers on top of map
    private View massiveNumbersView;
    private TextView massiveSpeedView, massiveSocView, massiveRangeView, massiveTempView;
    private TextView rangeUnitView, speedUnitView;

    private View obd2DataView;
    private TextView obd2SocView;
    private TextView obd2TempView;
    private TextView obd2VoltageView;
    private TextView obd2CurrentView;
    private TextView obd2PowerView;
    private TextView obd2BalanceView;

    private TextView reserveView;
    private TextView calculatedConsumptionView;
    private Location lastLocation;
    // service for receiving nearby chargers
    private ChargeMapRepo chargeMapRepo;
    private GoogleMap mapView;
    private Marker lastPositionMarker = null; // current position (end of route)

    private Marker startPosition = null; // initial position on map
    private boolean isReceivingGpsData = false;
    private Location lastLocationFetchedChargers = null;
    private VehicleStatus lastStatus;
    private boolean charging = false;
    private long lastChargeStopTimestamp = 0;
    private int topSpeed = 0;
    private int calculatedDistance = 0; // in meters based on gps data received
    private float initialOdoValue = 0;
    private int initialBatteryValue = 0;
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        public void onReceive(
                Context context,
                Intent intent) {
            String event = intent.getStringExtra(Const.BROADCAST_EVENT);
            if (BROADCAST_LOCATION.equals(event)) {
                double latitude = intent.getDoubleExtra(LOCATION_LAT, 0d);
                double longitude = intent.getDoubleExtra(LOCATION_LON, 0d);
                float speed = intent.getFloatExtra(LOCATION_SPEED, 0f);
                float precision = intent.getFloatExtra(LOCATION_PRECISION, 0f);
                Location location = new Location("gps"); // received location from background service
                location.setLatitude(latitude);
                location.setLongitude(longitude);
                location.setSpeed(speed);
                location.setAccuracy(precision);
                onLocationChanged(new LocationData(location));
            } else if (BROADCAST_BLE_EVENT.equals(event)) {
                // receive BLE updates
                final BleData data = (BleData) intent.getSerializableExtra(BLE_EVENT);
                if (data != null) onBleDataEvent(data);
            } else if (BROADCAST_OBD2_DATA.equals(event)) {
                int soc = intent.getIntExtra(OBD2_SOC, 0);
                int temp1 = intent.getIntExtra(OBD2_TEMP1, 0);
                int temp2 = intent.getIntExtra(OBD2_TEMP2, 0);
                double voltage = intent.getDoubleExtra(OBD2_VOLT, 0.0);
                double current = intent.getDoubleExtra(OBD2_CURRENT, 0.0);
                double power = intent.getDoubleExtra(OBD2_POWER, 0.0);
                int cellMin = intent.getIntExtra(OBD2_CELL_MIN, 0);
                int cellMax = intent.getIntExtra(OBD2_CELL_MAX, 0);
                updateObdData(soc, temp1, temp2, voltage, current, power, cellMin, cellMax);
            } else if (BROADCAST_OBD2_GENERIC_DATA.equals(event)) {
                // also check for name so we can display SOC from generic OBD data
                String name = intent.getStringExtra(OBD2_NAME);
                String value = intent.getStringExtra(OBD2_RESULT);
                if (HybridBatteryPackCommand.NAME.equals(name)) {
                    updateObdData(value);
                }
            }
        }
    };

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        metricUnitValue = getString(R.string.value_metric);
        imperialUnitValue = getString(R.string.value_imperial);
    }

    private String metricUnitValue = "metric", imperialUnitValue = "imperial";

    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    @Nullable
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {

        context = (MainActivity) getActivity();
        chargeMapRepo = new ChargeMapRepo(context);
        prefs = Help.getSharedPrefs(context);

        // listen for broadcast updates
        IntentFilter filter = new IntentFilter(Const.BROADCAST_FILTER);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            context.registerReceiver(broadcastReceiver, filter, RECEIVER_EXPORTED);
            // RECEIVER_EXPORTED is needed for gps service to publish data to here, not working with RECEIVER_NOT_EXPORTED
        } else {
            context.registerReceiver(broadcastReceiver, filter);
        }

        final View rootView = inflater.inflate(R.layout.fragment_main_dashboard, container, false);
        socView = rootView.findViewById(R.id.soc_label);
        tempView = rootView.findViewById(R.id.temp_label);
        rangeView = rootView.findViewById(R.id.range_label);
        tripMeterView = rootView.findViewById(R.id.tripmeter_label);
        consumptionView = rootView.findViewById(R.id.consumption_label);
        statsView = rootView.findViewById(R.id.stats);
        reserveView = rootView.findViewById(R.id.reserve_label);
        calculatedConsumptionView = rootView.findViewById(R.id.calculated_consumption);
        bleDataView = rootView.findViewById(R.id.ble_data);

        massiveNumbersView = rootView.findViewById(R.id.massive_numbers);
        massiveSpeedView = rootView.findViewById(R.id.massive_speed);
        massiveSocView = rootView.findViewById(R.id.massive_soc);
        massiveRangeView = rootView.findViewById(R.id.massive_range);
        massiveTempView = rootView.findViewById(R.id.massive_temp);
        rangeUnitView = rootView.findViewById(R.id.range_unit);
        speedUnitView = rootView.findViewById(R.id.speed_unit);

        obd2DataView = rootView.findViewById(R.id.obd_data);
        obd2SocView = rootView.findViewById(R.id.obd_soc_value);
        obd2TempView = rootView.findViewById(R.id.obd_temp_value);
        obd2VoltageView = rootView.findViewById(R.id.obd_voltage);
        obd2CurrentView = rootView.findViewById(R.id.obd_current);
        obd2PowerView = rootView.findViewById(R.id.obd_power);
        obd2BalanceView = rootView.findViewById(R.id.obd_balance_value);

        // some more features added
        rootView.findViewById(R.id.export_gpx).setOnClickListener(v ->
                doAfterConfirmation(context, R.string.confirm_export_gpx_route,
                        (d, w) -> {
                            exportRouteToGpx();
                            Toast.makeText(context, "data GPX export done", Toast.LENGTH_SHORT).show();
                        }
                ));
        // allow user to manually start and stop charge logs
        logChargeView = rootView.findViewById(R.id.log_charge);
        logChargeView.setOnClickListener(v ->
                doAfterConfirmation(context, R.string.confirm_add_waypoint,
                        (d, w) -> handleManualChargeStop()
                ));
        rootView.findViewById(R.id.reset_trip).setOnClickListener(v ->
                doAfterConfirmation(context, R.string.confirm_clear_current_route_export_first,
                        (dialog, whichButton) -> {
                            // context.getParser().setResetTrip();
                            resetGpsData();
                            resetStats();
                            log("route removed from map");
                        }
                ));

        // enables google maps
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) mapFragment.getMapAsync(this);

        return rootView;
    }

    // endregion

    @Override
    public void onDestroy() {
        context.unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();

        // update from prefs
        isReceivingGpsData = prefs.getBoolean(getString(R.string.key_use_device_gps), false);
        // enable or disable massive speed view visibility based on user prefs
        massiveNumbersView.setVisibility(prefs.getBoolean(getString(R.string.key_show_massive_speed), false) ? VISIBLE : GONE);

        clearRouteFromMap();
        plotCompleteRoute();
        updateStats();
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mapView = googleMap;
        plotCompleteRoute();
        mapView.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(@NonNull @NotNull final Marker marker) {
        // don't provide navigation to current position marker
        if (lastPositionMarker.equals(marker)) {
            return false;
        }
        new AlertDialog.Builder(requireContext(), R.style.AlertDialogStyle)
                .setMessage("Navigate to this place? \"" + marker.getTitle() + "\"")
                .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                    log("triggered navigation to selected marker");
                    double latitude = marker.getPosition().latitude;
                    double longitude = marker.getPosition().longitude;

                    String uri = "geo:" + latitude + "," + longitude + "?q=" + latitude + "," + longitude;
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
                })
                .setNegativeButton(android.R.string.no, null).show();
        return true;
    }

    public void resetGpsData() {
        clearRouteFromMap();
        mapPoints.clear();
        plotted.clear();
        markers.clear();
        nearbyChargers.clear();
    }

    private Location toLocation(PushDataInput input) {
        if (input == null || input.getLongitude() == null || input.getLatitude() == null) {
            // we need at least a validation location for this to work
            return null;
        }
        final Location location = new Location("gps-cache");
        location.setLongitude(input.getLongitude());
        location.setLatitude(input.getLatitude());
        location.setTime(input.getTimestamp());
        if (input.getGpsAcc() != null) location.setAccuracy(input.getGpsAcc());
        if (input.getSpeed() != null) location.setSpeed(input.getSpeed());
        return location;
    }

    public void loadDataFromCache() {
        log("MAP - started plotting route from cache file");
        final PolylineOptions polyLineOptions = new PolylineOptions();
        //final Location lastPoint = new Location("gps", new Location());
        // do in background
        final Thread work = new Thread(() -> {
            log("MAP - plotting background thread created");
            // retrieve all map data from cache
            final List<PushDataInput> data = ExportData.Companion.retrieveCachedData(context);
            log("MAP - data size is: "+data.size());
            // create line at once to plot from this data
            final Location lastPoint;
            for (PushDataInput input : data) {
                if (input != null && input.getLatitude() != null && input.getLongitude() != null) {
                    polyLineOptions.add(new LatLng(input.getLatitude(), input.getLongitude()));
                }
            }
            // set last data point here
            if (!data.isEmpty()) {
                lastPoint = toLocation(data.get(data.size() - 1));
            } else {
                lastPoint = null;
            }
            log("MAP - done plotting, back to UI thread");
            // when done run on UI thread
            context.runOnUiThread(() -> {
                Polyline polyline = mapView.addPolyline(polyLineOptions);
                // Store a data object with the polyline, used here to indicate an arbitrary type.
                polyline.setWidth(getResources().getDimension(R.dimen.map_stroke_width));
                polyline.setColor(ResourcesCompat.getColor(getResources(), R.color.map_stroke_color, null));
                polyline.setJointType(JointType.ROUND);
                // keep reference so we can remove lines from map when needed
                plotted.add(polyline);
                // change focus on map here
                if (lastPoint != null) {
                    mapView.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastPoint.getLatitude(), lastPoint.getLongitude()), 10));
                }
            });

        });
        try {
            work.start();
        } catch (Exception e) {
            log("Error importing route from cache: " + e.getMessage());
        }
    }

    public void onLocationChanged(LocationData data) {
        if (!isReceivingGpsData && "gps".equals(data.getLocation().getProvider())) {
            // auto correct for receiving GPS data
            isReceivingGpsData = true;
        }
        if (isReceivingGpsData && !"gps".equals(data.getLocation().getProvider())) {
            // filter on gps data if gps data is enabled
            return;
        }
        if (lastLocation != null) {
            // update (unit is meters) not very accurate, prefer ODO values instead if available
            calculatedDistance += lastLocation.distanceTo(data.getLocation());
        }
        lastLocation = data.getLocation();
        updateChargers();
        updateStats();
        mapPoints.add(data.getLocation());
        if (isAdded()) {
            // when visible also update map
            addLastPointToRoute();
            // easy fix is to keep track of points added or not is to just rebuild the complete route on resume
        }
    }

    private void updateChargers() {
        // no need to without location
        if (lastLocation == null) {
            return;
        }
        // don't check when feature is disabled
        if (!prefs.getBoolean(getString(R.string.key_fetch_chargers), true)) {
            return;
        }
        // based on new lastLocation first check if we have covered another (configurable distance)
        int distanceBetween = Integer.parseInt(prefs.getString(getString(R.string.key_distance_between_chargers), "5")) * 1_000;
        if (lastLocationFetchedChargers == null || lastLocationFetchedChargers.distanceTo(lastLocation) > distanceBetween) {

            // go fetch new nearby chargers
            chargeMapRepo.chargeLocations(
                    lastLocation.getLatitude(),
                    lastLocation.getLongitude(),
                    () -> {
                        int searchRadiusInKm = Integer.parseInt(prefs.getString(getString(R.string.key_radius_chargers), "20"));
                        int numberOfResults = Integer.parseInt(prefs.getString(getString(R.string.key_max_number_chargers), "10"));
                        String typeOfChargers = prefs.getString(getString(R.string.key_charger_types), "33,32");
                        String distanceUnit = "KM";
                        if (imperialUnitValue.equals(prefs.getString(getString(R.string.key_unit_system), metricUnitValue)))
                            distanceUnit = "Miles";
                        return new ChargeMapRepo.ChargeConfig(
                                searchRadiusInKm,
                                numberOfResults,
                                typeOfChargers,
                                distanceUnit
                        );
                    },
                    (chargers) -> {
                        log("received nearby chargers for current location");
                        handleNewNearbyChargers(chargers);
                        return null;
                    },
                    () -> {
                        log("failed to get nearby chargers from api");
                        return null;
                    },
                    () -> {
                        // fetching chargers was queued already so this request was ignored
                        return null;
                    });
        }
    }

    private void handleNewNearbyChargers(List<ChargeLocation> chargeLocations) {
        lastLocationFetchedChargers = lastLocation; // mark that we just fetched chargers
        // remove any existing chargers (if present)
        for (Marker chargerMarker : nearbyChargers) {
            chargerMarker.remove();
        }
        nearbyChargers.clear();
        if (mapView == null) {
            return;
        }
        // and plot the new ones on the map instead
        for (ChargeLocation charger : chargeLocations) {
            LatLng position = new LatLng(charger.getAddressInfo().getLatitude(), charger.getAddressInfo().getLongitude());
            String title = charger.getAddressInfo().getTitle();
            nearbyChargers.add(
                    mapView.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                            .position(position)
                            .title(title)));
        }
    }

    // endregion

    // region route plotting on map

    private void log(String message) {
        ExportData.Companion.log(context, message);
    }

    private void clearRouteFromMap() {
        // doesn't remove any collected data, only visually removed from map
        for (Polyline line : plotted) {
            line.remove(); // complete route plotted on map
        }
        for (Marker marker : markers) {
            marker.remove(); // includes start and end position
        }
        for (Marker marker : nearbyChargers) {
            marker.remove(); // removes all chargers on the map
        }
    }

    private void exportRouteToGpx() {
        String fileName = "exported-route-" + ExportData.Companion.getTimeFormatLogStatement().format(System.currentTimeMillis()) + ".gpx";
        StringBuilder segments = new StringBuilder("<trk><name>" + fileName + "</name>");
        segments.append("<trkseg>\n");
        for (Location location : mapPoints) {
            segments.append("<trkpt lat=\"").append(location.getLatitude()).append("\" lon=\"").append(location.getLongitude()).append(
                    "\"><time>").append(ExportData.Companion.getDateFormatGpx().format(
                    new Date(location.getTime()))).append("</time></trkpt>\n");
        }
        segments.append("</trkseg></trk>"); // these manually created export files are properly closed
        // also append all chargestops as waypoints
        StringBuilder waypoints = new StringBuilder();
        for (Marker marker : markers) {
            waypoints.append("<wpt lat=\"").append(marker.getPosition().latitude).append("\" lon=\"").append(marker.getPosition().longitude).append(
                    "\">").append(
                    // should we store start and end time for chargestops also? Not really the goal of this app anyway
                    //"<time>" + gpxDateFormat.format(new Date(marker.getPosition().location.getTime())) + "</time>" +
                    "<name>").append(marker.getTitle()).append("</name></wpt>");
        }
        String footer = "</gpx>"; // these manually created export files are properly closed
        ExportData.Companion.writeToNewFile(fileName, ExportData.GPX_HEADER + segments + waypoints + footer, requireContext());
    }

    private void plotCompleteRoute() {
        // used to recover from app being in background while service was running, recreate the route from collected data
        if (mapView == null) {
            return;
        }

        // add all markers again to map
        List<Marker> newMarkers = new ArrayList<>();
        for (Marker marker : markers) {
            // also need to keep track of the lastPositionMarker so it can be updated once we continue plotting
            lastPositionMarker = mapView.addMarker(new MarkerOptions().position(marker.getPosition()).title(marker.getTitle()));
            newMarkers.add(lastPositionMarker);
        }
        // need to reset the references to these markers as they are now added again
        markers.clear();
        markers.addAll(newMarkers);

        // add all nearby chargers to map
        List<Marker> newChargerMarkers = new ArrayList<>();
        for (Marker charger : nearbyChargers) {
            newChargerMarkers.add(mapView.addMarker(
                    new MarkerOptions()
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                            .position(charger.getPosition())
                            .title(charger.getTitle())));
        }
        nearbyChargers.clear();
        nearbyChargers.addAll(newChargerMarkers);

        // lines we can just clear as they are build from Locations
        plotted.clear();
        // renders a line on map for all points retrieved so far
        PolylineOptions polyLineOptions = new PolylineOptions();//.clickable(true);
        for (Location location : mapPoints) {
            if (location.getLatitude() != 0 && location.getLongitude() != 0) {
                polyLineOptions.add(new LatLng(location.getLatitude(), location.getLongitude()));
            }
        }
        Polyline polyline = mapView.addPolyline(polyLineOptions);
        // Store a data object with the polyline, used here to indicate an arbitrary type.
        //polyline.tag = date // uses date as a tag on the lines
        polyline.setWidth(getResources().getDimension(R.dimen.map_stroke_width));
        polyline.setColor(ResourcesCompat.getColor(getResources(), R.color.map_stroke_color, null));
        polyline.setJointType(JointType.ROUND);
        // keep reference so we can remove lines from map when needed
        plotted.add(polyline);
        // move to last position
        centerMapUpdatingLastPoint(false); // last position is already there as a marker so don't add it again
    }

    private void addLastPointToRoute() {

        // plot start position if not already done
        if (startPosition == null && !mapPoints.isEmpty()) {
            Location location = mapPoints.get(0);
            initialMapLocation(new LastLocation(location.getLatitude(), location.getLongitude()));
            return;
        }

        // TODO documentation needed on the POI colors used
        // red = start and end point markers
        // green = charge stops
        // blue = chargers nearby

        // draw line when enough points collected
        if (mapPoints.size() >= 2) {
            Location lastPoint = mapPoints.get(mapPoints.size() - 1);
            Location previousPoint = mapPoints.get(mapPoints.size() - 2);
            PolylineOptions polyLineOptions = new PolylineOptions();
            polyLineOptions.add(new LatLng(previousPoint.getLatitude(), previousPoint.getLongitude()));
            polyLineOptions.add(new LatLng(lastPoint.getLatitude(), lastPoint.getLongitude()));
            Polyline polyline = mapView.addPolyline(polyLineOptions);
            // Store a data object with the polyline, used here to indicate an arbitrary type.
            polyline.setWidth(getResources().getDimension(R.dimen.map_stroke_width));
            polyline.setColor(ResourcesCompat.getColor(getResources(), R.color.map_stroke_color, null));
            polyline.setJointType(JointType.ROUND);
            // keep reference so we can remove lines from map when needed
            plotted.add(polyline);
        }
        centerMapUpdatingLastPoint(true); // also updates last position marker
    }

    public void initialMapLocation(LastLocation event) {
        if (startPosition != null || mapView == null) {
            return; // ignore if we already have a start location plotted on map or if map isn't ready
        }
        // mark start location on map and change zoom level
        LatLng location = new LatLng(event.getLatitude(), event.getLongitude());
        startPosition = mapView.addMarker(new MarkerOptions().position(location).title("Start"));
        mapView.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 10));
        markers.add(startPosition);
    }

    private void centerMapUpdatingLastPoint(boolean addLastPosition) {
        // center map on last location (no change in zoom level)
        if (!mapPoints.isEmpty()) {
            Location location = mapPoints.get(mapPoints.size() - 1);
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            if (addLastPosition) {
                if (lastPositionMarker != null) {
                    lastPositionMarker.remove();
                    markers.remove(lastPositionMarker);
                }
                lastPositionMarker = mapView.addMarker(new MarkerOptions().position(latLng).title("You"));
                markers.add(lastPositionMarker);
            }
            mapView.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }

    private void handleManualChargeStop() {
        // toggles charging state manually for users that don't have a BLE connection
        if (charging) {
            stopCharging();
        } else {
            startCharging();
        }
    }

    // endregion

    // region charge stop handling

    private void startCharging() {
        charging = true;
        logChargeView.setText(getString(R.string.stop_charge));
        showChargeStopOnMap();
        lastChargeStopTimestamp = System.currentTimeMillis();
        log("START charging, total charge sessions " + chargeStopLengths.size());
        Toast.makeText(context, "Charge Session STARTED", Toast.LENGTH_SHORT).show();
    }

    private void stopCharging() {
        charging = false;
        logChargeView.setText(getString(R.string.start_charge));
        long totalChargingTimeInMs = System.currentTimeMillis() - lastChargeStopTimestamp;
        chargeStopLengths.add(totalChargingTimeInMs);
        log("STOP charging session of " + (totalChargingTimeInMs / 1000 / 60) + " min, total charge sessions " + chargeStopLengths.size());
        Toast.makeText(context, "Charge Session STOPPED", Toast.LENGTH_SHORT).show();
        // triggers update of stats on data screen
        updateStats();
    }

    private void showChargeStopOnMap() {
        // just a single charge stop marker on the map using current location
        if (lastLocation != null) {
            LatLng latLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
            Marker marker = mapView.addMarker(
                    new MarkerOptions()
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                            .position(latLng)
                            .title("Charging from " + (lastStatus != null ? lastStatus.soc : "?") + "%"));
            markers.add(marker);
        }
    }

    // endregion

    // region collecting stats

    private void updateObdData(int soc, int temp1, int temp2, double voltage, double current, double power, int cellMin, int cellMax) {
        // also used for pushing cell balance data and then soc and temp are empty, skip
        if (soc != 0) obd2SocView.setText(getString(R.string.battery_soc_value).replace(PLACEHOLDER, String.valueOf(soc)));
        if (temp1 != 0 && temp2 != 0) obd2TempView.setText(getString(R.string.battery_temp_value).replace(PLACEHOLDER, temp1 + "/" + temp2));
        if (voltage != 0) obd2VoltageView.setText(getString(R.string.battery_voltage_obd2_value).replace(PLACEHOLDER, String.valueOf(voltage)));
        if (current != 0) obd2CurrentView.setText(getString(R.string.battery_current_obd2_value).replace(PLACEHOLDER, String.valueOf(current)));
        if (power != 0) obd2PowerView.setText(getString(R.string.battery_power_obd2_value).replace(PLACEHOLDER, String.format("%.1f", power)));
        if (cellMin != 0 && cellMax != 0) obd2BalanceView.setText(getString(R.string.battery_balance_obd2_value).replace(PLACEHOLDER,
                cellMin + "/" + cellMax));
    }

    private void updateObdData(final String soc) {
        // for generic OBDII connection we only support SOC values for now?
        obd2SocView.setText(getString(R.string.battery_soc_value).replace("--", soc));
    }

    public void onBleDataEvent(BleData event) {
        lastStatus = event.getStatus();
        updateStats();
        updateValues(event.getSelectedUnit(), event.getStatus());
    }

    private void resetStats() {
        topSpeed = 0;
        calculatedDistance = 0;
        chargeStopLengths.clear();
        lastChargeStopTimestamp = 0;
        charging = false;
        logChargeView.setText(getString(R.string.start_charge));
        initialBatteryValue = 0;
    }

    private void updateStats() {

        // OBD2 details on map
        boolean showObd2Data = prefs.getBoolean(getString(R.string.key_obd2_show_on_map), true);
        obd2DataView.setVisibility(showObd2Data ? VISIBLE : GONE);

        // BLE details on map
        boolean showBleData = prefs.getBoolean(getString(R.string.key_show_ble_data_map), true);
        bleDataView.setVisibility(showBleData ? VISIBLE : GONE);

        // bike specific details
        //int lastBikeSpeed = lastStatus != null ? lastStatus.speed : 0;
        //int currentUnitBike = lastStatus != null ? lastStatus.distanceUnit : DISTANCE_UNIT_KM;
        //String unitValueBike = currentUnitBike == DISTANCE_UNIT_MI ? "mi" : "km";

        // calc consumption from BLE
        boolean showCalculatedConsumption = prefs.getBoolean(getString(R.string.key_show_calculated_consumption_map), true);
        if (showCalculatedConsumption)
            handleCalculatedConsumption();
        else
            hideCalculatedConsumption();

        // GPS details on top of map
        int lastGpsSpeed = lastLocation != null ? Math.round(lastLocation.getSpeed() * MPS_TO_KPH) : 0;
        String valueMetric = getString(R.string.value_metric);
        int unitSystem = valueMetric.equals(prefs.getString(getString(R.string.key_unit_system), valueMetric)) ? DISTANCE_UNIT_KM : DISTANCE_UNIT_MI;
        String unitValueSystem = unitSystem == DISTANCE_UNIT_MI ? "mi" : "km";
        int averageSpeed = Math.round(calculateAverageSpeed() * MPS_TO_KPH);
        float totalDistance = (float) (calculatedDistance / 100) / 10; // total distance in km
        // register top speed
        if (lastGpsSpeed > topSpeed) {
            topSpeed = lastGpsSpeed;
        }
        if (unitSystem == DISTANCE_UNIT_MI) {
            // correct speeds for unit
            lastGpsSpeed *= KM_TO_MI;
            averageSpeed *= KM_TO_MI;
            topSpeed *= KM_TO_MI;
            totalDistance *= KM_TO_MI;
        }

        // GPS details
        boolean showGpsDetails = prefs.getBoolean(getString(R.string.key_show_gps_details), true);
        if (showGpsDetails) {
            statsView.setText("Speed: " + lastGpsSpeed
                    + ", Avg: " + averageSpeed
                    + ", Dist:" + totalDistance
                    + " (" + unitValueSystem + ")");
            statsView.setVisibility(VISIBLE);
        } else {
            statsView.setVisibility(View.GONE);
        }
        if (massiveSpeedView != null) {
            massiveSpeedView.setText(String.valueOf(lastGpsSpeed));
            if (unitSystem == DISTANCE_UNIT_MI) {
                speedUnitView.setText(getString(R.string.unit_imperial_speed));
            } else {
                speedUnitView.setText(getString(R.string.unit_metric_speed));
            }
        }
    }

    private void hideCalculatedConsumption() {
        calculatedConsumptionView.setVisibility(GONE);
    }

    private void handleCalculatedConsumption() {
        // bike specific details
        int currentUnitBike = lastStatus != null ? lastStatus.distanceUnit : DISTANCE_UNIT_KM;
        String unitValueBike = currentUnitBike == DISTANCE_UNIT_MI ? "mi" : "km";

        // average consumption from ODO and resBatteryEnergy values
        float averageConsumption = 0, coveredDistance = 0, consumedKwh = 0, rangeLeft = 0;
        if (lastStatus != null) {
            final float lastOdoValue = lastStatus.totalOdometer;
            final int lastBatteryValue = lastStatus.resBatteryEnergy;
            if (initialOdoValue == 0) initialOdoValue = lastOdoValue;
            if (initialBatteryValue == 0) initialBatteryValue = lastBatteryValue;
            // calculate how much kWh were used over what distance
            coveredDistance = lastOdoValue - initialOdoValue;
            consumedKwh = initialBatteryValue - lastBatteryValue;
            if (consumedKwh != 0 && coveredDistance != 0) {
                averageConsumption = consumedKwh / coveredDistance;
            }
            if (averageConsumption != 0 && lastBatteryValue != 0) {
                rangeLeft = lastBatteryValue / averageConsumption;
            }
        }
        // display these values
        if (averageConsumption != 0) {
            calculatedConsumptionView.setText(getString(R.string.calculated_consumption, coveredDistance, unitValueBike, consumedKwh,
                    averageConsumption, unitValueBike, rangeLeft, unitValueBike));
            calculatedConsumptionView.setVisibility(VISIBLE);
        } else {
            hideCalculatedConsumption();
        }
    }

    private int calculateAverageSpeed() {
        // alternative option is to calculate average speed with a moving average
        // new_average = (n * old_average - x_forget + x_new) / n
        // Here, n is the number of data points, x_forget is the value you are "forgetting" and x_new is the latest value
        int totalSpeed = 0;
        int dataPoints = 0;
        for (Location location : mapPoints) {
            if (location.hasSpeed() && "gps".equals(location.getProvider())) {
                // only takes values from gps provider into account, values from bike are ignored for speed calculations
                totalSpeed += location.getSpeed();
                dataPoints++;
            }
        }
        return totalSpeed > 0 && dataPoints > 0 ? totalSpeed / dataPoints : 0;
    }

    private void updateValues(
            final int selectedUnit,
            final VehicleStatus currentStatus) {

        if (!isAdded()) {
            return;
        }

        // uses metric system from the bike itself
        reserveView.setText(getString(R.string.battery_reserve_value).replace(PLACEHOLDER,
                String.format(Locale.getDefault(), "%,d", currentStatus.resBatteryEnergy)));
        socView.setText(getString(R.string.battery_soc_value).replace(PLACEHOLDER,
                String.format(Locale.getDefault(), "%d", currentStatus.soc)));
        tempView.setText(getString(R.string.battery_temp_value).replace(PLACEHOLDER,
                String.format(Locale.getDefault(), "%d", currentStatus.temp)));
        // some require a specific unit
        consumptionView.setText(getString(R.string.avg_consumption_value_no_unit).replace(PLACEHOLDER,
                String.format(Locale.getDefault(), "%.1f %s", currentStatus.avgConsumption,
                        Help.getConsumptionUnit(currentStatus.avgConsumptionUnit))));
        if (selectedUnit == DISTANCE_UNIT_MI) {
            rangeView.setText(getString(R.string.remaining_range_value_no_unit).replace(PLACEHOLDER,
                    String.format(Locale.getDefault(), "%d %s",
                            Math.round(((double) currentStatus.range) * KM_TO_MI), getString(R.string.unit_imperial_distance))));
            tripMeterView.setText(getString(R.string.tripmeter_value_no_unit).replace(PLACEHOLDER,
                    String.format(Locale.getDefault(), "%d %s",
                            Math.round(((double) currentStatus.tripMeter) * KM_TO_MI), getString(R.string.unit_imperial_distance))));
        } else {
            rangeView.setText(getString(R.string.remaining_range_value_no_unit).replace(PLACEHOLDER,
                    String.format(Locale.getDefault(), "%d %s", currentStatus.range, getString(R.string.unit_metric_distance))));
            tripMeterView.setText(getString(R.string.tripmeter_value_no_unit).replace(PLACEHOLDER,
                    String.format(Locale.getDefault(), "%.1f %s", currentStatus.tripMeter, getString(R.string.unit_metric_distance))));
        }

        // big numbers on top of map
        massiveSocView.setText(String.format(Locale.getDefault(), "%d", currentStatus.soc));
        massiveTempView.setText(String.format(Locale.getDefault(), "%d", currentStatus.temp));
        // range comes in different units
        if (selectedUnit == DISTANCE_UNIT_MI) {
            massiveRangeView.setText(String.format(Locale.getDefault(), "%d", Math.round((double) currentStatus.range * KM_TO_MI)));
            rangeUnitView.setText(getString(R.string.unit_imperial_distance));
        } else {
            massiveRangeView.setText(String.format(Locale.getDefault(), "%d", currentStatus.range));
            rangeUnitView.setText(getString(R.string.unit_metric_distance));
        }
    }

    // endregion

}
