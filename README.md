# Energica BLE App (Android) #

## What is this repository for?

This is a an open source app that connects to ENERGICA ELECTRIC MOTORCYCLES over Bluetooth 
low energy (BLE) or OBD interface to collect and display information received from the bike.

For non Energica motorcycles we also added a more generic OBD2 implementation and tools like 
the range calculator. For Zero Gen 3 support there is a separate app consuming their cloud API 
(check links below).

_Note that iOS apps were removed since this requires me to pay a yearly fee of 99 EUR._ 

Android Play Store link https://play.google.com/store/apps/details?id=be.hcpl.android.energica

~~iOS App Store link https://apps.apple.com/app/id1559468042~~

Zero specific Android App https://play.google.com/store/apps/details?id=be.hcpl.android.zengo

~~Zero specific iOS App https://apps.apple.com/us/app/zerong/id1488172044~~

![App screenshot 1](https://bitbucket.org/cappelleh/emapp-android/raw/52cfc37c7bb9fcf597fd0213f7b5e2b081c7f6b4/release/Screenshot_1635011310.png "App Screenshot 1")
![App screenshot 2](https://bitbucket.org/cappelleh/emapp-android/raw/d2c42c9097e54211a81d38481790cd2b1498e5ab/release/Screenshot_1635011312.png "App Screenshot 2")
![App screenshot 3](https://bitbucket.org/cappelleh/emapp-android/raw/d2c42c9097e54211a81d38481790cd2b1498e5ab/release/Screenshot_1635011324.png "App Screenshot 3")
![App screenshot 4](https://bitbucket.org/cappelleh/emapp-android/raw/d2c42c9097e54211a81d38481790cd2b1498e5ab/release/Screenshot_1635011340.png "App Screenshot 4")

## Open Test Track ##

Everyone can test early updates after subscribing to this open test track

On Android: https://play.google.com/store/apps/details?id=be.hcpl.android.energica
Web version: https://play.google.com/apps/testing/be.hcpl.android.energica

## Roots ##

Energica BLE connection is based on Official MyEnergica Android App version 1.9.5. This app doesn't
seem to evolve any longer, recent bikes don't even ship with the BLE connection anymore. 

Generic OBDII connection is based on an opensource Android app with the name 
[Android OBD reader](https://app.circleci.com/pipelines/github/pires/android-obd-reader).

DISCLAIMER: USE AT YOUR OWN RISK. Shouldn't harm the bike since we are just submitting a few BLE 
info requests and the known horn, trip reset and stop charge commands. For OBDII commands we query 
a few ones that are safe but this is on the databus of the bike itself so too much traffic can 
cause issues.

## How do I get set up?

### Installation 

Check play and app store links on top or check downloads on this repo or the build pipelines in case
you don't like the Google Play Store. You can also look in the release folders for debug apk builds.
Note that these aren't signed so require some development options checked on your device, see next 
paragraph for details on that.

For non play store (debug) builds you'll have to allow installation of apps from unknown sources on
your device. See [these instructions](https://www.androidauthority.com/how-to-install-apks-31494/) 
for more details. Note that this setting has moved a lot between Android versions and manufacturers 
also change it. So not always straight forward. If you can't find it it's best to just download it 
from the app store and wait for an update there with the latest features.

### Connecting w/ Energica

If you have the official MyEnergica app on your phone installed already and set up this app should 
connect right away. On modern Android versions the app will request permission to run Bluetooth 
(that is the location request). 
 
If the connection doesn't work make sure the MyEnergica App is currently closed, look at the 
running apps on your device and kill it. Same goes the other way around, if you want to connect 
with the MyEnergica App you should make sure this app is closed. 

If you haven't had the phone connected to the bike yet or another phone was connected prior to this 
install make sure to remove the Bluetooth MAC address registered on your bike. The Energica 
motorcycles only allow connection to a single Device. This can be reset from the menu on the 
dashboard. 
  
If the app or your device was used with another motorcycle before you'll also have to use the 
reset option from the settings screen of this app.

If you still don't get a connection try to restart the app (no need to reboot the phone) in 
combination with switching the bike OFF and ON again. That sometimes helps since both will try to 
connect on startup. Plus the bike can only be found using BLE scanning right after the startup.

There is no need to perform so called bluetooth device pairing. So if you're prompted for a 
bluetooth pin you're attempting to pair the two. That is not how this connection works (it does 
for Zero but that's another story).

### Connecting w/ Generic OBD2

For this to work you need to get a Bluetooth 4 or BLE enabled OBD2 dongle. Wifi dongles are not 
supported ATM. Most of the cheap dongles available won't work with the specific Energica commands 
used here because they are cheap and bad copies that only support some common OBDII commands. 
The one that does work can be found in Europe on amazon by searching for Power Cruise Control.

For Android you need to go into the Bluetooth System settings first and scan for available devices 
and then pair the Bluetooth dongle. Some will require a PIN for pairing. That is typically 0000 
or 1234. Then in the app you'll find the dongle listed in the paired devices drop down on top of 
the 1235. OBD views.

For iOS you should NOT pair the Bluetooth dongle. Instead go directly into the app and select the 
OBD2 device. For iOS these dongles are discovered as a BLE device advertising some specific 
characteristics that this app will make use of.

### Troubleshooting

#### Energica BLE connection issues

You can only have one physical device connected to your Energica motorcycle so either do this on 
the phone already connected (using MyEnergica app) or reset the connection on the bike first. 

You can have several apps installed on the device that can connect to this BLE interface but only 
one at a time. So if you currently have the MyEnergica app running in the background that one will 
connect and prevent this app to connect. Also true the other way around if you want to use the 
MyEnergica app. Just kill the other app from the running apps first.
  
Check the state description on the first view below the motorcycle to see if you're currently in 
Connected (state of bike showing) or Searching. Searching shouldn't take much longer than 30 
seconds. If it takes longer go closer to your bike or check the other issues. 
 
If connection doesn't work it helps to kill and restart the app and to toggle the ignition key 
on the bike.

There is something in place that looks like an auto reconnect feature to prevent the connection 
to die when your phone is no longer active. I need to look into that feature though cause it 
doesn't look like it will work ATM. Fix coming soon.

#### (Generic) OBD2 connection issues

There are several OBD bluetooth adapters available. Most of those are cheap chines copies with
only limited functionality. If you want the fully working connection you'll have to pay more than
10 EUR for an adapter. A known working one is available from amazon at: https://amzn.eu/d/6t5TqkT

Also some use BLE, others require classic Bluetooth connection. Best to check for documentation 
specific to the adapter you choose.

#### GPS connection issues

Double check your phone has location services enabled and the app has location permissions. Here
are some more tips to troubleshoot GPS connection:

https://www.imobie.com/android-system-recovery/fix-gps-not-working-on-android.htm

## Pushing data to the cloud

### Using EV-monitor.com services

Since version 2.5 of this app you can enable pushing data to a remote service provided by 
[https://ev-monitor.com](https://ev-monitor.com). They provide a dashboard to visualize the data 
you push. That project is being developed right now so expect the dashboard to be extended soon.

By default nothing is shared, you'll have to register for an account with ev-monitor.com first. 
Once you have that activated you can go into the settings within this app and enable pushing data 
and provide your credentials. With those credentials you can also select a vehicle from the ones
you've created on their dashboard. 

You can enable or disable pushing data any time. You can also separately check sharing of location 
or not. Location shared is based on GPS data collected during routes logged, so only when the GPS 
service is running. The other data is shared only when either the BLE or the OBD service is 
running. Stop either service is enough to prevent data being shared.

### Using custom endpoint

You can also provide a custom cloud service endpoint to push data to. This works in a similar way 
as the ev-monitor.com implementation except this can by anything hosted anywhere. Just provide the
endpoint in the app settings and enable the function by checking the box. 

You can use either `http` or `https`. Some form of security is provided with a token that can be 
changed in the settings. This way you could reject pushes of data not having this token. 

The format of data is json. We dropped the index based format that is used for ev-monitor as of
version 2.23 of the app. 

Example of such a request below:

```
16:50:14.257  I  --> POST http://10.0.2.2/
16:50:14.257  I  Content-Type: application/json; charset=UTF-8
16:50:14.257  I  Content-Length: 1238
16:50:14.258  I  [{"chargeState":2,"timestamp":1722523810,"token":"NO_TOKEN","user-agent":"Energ1ca App"},{"debug":"DEC 2 0 0 0 0 0 0 0","timestamp":1722523810,"token":"NO_TOKEN","user-agent":"Energ1ca App"},{"battSoc":1,"battSoh":100,"battTemp":21,"battVoltage":320.0,"chargeCurrent":6.1,"chargePower":1.83488,"timestamp":1722523810,"token":"NO_TOKEN","user-agent":"Energ1ca App"},{"debug":"DEC 21 1 100 21 12 136 0 61","timestamp":1722523810,"token":"NO_TOKEN","user-agent":"Energ1ca App"},{"cellMax":3917,"cellMin":3894,"timestamp":1722523810,"token":"NO_TOKEN","user-agent":"Energ1ca App"},{"debug":"2030F40432E0F360F4D","timestamp":1722523810,"token":"NO_TOKEN","user-agent":"Energ1ca App"},{"odometer":0,"timestamp":1722523810,"token":"NO_TOKEN","user-agent":"Energ1ca App"},{"chargeState":2,"timestamp":1722523814,"token":"NO_TOKEN","user-agent":"Energ1ca App"},{"debug":"DEC 2 0 0 0 0 0 0 0","timestamp":1722523814,"token":"NO_TOKEN","user-agent":"Energ1ca App"},{"battSoc":2,"battSoh":100,"battTemp":21,"battVoltage":320.0,"chargeCurrent":6.1,"chargePower":1.83488,"timestamp":1722523814,"token":"NO_TOKEN","user-agent":"Energ1ca App"},{"debug":"DEC 21 2 100 21 12 136 0 61","timestamp":1722523814,"token":"NO_TOKEN","user-agent":"Energ1ca App"}]
16:50:14.258  I  --> END POST (1238-byte body)
```

### using MQTT for home automation

MQTT stands for Message Queuing Telemetry Transport and is a very simple protocol for connecting 
IoT devices. I've added support for this since app version 3.18. All you need to configure is an 
MQTT endpoint and secret token.

You will need an MQTT broker for this to work. You can either go with a cloud service for this or 
create your own. The easiest way to create your own is to run a Home Assistant config and enable 
MQTT on that instance. Instructions for that can be found at 
[MQTT HA integration](https://www.home-assistant.io/integrations/mqtt/). Make sure to create a 
long term token in your Home Assistant users config. 

The App will use a simplified REST interface to publish data to some pre defined topics. What 
follows are the currently supported topics so that your MQTT configuration can be adapted to 
support these. 

```
energica/soc
energica/range        // only for BLE connection
energica/odo          // onlyy for BLE conn. and Generic OBDII data
energica/reserve      // only for BLE connection
energica/temp/ambient // not pushed yet, for generic OBDII and BLE
energica/temp/battery
energica/power        // charge power in kW
energica/current      // charge current in A
energica/voltage      // battery voltage in V
```

## Exporting Data on device

All the data that is plotted in graphs in this App can be exported as CSV or comma separated 
value files. For that use the export data option on top of the graphs.

Next use a file browser (or use the Downloads app) on your phone to navigate to below directory 
where you'll find all the extracted files. 

```
internal storage > Android > data > be.hcpl.android.energica > files > Download
```

Note that this folder also contains automatically created gpx files. Those are for backup reasons
and because they are running while the route is recorded these aren't properly closed yet. You 
can do so manually by adding in the end:

```
</trkseg></trk></gpx>
```

For quick visualisation of these gpx files I've been using https://gpx.studio/l/nl/ 
 
## What this App will not provide 

The Official MyEnergica App is not a bad app. In fact it does a few things quite good, so good 
that I won't even bother implementing those in this app. Think of that horn button :D or the 
diagnostics screen.
  
I couldn't find the speed limit setting in the original app. ~~So I'll keep that in for those that 
want a lower limit than the 90 kph that is set in ECO mode. Just be aware that it only changes the 
speed limit in ECO mode and that it doesn't work for anything higher than 90 kph (or lower than 
30 kph).~~ Due to these limitations I dropped it from the app.  

## What this App will focus on

Graphs, more graphs, data and basic functionality.

## Roadmap

* more Energica specific can bus (& BLE) data decoding
* make pushing data interval configurable (on location, time...)
* convert more code to kotlin (technical task)

## Known Bugs and other issues

* sometimes initial connection with bike using BLE fails, check key exchange logic
* fix reconnect when user disabled and enabled bluetooth

## App Version History

### 4.8

* improvements for generic cloud

### 4.7

* update maps sdk version
* improved error handling on route imports
* removed unused storage permission

### 4.6

* fix invalid filename characters for gpx
* added device information at start of log and gpx files
* fixed another crash on importing route from cache
* print cache size on start of logging
* add some logging for background plotting of route from cache
* more info on background loading of cached data

### 4.5

* fixed importing route from cache
* fixed closure of GPX file logged
* log how big cache size is when starting GPS service
* test compat version of foreground service
* create a new app log file every day, not every hour
* only log state change when actually different
* load data on map done in background

### 4.4

* added confirmation on actions on top of map
* compacted gpx file by removing extra newline on each entry
* added some extra configuration for data pushing
* added an option to visualise current cached route on map
* proper logging of GPS state changes

### 4.3

* renamed manually exported files to make a distinction between those and the logged routes
* changed GPS message on top of screen to speed, average and distance
* keep plotted route and maps when switching views on bottom menu

### 4.2

* fix data broadcasting by using RECEIVER_EXPORTED
* improved app logs
* fixed starting/stopping services from other views

### 4.1

* fix crash on reset route and resume
* fix obfuscation issues
* fix refreshing services with latest settings (no more separate process)
* fix log statement if connection fails (wrong base uri is printed)

### 4.0

* major release for the new app navigation & layout
* allow app rotation + optimised layouts for landscape
* fix crash on range calculation selecting labels
* fixed service state/launch from icon vs view
* moved map options to map section in app settings
* keep screen on for map, ble and obd connection

### 3.23

* code clean up
* navigation update dropping blog and video content
* dropped storage permission pop ups in app
* fixed missing `data[0][general][batt][0][v]` field for ev monitor
* dropped `SerializedName` annotations for non EV monitor use
* added `chargeState` field to pushed data (non EV-monitor)
* dropped `vehicle` field from pushed data (non EV-monitor)
* fixed `chargeCurrent` and added chargePower (non EV-monitor)
* fixed `mqtt` charge power vs charge current values

### 3.22 

* crash fix for Android 14 with new permissions & type required for foreground services
* fixed bug with new https ev monitor option

### 3.21

* target sdk update to version 34 as required by google

### 3.20

* added min and max cell voltage w/ MQTT topics `energica/minCellVoltage` and `energica/maxCellVoltage`

### 3.19

* Update target SDK version (Google requirement)

### 3.18

* update URLs for app documentation to new wordpress location
* added secret token to payload for custom cloud service
* added MQTT integration for Home Assistant and other Home Automation

### 3.17

* add units to big numbers on top of map

### 3.16

* added battery temp, gps speed, range and SOC to map in big numbers

### 3.15

* sometimes initial connection with bike using BLE fails, always scan at first use

### 3.14

* get ODO meter value from OBD2 dongle for Energica
* add some time between pushing data sets

### 3.13

* move data flush to background task, blocks UI right now
* fix BLE connection state displayed value
* background BLE connection improvements 
* add support for generic json data service storage
* fix pushing cell balance, pack voltage and charge current to cloud
* fix layout and values on map

### 3.12

* prevent pushing empty datasets + warn user
* fixed data lost on some specific ev-monitor responses
* close keyboard after entering property in settings
* check data flush feature not working, push in smaller chunks (500)

### 3.11

* more small layout improvements
* hide power values of more than 1000 kW
* show power and voltage from OBDII connection also on map
* bugfix don't disable GPS on closing BLE or OBDII views

### 3.10

* debug and fix data loss when internet connection (temp) gone
* manual flush data option added in menu

### 3.9

* more layout improvements
* fix killing background services
* fixed removal of service notifications on older devices
* show calculated kW value on OBD dash (power = voltage * 0.94 * amps / 1000)
* fixed pack current value displayed (/10)
* fixed broken OBDII values reported on map view

### 3.8

* added battery temp value on screen from BLE connection
* improved upon BLE data parsing
* added temp to charge graphs and data exports
* Show GPS data collection dialog on first startup only.
* Few warning fixes.
* Added raw data logging on BLE connection.
* Corrected tripMeter value (missing /10.0f).
* Corrected dtc2 parsing (index not changed during copy-paste).
* Moved logging from CommParser to ConnectionManager.
* BLE raw logs uses epoch instead of nicer date format (easier to parse with awk-like tools).
* Added option to enable BLE raw logs (FIXME: probably not the best way to do it).
* BLE raw data logging directly in BleWrapper to log writes. Fixed trip/10. Fixed minor warnings.
* Removed thread name in BEGIN/END blocks. Raw logging after actual writes. Raw logging is synchronized.
* fixed BLE state remains in IDLE, even when charging or running

### 3.7

* general bug fixing for crash reports
* Added message on generic OBDII view for clarification
* reduced resolution of ODO meter value

### 3.6

* removed automatic charge detection (wip)
* added cell min and max voltage parsing to OBD data
* fixed dummy charge state values
* fixed charge and ride current value from OBD

### 3.5

* continued removal of eventbus library
* have a dummy setup for testing BLE data

### 3.4

* update in OBD data loop sequence
* eventbus library completely removed (wip)
* FLAG_IMMUTABLE added on pending intents
* fixed permission requests for latest Android releases (incl BLUETOOTH_CONNECT)

### 3.3

* improve data recovery when pushing failed because app was offline
* fix warning about location tracking

### 3.2

* restored ACCESS_BACKGROUND_LOCATION permission + added required popup
* no longer start tracking on app start
* fixed resume of same Activity on clicking background notification

### 3.1

* fixed java.lang.ArrayIndexOutOfBoundsException on permission denied
* disabled obfuscation to get more debug log info from release
* update bluetooth permissions, including BLUETOOTH_SCAN

### 3.O

* update Android target SDK and Google Maps SDK to latest versions
* merged EmApp range, blog and video views into this app

### 2.15

* small layout improvements
* fixed DC charging indication (ID 16)

### 2.14

* added battery voltage readout from Energica OBDII data
* implemented parsing Charge state field from Energica OBD2 data

### 2.13

* show warning when pushing data failed
* manual refresh option for log files
* fixed logical naming of auto connect feature settings toggles

### 2.12

* OBD2 bluetooth connection improvements 

### 2.11

* implement auto reconnect for OBDII (like BLE has)

### 2.10

* fixed plotting time based graph
* added time to logs from OBD2 data
* use icons on map view toolbar to start/stop services

### 2.9

* prevent pushing empty data to cloud service
* limit logs to last hour + made more readable
* allow pushing debug data to cloud
* debug data generation option added for Energica OBDII data

### 2.8

* fixed BLE connection state values from service
* fixed OBDII connection state values from service
* new icon for OBDII connection service
* replace default values with dashes (--) 
* also show OBDII SOC and temp data on map
* added time label on X axis for second OBDII graph
* update app menu layout for faster OBD and BLE access

### 2.7

* fixed crash on first use of app logs
* improved state update (in progress) from OBD connection service
* fixed obfuscation issue for new network calls

### 2.6

* fixed fetching vehicles
* move BLE connection to service (like GPS)
* fixed app logs using filesystem

### 2.5

* fixed viewport & layout on time based OBD2 graph
* fixed generic odometer calculated value

### 2.4

* fix parsing for ODO meter for supported vehicles
* allow for switching between time and soc based graph
* initial ev-monitor cloud storage integration

### 2.3

* add support for generic OBD2 connectoin (Zero and more)
* reorganise screens

### 2.2

* filter too high values from OBDII
* allow for notifications on changed SOC also (every 5%)
* improve connection state for OBDII
* show all updated values on each notification update
* fixed crashes on parsing data (StringIndexOutOfBoundsException and NumberFormatException)

### 2.1

* only show useful OBD2 output
* add charging graph from OBD2 data (current, temp, soc)
* fix sticky service for ob2 including notification
* show changes in temp and current as a notification

### 2.0

* add optional speed as a big overlay on map (personal feature)
* add support for OBDII dongles
* remember last used OBD device for faster connect

### 1.17

* added openchargemap.io reference to about view in app
* don't start gps tracking on resume, have a start tracking option in the settings instead
* pass preferred unit from config to openchargemap api (and update literals)
* include expected range left from calculated range values
* let user configure what fields should be visible on map overlay

### 1.16

* show nearby chargers from public api (openchargemap because it's free)
* add preferences for nearby charger fetching
* small layout improvements
* allow for navigation to nearby chargers

### 1.15

* option added for sending logs to dev
* reduced GPS position update statements in logs
* calculate actual average consumption from distance (odo) and battery used
* more small layout and color fixes
* don't use bike speed values for stats
* ignore bike GPS data when device GPS data is enabled (should improve distance measurement)

### 1.14

* fixed reset BLE connection from new settings
* small layout fixes

### 1.13

* Another auto reconnect improvement
* have a settings screen so more room left for map itself
* an in app about screen added with source code link
* connection state icons added in toolbar
* allow manual override of mi vs km units (for non BLE Energica users)
* also show a notification while service is running on older devices
* optionally log directly to gpx on device for backup

### 1.12

* more BLE connection issues resolved
* improved ConnectionManager code
* added warning about reset function

### 1.11

* faster initial location lock
* Fixed manual connect/reconnect while bike still ON (w/o app restart)
* Improved detection of connection lost (bike key OFF)
* improved auto connect on startup and permission and bluetooth setting changed 
* auto reconnects after bike OFF and ON keyswitch toggle
* have force use of phone GPS checked by default
* added reserve indication (Wh) on maps view
* keep last connection values visible on map

### 1.10

* bug fix for speed indication above map
* fixed missing plotted route on standby
* fixed background GPS on Android 8 and higher
* added confirmation on data export
* fixed crash on resuming app after being in background for longer
* show last known location on map right away if available

### 1.9

* add ride stats for logged route 
    * avg speed, top speed, gps speed, bike speed
    * number of charge stops, total charge time (bike in CHARGE mode or manual)
    * total distance of route (GPS based)
* allow for manual control of charge stops (start and stop charge session)
* include charge stops on map in export data for gpx file

### 1.8

* use readable date timestamps for exported files
* show ride speed on top of map (current GPS, bike and avg)
* show charge stops on route (initial implementation)
* small visual improvements
* reset route option? with a big warning

### 1.7

* fixed release build (obfuscation)
* alternative debug app name
* reduced in app log output gps data
* confirm gpx export with Toast message

### 1.6

* add gpx route export option and log route to file while riding
* initial map view implementation
* added event bus to push data to views
* allow use of device gps only instead of bike

### 1.5

* added export data option and direct to file logging
* improved documentation
* small screen UI improvements
* made app compatible with android 5.0 (api level 21)

### 1.4

* fixed several connect and state issues
* replaced deprecated BLE scanning code
* changed ACCESS_COARSE_LOCATION to ACCESS_FINE_LOCATION for compatibility 
* more code clean up and improvements
* small UI changes

### 1.3

* removed demo mode completely
* simplified loop logic and reduced data update refresh rate
* added app version to logs data view

### 1.2

* improved readability of charge graphs
* more charge values displayed
* allow for resetting charge graph and change scale (AC/DC)
* removal of manual input fields for security reasons

### 1.1 

* code clean up removing unused logic
* two initial graphs implemented
* removed diagnostics button
* completed charging graph added more charging numbers on view

### 1.0

Initial app release

## References

BLE disconnects were partly fixed in 1.11 but then I still encountered disconnects after the initial connection was made.
Could be a device timeout https://stackoverflow.com/questions/44785996/android-ble-device-continually-disconnects-and-re-connects-every-30-seconds

Since version 1.12 handling of already "discovered" device is improved so we don't have to scan on each connect.

Implemented graphs with [this library](https://tjah.medium.com/how-to-create-a-simple-graph-in-android-6c484324a4c1)

Event bus https://github.com/greenrobot/EventBus note that this only works when app is in foreground,
thus for service communication there is still intent broadcasting in place.

Calculate average speed https://stackoverflow.com/questions/8555102/how-to-calculate-moving-average-speed-from-gps

App Compat toolbar https://guides.codepath.com/android/using-the-app-toolbar

Android Dialog theme styling https://web.archive.org/web/20200214041924/http://blog.supenta.com/2014/07/02/how-to-style-alertdialogs-like-a-pro/

Chargemap api docs https://openchargemap.io/site/develop/api

## Contribution guidelines

### Charge locations

Charge station integration is provided by https://openchargemap.io so do show them some love
by getting their app. And once you've got their app installed and you're charging at a charger
that isn't in their system yet you can just add it from there.

### Code contributions

This is a public repo so everyone can see code. However only limited people have access to publish 
code. If you want in just let me know. If you rather do your own thing feel free to branch this repo.

For those pushing code here make sure to do so on develop branches. The limited free plan build 
pipelines are now configured to run only from the master branch. Once a release is tested and fine 
we can move to the master branch.

Also tag releases so we have an easy way to check back on previous builds.

[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

[Learn BLE](https://developer.android.com/guide/topics/connectivity/bluetooth-le)

* Writing tests - but only if you are in the mood
* Code review - not required, just fork this repo
* Other guidelines - enjoy life